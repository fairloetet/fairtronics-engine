from tests.test_helper import database, portfolio, device, part, material, resource, location, indicator, \
                              PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, LOCATION, INDICATOR, CHAIN, Slug
from fairtronics_engine.unit import Unit, compile_spec_string, extract_filtered_slugs
import pytest


data = database(
        portfolio("portfolio1", devices={"device1": 1.0, "device2": 1.0}),
        device("device1", parts={"part1": 1.0, "part2": 1.0}),
        device("device2", parts={"part1": 1.0, "part2": 1.0}),
        part("part1", materials={"material1": 1.0, "material2": 1.0}),
        part("part2", materials={"material1": 1.0}, manufacturer="man"),
        material("material1", category="cat", cas="cas"),
        material("material2", cas="cas"),
        material("material_extra")
    )


def test_unit_base():
    unit = Unit(data, PART, "part.*")
    assert unit.tier is PART
    assert min(unit.supply_chain.keys()) == PART
    assert len(unit.supply_chain[PART]) == 2
    assert "part1" in unit.supply_chain[PART]
    assert "part2" in unit.supply_chain[PART]


def test_unit_supply_chain():
    unit = Unit(data, DEVICE, "device1")
    assert PORTFOLIO not in unit.supply_chain
    assert len(unit.supply_chain[DEVICE]) == 1
    assert "device1" in unit.supply_chain[DEVICE]
    assert "device2" not in unit.supply_chain[DEVICE]
    assert len(unit.supply_chain[PART]) == 2
    assert "part1" in unit.supply_chain[PART]
    assert "part2" in unit.supply_chain[PART]
    assert len(unit.supply_chain[MATERIAL]) == 2
    assert len([m for m in unit.supply_chain[MATERIAL] if m == "material1"]) == 1
    assert "material_extra" not in unit.supply_chain[MATERIAL]
    assert RESOURCE in unit.supply_chain  # TODO Warum ist das so? Nötig?


def test_unit_with_filter():
    unit = Unit(data, PORTFOLIO, "portfolio1", "p[part2]")
    assert "device1" in unit.supply_chain[DEVICE]
    assert "part1" not in unit.supply_chain[PART]
    assert "part2" in unit.supply_chain[PART]
    assert "material1" in unit.supply_chain[MATERIAL]
    assert "material2" not in unit.supply_chain[MATERIAL]


# TODO This needs more testing
def test_compile_spec_string():
    selection, depth, filter = compile_spec_string("dP[filter,with,comma,and,equal=sign]r[res_filter]Ci")
    assert selection == [PART, LOCATION]
    assert depth == INDICATOR
    assert filter == {PART: "filter,with,comma,and,equal=sign", RESOURCE: "res_filter"}


def test_extract_filtered_slugs_slug():
    filters = extract_filtered_slugs(data, {MATERIAL: "material_extra"})
    assert filters[MATERIAL] == {Slug("material_extra")}
    assert all(f not in filters for f in set(CHAIN.tiers.values()) - {MATERIAL})


def test_extract_filtered_slugs_two_results():
    filters = extract_filtered_slugs(data, {MATERIAL: "cas=cas"})
    assert filters[MATERIAL] == {Slug("material1"), Slug("material2")}


def test_extract_filtered_slugs_double_criteria():
    filters = extract_filtered_slugs(data, {MATERIAL: "cas=cas,category=cat"})
    assert filters[MATERIAL] == {Slug("material1")}


def test_extract_filtered_slugs_double_criteria_incl_slug():
    filters = extract_filtered_slugs(data, {MATERIAL: "material.*,category=cat"})
    assert filters[MATERIAL] == {Slug("material1")}


def test_extract_filtered_slugs_multiple_tiers():
    filters = extract_filtered_slugs(data, {MATERIAL: "material\\d", PART: "manufacturer=.+"})
    assert filters[MATERIAL] == {Slug("material1"), Slug("material2")}
    assert filters[PART] == {Slug("part2")}


def test_extract_filtered_slugs_empty_result():
    with pytest.raises(ValueError):
        extract_filtered_slugs(data, {MATERIAL: "cas=xxxxxx"})
