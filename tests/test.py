import sys
import pandas as pd
import fairtronics_engine as ft
import time

from fairtronics_engine.database import PART


def main(database_folder: str):
    pd.set_option('display.max_rows', 50)
    pd.set_option('display.max_columns', 10)
    pd.set_option('display.width', 1000)
    pd.set_option('display.colheader_justify', 'right')
    pd.set_option('display.float_format', '{:.4f}'.format)

    print("Starting at ", end='')
    st = time.time()
    # spt = time.process_time()
    print(time.strftime('%Y-%m-%d %H:%M', time.localtime(st)))

    database = ft.Database(database_folder)

    if "test" in database.devices.keys():  # minimal
        test = ft.Unit(database, ft.DEVICE, "test")
        print(test.analyse("Dp").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("Dpm").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("Dpmr").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("dPm").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("dpM").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("dPmr").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("dpmR").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("dpmRC").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("dpmrC").view().dump())
        print("-------------------------------------------------------------------")
        print(test.analyse("DPMRCI").view().show(ft.SHOW_STANDARD).dump())
    elif "device" in database.devices.keys():  # demo_tables
        device = ft.Unit(database, ft.DEVICE, "device")
        if True:
            unit = ft.Unit(database, ft.PORTFOLIO, "portfolio", "p[part1]")
            print(unit.analyse("F__m").view().dump())
            print(unit.analyse("Fd").view().dump())
            print(unit.analyse("Fp").view().dump())
            print(unit.analyse("M").view().dump())
            print(unit.analyse("P").view().dump())
            pass
        elif False:
            print(device.analyse("Dp").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("Dpm").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("Dpmr").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dPm").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dpM").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dPmr").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dpmR").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dpmRC").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dpmrC").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("DPmRCI").view().show(ft.SHOW_STANDARD).dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("Dp_r").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dP_r").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dp_R").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dp_RC").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("dp_rC").view().dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("DP_RCI").view().show(ft.SHOW_STANDARD).dump())
            print("-------------------------------------------------------------------")
            print(device.analyse("DPMRCi[indicator1]").view().show(ft.SHOW_STANDARD).dump())
            print("-------------------------------------------------------------------")
    elif "mnt-reform" in database.devices.keys():  # full
        # To test anything
        if False:
            print("*******************************************************************")
            print(ft.Unit(database, ft.PART, ".*").analyse("P").view().dump())
            # df = database.list(2).dataframe()
            # rec = df.to_records()
            # dt = df.to_dict('tight')
        # To test explain
        if False:
            mnt_reform = ft.Unit(database, ft.DEVICE, "mnt-reform")
            print(mnt_reform.explain("c[peru]r[unknown-tin_ore]").write())
            print(mnt_reform.explain("c[peru]i[forced-labour]p[pcbway-mnt_reform_keyboard_pcb]").write())
            print(mnt_reform.explain("r[unknown-bauxite_ore]p[neuform-mnt_MREFCSCF20R01]c[guinea]").write())
        # To test unit filtering and filtering with multiple properties
        if False:
            device = ft.Unit(database, ft.DEVICE, "mnt-reform", "i[probability_based=True,direction=ascending,"
                                                                "usage=straight]")
            print(device.analyse("I").view().dump())
        # Daten zu Improvements Folien
        if False:
            carrizal_copper = ft.Unit(database, ft.MATERIAL, "carrizal-copper")
            print(carrizal_copper.analyse("Mi[child-labour]").view().value_unit())
            print(carrizal_copper.analyse("Mi[child-labour]").view().percentageof(
                carrizal_copper.analyse("Mr").view()).value())
        if False:
            te_usb = ft.Unit(database, ft.PART, "te-1487593-1")
            print(te_usb.analyse("Pm[.*-copper]").view().value_unit())
            print(te_usb.analyse("Pm[.*-copper]r").view().value_unit())
            print(te_usb.analyse("Pm[.*-gold]").view().value_unit())
            print(te_usb.analyse("Pm[.*-gold]r").view().value_unit())
            print(te_usb.analyse("Pr").view().value_unit())
            print(te_usb.analyse("Pi[child-labour]").view().value_unit())
            print("-------------------------------------------------------------------")
            fairloetet_usb_cable = ft.Unit(database, ft.PART, "fairloetet-usb_cable")
            print(fairloetet_usb_cable.analyse("Pm[.*-copper]").view().value_unit())
            print(fairloetet_usb_cable.analyse("Pm[.*-copper]r").view().value_unit())
            print(fairloetet_usb_cable.analyse("Pr").view().value_unit())
            print(fairloetet_usb_cable.analyse("Pi[child-labour]").view().value_unit())
            print("-------------------------------------------------------------------")
            syllucid_usb_cable = ft.Unit(database, ft.PART, "syllucid-usb_cable")
            print(syllucid_usb_cable.analyse("Pm[.*-gold]").view().value_unit())
            print(syllucid_usb_cable.analyse("Pm[.*-gold]r").view().value_unit())
            print(syllucid_usb_cable.analyse("Pr").view().value_unit())
            print(syllucid_usb_cable.analyse("Pi[child-labour]").view().value_unit())
        # Full new MNT Reform benchmark
        if True:
            mnt_reform = ft.Unit(database, ft.DEVICE, "mnt-reform")
            print("*******************************************************************")
            if True:
                print(mnt_reform.analyse("Dp").view().dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("Dpm").view().dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("Dpmr").view().dump())
                print("-------------------------------------------------------------------")
            if True:
                print(mnt_reform.analyse("dPm").view_top_other(20).dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("dpM").view_top_other(50).dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("dPmr").view_top_other(20).dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("dpmR").view_top_other(50).dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("dpmRC").view_top_other(20).dump())
                print("-------------------------------------------------------------------")
                print(mnt_reform.analyse("dpmrC").view_top_other(50).dump())
                print("-------------------------------------------------------------------")
            if True:
                print(mnt_reform.analyse("Di[child-labour]").view().dump())
                print("-------------------------------------------------------------------")
                if True:
                    print(mnt_reform.analyse("Di[child-labour-inverse]").view().dump())
                    print("-------------------------------------------------------------------")
                    print(mnt_reform.analyse("Di[child-labour-unknown]").view().dump())
                    print("-------------------------------------------------------------------")
                if True:
                    print(mnt_reform.analyse("Pi[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                    print(mnt_reform.analyse("Mi[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                    print(mnt_reform.analyse("Ri[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                    print(mnt_reform.analyse("Ci[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                # These currently take to long because of bad view implementation
                if True:
                    print(mnt_reform.analyse("PMi[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                    print(mnt_reform.analyse("MCi[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                    print(mnt_reform.analyse("RCi[child-labour]").view_top_other(10).dump())
                    print("-------------------------------------------------------------------")
                if True:
                    print(mnt_reform.analyse("PMCi[child-labour]").view_top_other(10).
                          show([ft.RANK, ft.AMOUNT, ft.UNIT, ft.SHARE, ft.NAME, 'part manufacturer',
                                'material category', 'location sub region']).dump())
                    print("-------------------------------------------------------------------")
                if True:
                    print(mnt_reform.analyse("Dci[child-labour]").view().percentageof(
                          mnt_reform.analyse("Dc").view()).value())
                    print("-------------------------------------------------------------------")
    et = time.time()
    # ept = time.process_time()
    print()
    print("Execution time: ", et - st, "seconds")
    # print("CPU Process time: ", ept - spt, "seconds")

    pass


if __name__ == "__main__":
    # Path to database files required
    main(sys.argv[1])
