from fairtronics_engine import STRAIGHT, INVERSE, UNKNOWN
from fairtronics_engine.database import Database, PORTFOLIO, DEVICE, PART, MATERIAL, \
                                        RESOURCE, LOCATION, INDICATOR, CHAIN
from fairtronics_engine.model import Manufacturer, Slug, Entity, Tier, Chain, \
                                     Portfolio, DeviceAssignment, Device, PartAssignment, \
                                     Part, MaterialAssignment, Material, ResourceAssignment, Resource, \
                                     LocationAssignment, Location, IndicatorAssignment, Indicator, \
                                     Manufacturer
from fairtronics_engine import AMOUNT, NAME, SHORT, SHARE, RANK, TYPE, UNIT
import pandas as pd
from typing import Dict, Optional, Tuple, Any

from fairtronics_engine.statics import OTHER_NAME, OTHER_SHORT, OTHER_SLUG


def get_names(slug: str, name: Optional[str] = None, short: Optional[str] = None):
    if not name:
        name = slug.capitalize()
    if not short:
        short = name.translate(str.maketrans(dict.fromkeys('aeiou')))
    return name, short


def portfolio(slug: str, name: Optional[str] = None, short: Optional[str] = None,
              devices: Optional[Dict[str, float]] = None) -> Portfolio:
    name, short = get_names(slug, name, short)
    portfolio = Portfolio(slug=Slug(slug), name=name, short_name=short, devices={})
    if devices:
        for device in devices:
            portfolio.devices[Slug(device)] = DeviceAssignment(entity_slug=Slug(slug), assigned_slug=Slug(device),
                                                               amount=devices[device], source="")
    return portfolio


def device(slug: str, name: Optional[str] = None, short: Optional[str] = None, 
           parts: Optional[Dict[str, float]] = None) -> Device:
    name, short = get_names(slug, name, short)
    device = Device(slug=Slug(slug), name=name, short_name=short, parts={},
                    manufacturer="", dependency="", description="", project_url="")
    if parts:
        for part in parts:
            device.parts[Slug(part)] = PartAssignment(entity_slug=Slug(slug), assigned_slug=Slug(part),
                                                      amount=parts[part], source="")
    return device


def part(slug: str, name: Optional[str] = None, short: Optional[str] = None,
         materials: Optional[Dict[str, float]] = None, manufacturer: str = "", category: str = "") -> Part:
    name, short = get_names(slug, name, short)
    part = Part(slug=Slug(slug), name=name, short_name=short, materials={},
                manufacturer=manufacturer, category=category, weight=1.0, weight_unit="g", product_url="")
    if materials:
        for material in materials:
            part.materials[Slug(material)] = MaterialAssignment(entity_slug=Slug(slug), assigned_slug=Slug(material),
                                                                amount=materials[material], source="")
    return part


def material(slug: str, name: Optional[str] = None, short: Optional[str] = None,
             resources: Optional[Dict[str, float]] = None, category: str = "", cas: str = "") -> Material:
    name, short = get_names(slug, name, short)
    material = Material(slug=Slug(slug), name=name, short_name=short, resources={},
                        category=category, cas=cas, density_g_cm3="")
    if resources:
        for resource in resources:
            material.resources[Slug(resource)] = ResourceAssignment(entity_slug=Slug(slug), assigned_slug=Slug(resource),
                                                                    amount=resources[resource], source="")
    return material


def resource(slug: str, name: Optional[str] = None, short: Optional[str] = None,
             locations: Optional[Dict[str, float]] = None) -> Resource:
    name, short = get_names(slug, name, short)
    resource = Resource(slug=Slug(slug), name=name, short_name=short, locations={},
                        circularity="virgin")
    if locations:
        for location in locations:
            resource.locations[Slug(location)] = LocationAssignment(entity_slug=Slug(slug), assigned_slug=Slug(location),
                                                                  amount=locations[location], source="")
    return resource


def location(slug: str, name: Optional[str] = None, short: Optional[str] = None,
            indicators: Optional[Dict[str, float]] = None) -> Location:
    name, short = get_names(slug, name, short)
    location = Location(slug=Slug(slug), name=name, short_name=short, indicators={},
                      code="", region="", sub_region="")
    if indicators:
        for indicator in indicators:
            location.indicators[Slug(indicator)] = IndicatorAssignment(entity_slug=Slug(slug), assigned_slug=Slug(indicator),
                                                                      amount=indicators[indicator], source="")
    return location


def indicator(slug: str, name: Optional[str] = None, short: Optional[str] = None) -> Indicator:
    name, short = get_names(slug, name, short)
    indicator = Indicator(slug=Slug(slug), name=name, short_name=short,
                          direction='ascending', probability_based=True, usage=STRAIGHT, data_base="")
    return indicator


def manufacturer(slug: str, name: Optional[str] = None, short: Optional[str] = None) -> Manufacturer:
    name, short = get_names(slug, name, short)
    manufacturer = Manufacturer(slug=Slug(slug), name=name, short_name=short, company_url="")
    return manufacturer


def database(*args: Entity) -> Database:
    database = Database()
    database.name = "Test database"
    for entity in args:
        if isinstance(entity, Portfolio):
            database.portfolios[entity.slug] = entity
        if isinstance(entity, Device):
            database.devices[entity.slug] = entity
        if isinstance(entity, Part):
            database.parts[entity.slug] = entity
        if isinstance(entity, Material):
            database.materials[entity.slug] = entity
        if isinstance(entity, Resource):
            database.resources[entity.slug] = entity
        if isinstance(entity, Location):
            database.locations[entity.slug] = entity
        if isinstance(entity, Indicator):
            database.indicators[entity.slug] = entity
        if isinstance(entity, Manufacturer):
            database.manufacturers[entity.slug] = entity
    database.unknown_manufacturer = Manufacturer(name="- Unknown - ", short_name="Unknown", 
                                                    slug=Slug("unknown"), company_url="")
    return database


def test_indicator_minimal():
    indicator1 = indicator("indicator1")
    assert indicator1.slug == Slug("indicator1")
    assert indicator1.name == "Indicator1"
    assert indicator1.short_name == "Indctr1"
    assert indicator1.usage == "straight"
    assert indicator1.probability_based is True
    assert indicator1.data_base is ""


def test_indicator_full():
    indicator1 = indicator("indicator1", "Indicator1", "Ind1")
    assert indicator1.slug == Slug("indicator1")
    assert indicator1.name == "Indicator1"
    assert indicator1.short_name == "Ind1"
    assert indicator1.usage == "straight"
    assert indicator1.probability_based is True
    assert indicator1.data_base is ""


def test_location_typical():
    location1 = location("location1", "Location1", "Ctry1")
    assert location1.slug == Slug("location1")
    assert location1.name == "Location1"
    assert location1.short_name == "Ctry1"
    assert location1.region is ""
    assert location1.sub_region is ""
    assert location1.code is ""


def test_location_indicators():
    indicator1 = indicator("i1")
    indicator2 = indicator("i2")
    location1 = location("Location1", "Location1", "Ctry1", indicators={"i1": 1.0, "i2": 2.0})
    assert len(location1.assignments()) == 2
    assert location1.assignments()[indicator1.slug].amount == 1.0
    assert location1.assignments()[indicator2.slug].amount == 2.0


def dataframe(*args: Tuple[Optional[Tier], Slug, Tuple[int, float, str, float, 
                                                       Optional[str], Optional[str], Optional[str]]]):
    data_dict = {
        RANK: dict(),
        AMOUNT: dict(),
        UNIT: dict(),
        SHARE: dict(),
        NAME: dict(),
        SHORT: dict(),
        TYPE: dict()
    }
    index_list = list()
    for tier, slug, values in args:
        index_list.append(slug)
        data_dict[RANK][slug] = values[0]
        data_dict[AMOUNT][slug] = values[1]
        data_dict[UNIT][slug] = values[2]
        data_dict[SHARE][slug] = values[3]
        data_dict[NAME][slug] = values[4] if values[4] else ""
        data_dict[SHORT][slug] = values[5] if values[5] else ""
        data_dict[TYPE][slug] = values[6] if values[6] else ""
    return pd.DataFrame(data_dict, index=pd.Index(index_list, name="slug"))


def entity_row(tier: Tier, slug: str, rank: int = 1, amount: float = 1.0, share: float = 100.0,
               name: Optional[str] = None, short: Optional[str] = None) -> \
               Tuple[Optional[Tier], Slug, Tuple[int, float, str, float, Optional[str], Optional[str], Optional[str]]]:
    return (tier, Slug(slug), (rank, amount, tier.unit, share, name, short, tier.name.lower()))


def other_row(slug: str, rank: int = 1, amount: float = 1.0, share: float = 100.0,
              name: Optional[str] = None, short: Optional[str] = None) -> \
              Tuple[Optional[Tier], Slug, Tuple[int, float, str, float, Optional[str], Optional[str], Optional[str]]]:
    return (None, Slug(slug), (rank, amount, "", share, name, short, ""))


def test_dataframe_entity_row():
    df = dataframe(entity_row(DEVICE, "device1", 1, 1.0, 100.0, "Deeevice1", "Dev1"))
    assert isinstance(df, pd.DataFrame)
    assert df.at["device1", RANK] == 1
    assert df.at["device1", AMOUNT] == 1.0
    assert df.at["device1", UNIT] == DEVICE.unit
    assert df.at["device1", NAME] == "Deeevice1"
    assert df.at["device1", SHORT] == "Dev1"
    assert df.at["device1", SHARE] == 100.0
    assert df.at["device1", TYPE] == DEVICE.name.lower()


def test_dataframe_two_entity_rows():
    df = dataframe(entity_row(PART, "part1", 1, 0.4, 40.0, "Part1", "Prt1"),
                   entity_row(PART, "part2", 2, 0.6, 60.0, "Part2", "Prt2"))
    assert len(df.index) == 2
    assert df.at["part1", RANK] == 1
    assert df.at["part2", RANK] == 2
    assert df[AMOUNT].sum() == 0.4 + 0.6
    assert df[SHARE].sum() == 40.0 + 60.0


def test_dataframe_other_row():
    df = dataframe(other_row(OTHER_SLUG, 1, 1.0, 100.0, OTHER_NAME, OTHER_SHORT))
    assert isinstance(df, pd.DataFrame)
    assert df.at[OTHER_SLUG, RANK] == 1
    assert df.at[OTHER_SLUG, AMOUNT] == 1.0
    assert df.at[OTHER_SLUG, UNIT] == ""
    assert df.at[OTHER_SLUG, NAME] == OTHER_NAME
    assert df.at[OTHER_SLUG, SHORT] == OTHER_SHORT
    assert df.at[OTHER_SLUG, SHARE] == 100.0
    assert df.at[OTHER_SLUG, TYPE] == ""
