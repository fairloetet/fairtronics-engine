from tests.test_helper import database, portfolio, device, part, material, resource, location, indicator, \
                              Slug, \
                              PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, LOCATION, INDICATOR, CHAIN
from fairtronics_engine.unit import Unit
from fairtronics_engine.analysis import StaticCube
from fairtronics_engine.cube_method import ViewMethod
import pytest
import numpy as np


# TODO Unnötig, sobald die supply_chain alles beinhaltet, dann halt hier die supply-chain hinterlegen
data = database(
        portfolio("folio1", "Portfolio1", "Ptf1", {"device1": 1.0}),
        device("device1", "Device1", "Dev1", {"part1": 0.6, "part2": 0.4}),
        part("part1", "Part1", "Pt1", {"material1": 0.6, "material2": 0.4, "material3": 0.1}),
        part("part2", "Part2", "Pt2", {"material1": 0.3, "material2": 0.7, "material3": 0.1}),
        material("material1", "Material1", "Mat1", {"resource1": 10.0}),
        material("material2", "Material2", "Mat2", {"resource2": 10.0}),
        material("material3", "Material3", "Mat3", {"resource3": 10.0}),
        resource("resource1", "Resource1", "Res1", {"location1": 0.8, "location2": 0.2}),
        resource("resource2", "Resource2", "Res2", {"location1": 0.6, "location2": 0.4}),
        resource("resource3", "Resource3", "Res3", {"location1": 0.0, "location2": 0.0}),
        location("location1", "Location1", "Ctry1", {"indicator1": 0.05, "indicator2": 0.02}),
        location("location2", "Location2", "Ctry2", {"indicator1": 0.2, "indicator2": 0.1}),
        indicator("indicator1", "Indicator1", "Ind1"),
        indicator("indicator2", "Indicator2", "Ind2")
    )


def test_static_cube_init():
    unit = Unit(data, PORTFOLIO, "folio1")
    # Dm[material1]
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [DEVICE], {MATERIAL: {Slug("material1")}}, MATERIAL)
    assert cube.start_tier == PORTFOLIO
    assert cube.final_tier == MATERIAL
    assert cube.selection == [DEVICE]
    assert len(cube.slug_map) == 4
    assert PORTFOLIO in cube.slug_map
    assert cube.slug_map[PORTFOLIO] == [Slug("folio1")]
    assert DEVICE in cube.slug_map
    assert cube.slug_map[DEVICE] == [Slug("device1")]
    assert PART in cube.slug_map
    assert sorted(cube.slug_map[PART]) == [Slug("part1"), Slug("part2")]
    assert MATERIAL in cube.slug_map
    assert cube.slug_map[MATERIAL] == [Slug("material1")]


def test_static_cube_calculation_Dmaterial1():
    unit = Unit(data, PORTFOLIO, "folio1")
    # Dm[material1]
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [DEVICE], {MATERIAL: {Slug("material1")}}, MATERIAL)
    cube.calculate()
    assert cube.ndary.shape == tuple([len([DEVICE])])
    assert cube.ndary.shape == (1,)
    # 0.6 = part1 of device1, 0.6 = material1 of part1, 0.4 = part2 of device1, 0.3 = material1 of part2
    assert cube.ndary[0] == 0.6 * 0.6 + 0.4 * 0.3
    assert np.sum(cube.ndary) == ViewMethod.dtype(float(0.6 * 0.6 + 0.4 * 0.3))


def test_static_cube_calculation_fDp():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [DEVICE], {}, PART)
    cube.calculate()
    assert cube.ndary.shape == (1,)
    assert cube.ndary[0] == 1.0


def test_static_cube_calculation_fDm():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [DEVICE], {}, MATERIAL)
    cube.calculate()
    assert cube.ndary.shape == (1,)
    assert cube.ndary[0] == 1.1


def test_static_cube_calculation_fDr():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [DEVICE], {}, RESOURCE)
    cube.calculate()
    assert cube.ndary.shape == (1,)
    assert cube.ndary[0] == pytest.approx(11.0)


def test_static_cube_calculation_fPm():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [PART], {}, MATERIAL)
    cube.calculate()
    assert cube.ndary.shape == (2,)
    assert cube.ndary[cube.slug_map[PART].index(Slug("part1"))] == pytest.approx(0.66)
    assert cube.ndary[cube.slug_map[PART].index(Slug("part2"))] == pytest.approx(0.44)


def test_static_cube_calculation_fM():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [MATERIAL], {}, MATERIAL)
    cube.calculate()
    assert cube.ndary.shape == (3,)
    assert cube.ndary[cube.slug_map[MATERIAL].index(Slug("material1"))] == pytest.approx(0.6 * 0.6 + 0.4 * 0.3)
    assert cube.ndary[cube.slug_map[MATERIAL].index(Slug("material2"))] == pytest.approx(0.6 * 0.4 + 0.4 * 0.7)
    assert cube.ndary[cube.slug_map[MATERIAL].index(Slug("material3"))] == pytest.approx(0.6 * 0.1 + 0.4 * 0.1)


def test_static_cube_calculation_fPr():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [PART], {}, RESOURCE)
    cube.calculate()
    assert cube.ndary.shape == (2,)
    assert cube.ndary[cube.slug_map[PART].index(Slug("part1"))] == pytest.approx(
        0.6 * 0.6 * 10.0 + 0.6 * 0.4 * 10.0 + 0.6 * 0.1 * 10.0)
    assert cube.ndary[cube.slug_map[PART].index(Slug("part2"))] == pytest.approx(
        0.4 * 0.3 * 10.0 + 0.4 * 0.7 * 10.0 + 0.4 * 0.1 * 10.0)


def test_static_cube_calculation_fR():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [RESOURCE], {}, RESOURCE)
    cube.calculate()
    assert cube.ndary.shape == (3,)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource1"))] == \
           pytest.approx(0.6 * 0.6 * 10.0 + 0.4 * 0.3 * 10.0)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource2"))] == \
           pytest.approx(0.6 * 0.4 * 10.0 + 0.4 * 0.7 * 10.0)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource3"))] == \
           pytest.approx(0.6 * 0.1 * 10.0 + 0.4 * 0.1 * 10.0)


def test_static_cube_calculation_fRC():
    unit = Unit(data, PORTFOLIO, "folio1")
    # COUNTRY is automatically taken as depth
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [RESOURCE, LOCATION], {})
    cube.calculate()
    assert cube.ndary.shape == (3, 2)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource1")),
                      cube.slug_map[LOCATION].index(Slug("location1"))] == \
           pytest.approx(0.6 * 0.6 * 10.0 * 0.8 + 0.4 * 0.3 * 10.0 * 0.8)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource2")),
                      cube.slug_map[LOCATION].index(Slug("location1"))] == \
           pytest.approx(0.6 * 0.4 * 10.0 * 0.6 + 0.4 * 0.7 * 10.0 * 0.6)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource3")),
                      cube.slug_map[LOCATION].index(Slug("location1"))] == 0.0
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource1")),
                      cube.slug_map[LOCATION].index(Slug("location2"))] == \
           pytest.approx(0.6 * 0.6 * 10.0 * 0.2 + 0.4 * 0.3 * 10.0 * 0.2)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource2")),
                      cube.slug_map[LOCATION].index(Slug("location2"))] == \
           pytest.approx(0.6 * 0.4 * 10.0 * 0.4 + 0.4 * 0.7 * 10.0 * 0.4)
    assert cube.ndary[cube.slug_map[RESOURCE].index(Slug("resource3")),
                      cube.slug_map[LOCATION].index(Slug("location2"))] == 0.0


# TODO Needs way more tests
def test_static_cube_values():
    unit = Unit(data, PORTFOLIO, "folio1")
    # fDm[material1]
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [MATERIAL])
    cube.calculate()
    slugs_n_values, top_sum, middle_sum, bottom_sum, size = cube.values()
    assert size == 3
    assert top_sum == 0.0
    assert bottom_sum == 0.0
    # 0.6 = part1 of device1, 0.6 = material1 of part1, 0.4 = part2 of device1, 0.3 = material1 of part2
    # 0.6 = part1 of device1, 0.4 = material2 of part1, 0.4 = part2 of device1, 0.7 = material2 of part2
    # 0.6 = part1 of device1, 0.1 = material2 of part1, 0.4 = part2 of device1, 0.1 = material2 of part2
    assert middle_sum == pytest.approx((0.6 * 0.6 + 0.4 * 0.3) + (0.6 * 0.4 + 0.4 * 0.7) + (0.6 * 0.1 + 0.4 * 0.1))
    assert next(iter(slugs_n_values)) == (["material2"], pytest.approx(0.6 * 0.4 + 0.4 * 0.7))
    assert next(iter(slugs_n_values)) == (["material1"], pytest.approx(0.6 * 0.6 + 0.4 * 0.3))
    assert next(iter(slugs_n_values)) == (["material3"], pytest.approx(0.6 * 0.1 + 0.4 * 0.1))
    with pytest.raises(StopIteration):
        next(iter(slugs_n_values))


def test_static_cube_values_with_start():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [MATERIAL])
    cube.calculate()
    slugs_n_values, top_sum, middle_sum, bottom_sum, size = cube.values(start=1)
    # size is the count of all values, regardless of how many have been given (start/end) in the slugs_n_values
    assert size == 3
    # material2 is more than material1 and material3, so it's in the top-other because of start = 1
    # 0.6 = part1 of device1, 0.4 = material2 of part1, 0.4 = part2 of device1, 0.7 = material2 of part2
    assert top_sum == pytest.approx(0.6 * 0.4 + 0.4 * 0.7)
    assert bottom_sum == 0.0
    # 0.6 = part1 of device1, 0.6 = material1 of part1, 0.4 = part2 of device1, 0.3 = material1 of part2
    # 0.6 = part1 of device1, 0.1 = material2 of part1, 0.4 = part2 of device1, 0.1 = material2 of part2
    assert middle_sum == pytest.approx((0.6 * 0.6 + 0.4 * 0.3) + (0.6 * 0.1 + 0.4 * 0.1))
    assert next(iter(slugs_n_values)) == (["material1"], pytest.approx(0.6 * 0.6 + 0.4 * 0.3))
    assert next(iter(slugs_n_values)) == (["material3"], pytest.approx(0.6 * 0.1 + 0.4 * 0.1))
    with pytest.raises(StopIteration):
        next(iter(slugs_n_values))


def test_static_cube_values_with_end():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [MATERIAL])
    cube.calculate()
    slugs_n_values, top_sum, middle_sum, bottom_sum, size = cube.values(end=1)
    # size is the count of all values, regardless of how many have been given (start/end) in the slugs_n_values
    assert size == 3
    assert top_sum == 0.0
    # material1 and material 3 are less than material2, so they are in the bottom-other because of end = 1 (incl.)
    # 0.6 = part1 of device1, 0.6 = material1 of part1, 0.4 = part2 of device1, 0.3 = material1 of part2
    # 0.6 = part1 of device1, 0.1 = material2 of part1, 0.4 = part2 of device1, 0.1 = material2 of part2
    assert bottom_sum == pytest.approx((0.6 * 0.6 + 0.4 * 0.3) + (0.6 * 0.1 + 0.4 * 0.1))
    # 0.6 = part1 of device1, 0.4 = material2 of part1, 0.4 = part2 of device1, 0.7 = material2 of part2
    assert middle_sum == pytest.approx(0.6 * 0.4 + 0.4 * 0.7)
    assert next(iter(slugs_n_values)) == (["material2"], pytest.approx(0.6 * 0.4 + 0.4 * 0.7))
    with pytest.raises(StopIteration):
        next(iter(slugs_n_values))


def test_static_cube_values_with_end_out_of_bounds():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [MATERIAL])
    cube.calculate()
    # end out of bounds doesn't matter: It's shrinked to the size and therefore all is included
    slugs_n_values, top_sum, middle_sum, bottom_sum, size = cube.values(end=4)
    assert size == 3
    assert top_sum == 0.0
    assert bottom_sum == 0.0
    assert middle_sum == pytest.approx((0.6 * 0.4 + 0.4 * 0.7) + (0.6 * 0.6 + 0.4 * 0.3) + (0.6 * 0.1 + 0.4 * 0.1))
    assert next(iter(slugs_n_values)) == (["material2"], pytest.approx(0.6 * 0.4 + 0.4 * 0.7))
    assert next(iter(slugs_n_values)) == (["material1"], pytest.approx(0.6 * 0.6 + 0.4 * 0.3))
    assert next(iter(slugs_n_values)) == (["material3"], pytest.approx(0.6 * 0.1 + 0.4 * 0.1))
    with pytest.raises(StopIteration):
        next(iter(slugs_n_values))


def test_static_cube_values_with_specified_start_and_end_out_of_bound():
    unit = Unit(data, PORTFOLIO, "folio1")
    cube = StaticCube(data, ViewMethod(), unit.supply_chain, [MATERIAL])
    cube.calculate()
    slugs_n_values, top_sum, middle_sum, bottom_sum, size = cube.values(start=1, end=4)
    assert top_sum == 0.6 * 0.4 + 0.4 * 0.7
    assert bottom_sum == 0.0
    assert middle_sum == pytest.approx((0.6 * 0.6 + 0.4 * 0.3) + (0.6 * 0.1 + 0.4 * 0.1))
    assert next(iter(slugs_n_values)) == (["material1"], pytest.approx(0.6 * 0.6 + 0.4 * 0.3))
    assert next(iter(slugs_n_values)) == (["material3"], pytest.approx(0.6 * 0.1 + 0.4 * 0.1))
    with pytest.raises(StopIteration):
        next(iter(slugs_n_values))
