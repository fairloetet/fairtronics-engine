import fairtronics_engine as ft
import pytest

data = ft.Database("../fairtronics-data/target/full")
nager_it_mouse = ft.Unit(data, ft.DEVICE, ".*nager.*")
mnt_reform = ft.Unit(data, ft.DEVICE, "mnt-reform")


def test_mnt_reform_analyse_Dp():
    view = mnt_reform.analyse("Dp").view()
    assert view.number_of_all_entities() == 1
    assert view.sum_of_all_amounts() == pytest.approx(1699.7236, abs=1e-3)
    assert "Parts" in view.title()
    assert view.dataframe().shape == (1, 7)
    assert view.dataframe().loc["mnt-reform", ft.AMOUNT] == pytest.approx(1699.7236, abs=1e-3)
    assert view.dataframe().loc["mnt-reform", ft.UNIT] == "items"
    assert view.dataframe().loc["mnt-reform", ft.TYPE] == "device"


def test_mnt_reform_analyse_Dpm():
    view = mnt_reform.analyse("Dpm").view()
    assert view.sum_of_all_amounts() == pytest.approx(2195.5644, abs=1e-3)
    assert "Materials" in view.title()
    assert view.dataframe().shape == (1, 7)
    assert view.dataframe().loc["mnt-reform", ft.AMOUNT] == pytest.approx(2195.5645, abs=1e-3)
    assert view.dataframe().loc["mnt-reform", ft.UNIT] == "g"
    assert view.dataframe().loc["mnt-reform", ft.TYPE] == "device"


def test_mnt_reform_analyse_Dpmr():
    view = mnt_reform.analyse("Dpmr").view()
    assert view.sum_of_all_amounts() == pytest.approx(84443.3869, abs=1e-3)
    assert "Resources" in view.title()
    assert view.dataframe().shape == (1, 7)
    assert view.dataframe().loc["mnt-reform", ft.AMOUNT] == pytest.approx(84443.3869, abs=1e-3)
    assert view.dataframe().loc["mnt-reform", ft.UNIT] == "g"
    assert view.dataframe().loc["mnt-reform", ft.TYPE] == "device"


def test_mnt_reform_analyse_dPm():
    view = mnt_reform.analyse("dPm").view_top_other(20)
    assert view.number_of_all_entities() == 233
    assert view.sum_of_all_amounts() == pytest.approx(2195.5644, abs=1e-3)
    assert view.dataframe().shape == (21, 9)
    assert view.dataframe().index[0] == "jgne-JGCFR18650"
    assert view.dataframe().loc["jgne-JGCFR18650", ft.RANK] == 1
    assert view.dataframe().loc["jgne-JGCFR18650", ft.AMOUNT] == pytest.approx(332.0000, abs=1e-3)
    assert view.dataframe().loc["jgne-JGCFR18650", ft.UNIT] == "g"
    assert view.dataframe().loc["jgne-JGCFR18650", ft.SHARE] == pytest.approx(15.1214, abs=1e-3)
    assert view.dataframe().loc["jgne-JGCFR18650", ft.TYPE] == "part"
    # Both smooth-SMS-ZZ-219-L and smooth-SMS-ZZ-219-R are equal in terms of weight and materials
    #  and therefore may be the last element in the list
    assert view.dataframe().index[19] == "smooth-SMS-ZZ-219-L" or \
           view.dataframe().index[19] == "smooth-SMS-ZZ-219-R"
    assert view.dataframe().iloc[19][ft.RANK] == 20
    assert view.dataframe().iloc[19][ft.AMOUNT] == pytest.approx(20.0000, abs=1e-3)
    assert view.dataframe().iloc[19][ft.SHARE] == pytest.approx(0.9109, abs=1e-3)
    assert view.dataframe().index.get_loc(ft.OTHER_SLUG) == 20
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.RANK] == 21
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(237.0750, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.UNIT] == "g"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(10.7979, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.TYPE] == "part"


def test_mnt_reform_analyse_dpM():
    view = mnt_reform.analyse("dpM").view_top_other(50)
    assert view.number_of_all_entities() == 248
    assert view.sum_of_all_amounts() == pytest.approx(2195.5644, abs=1e-3)
    assert view.dataframe().shape == (51, 10)
    assert view.dataframe().index[0] == "unknown-a6061"
    assert view.dataframe().loc["unknown-a6061", ft.RANK] == 1
    assert view.dataframe().loc["unknown-a6061", ft.AMOUNT] == pytest.approx(742.8530, abs=1e-3)
    assert view.dataframe().loc["unknown-a6061", ft.UNIT] == "g"
    assert view.dataframe().loc["unknown-a6061", ft.SHARE] == pytest.approx(33.8343, abs=1e-3)
    assert view.dataframe().loc["unknown-a6061", ft.TYPE] == "material"
    assert view.dataframe().index[49] == "unknown-copper_oxide"
    assert view.dataframe().loc["unknown-copper_oxide", ft.RANK] == 50
    assert view.dataframe().loc["unknown-copper_oxide", ft.AMOUNT] == pytest.approx(1.1450, abs=1e-3)
    assert view.dataframe().loc["unknown-copper_oxide", ft.SHARE] == pytest.approx(0.0521, abs=1e-3)
    assert view.dataframe().index.get_loc(ft.OTHER_SLUG) == 50
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.RANK] == 51
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(17.4728, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.UNIT] == "g"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(0.7958, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.TYPE] == "material"


def test_mnt_reform_analyse_dPmr():
    view = mnt_reform.analyse("dPmr").view_top_other(20)
    assert view.number_of_all_entities() == 233
    assert view.sum_of_all_amounts() == pytest.approx(84443.3869, abs=1e-3)
    assert view.dataframe().shape == (21, 9)
    assert view.dataframe().index[0] == "pcbway-mnt_reform_motherboard_pcb"
    assert view.dataframe().loc["pcbway-mnt_reform_motherboard_pcb", ft.AMOUNT] == pytest.approx(11611.9668, abs=1e-3)
    assert view.dataframe().loc["pcbway-mnt_reform_motherboard_pcb", ft.SHARE] == pytest.approx(13.7512, abs=1e-3)
    assert view.dataframe().index[19] == "innolux-N125HCE-GN1-glass"
    assert view.dataframe().loc["innolux-N125HCE-GN1-glass", ft.AMOUNT] == pytest.approx(610.8932, abs=1e-3)
    assert view.dataframe().loc["innolux-N125HCE-GN1-glass", ft.SHARE] == pytest.approx(0.7234, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(18360.8626, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(21.7434, abs=1e-3)


def test_mnt_reform_analyse_dpmR():
    view = mnt_reform.analyse("dpmR").view_top_other(50)
    assert view.number_of_all_entities() == 60
    assert view.sum_of_all_amounts() == pytest.approx(84443.3869, abs=1e-3)
    assert view.dataframe().shape == (51, 8)
    assert view.dataframe().index[0] == "unknown-copper_ore"
    assert view.dataframe().loc["unknown-copper_ore", ft.AMOUNT] == pytest.approx(27165.9648, abs=1e-3)
    assert view.dataframe().loc["unknown-copper_ore", ft.SHARE] == pytest.approx(32.1706, abs=1e-3)
    assert view.dataframe().index[1] == "unknown-gold_ground"
    assert view.dataframe().loc["unknown-gold_ground", ft.AMOUNT] == pytest.approx(25890.9570, abs=1e-3)
    assert view.dataframe().loc["unknown-gold_ground", ft.SHARE] == pytest.approx(30.6607, abs=1e-3)
    assert view.dataframe().index[49] == "unknown-sulfur_ore"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(0.7624, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(0.0009, abs=1e-3)


def test_mnt_reform_analyse_dpmrC():
    view = mnt_reform.analyse("dpmrC").view_top_other(50)
    assert view.number_of_all_entities() == 165
    assert view.sum_of_all_amounts() == pytest.approx(84443.3869, abs=1e-3)
    assert view.dataframe().shape == (51, 9)
    assert view.dataframe().index[0] == "china"
    assert view.dataframe().loc["china", ft.AMOUNT] == pytest.approx(11355.3577, abs=1e-3)
    assert view.dataframe().loc["china", ft.UNIT] == "g"
    assert view.dataframe().loc["china", ft.SHARE] == pytest.approx(13.4473, abs=1e-3)
    assert view.dataframe().loc["china", ft.TYPE] == "location"
    assert view.dataframe().index[49] == "suriname"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(3108.8042, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.UNIT] == "g"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(3.6815, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.TYPE] == "location"


def test_mnt_reform_analyse_dpmRC():
    view = mnt_reform.analyse("dpmRC").view_top_other(20)
    assert view.number_of_all_entities() == 1368
    assert view.sum_of_all_amounts() == pytest.approx(84443.3869, abs=1e-3)
    assert view.dataframe().shape == (21, 12)
    assert view.dataframe().index[0] == "unknown-copper_ore|chile"
    assert view.dataframe().loc["unknown-copper_ore|chile", ft.AMOUNT] == pytest.approx(7135.3351, abs=1e-3)
    assert view.dataframe().loc["unknown-copper_ore|chile", ft.UNIT] == "g"
    assert view.dataframe().loc["unknown-copper_ore|chile", ft.SHARE] == pytest.approx(8.4498, abs=1e-3)
    assert view.dataframe().loc["unknown-copper_ore|chile", ft.TYPE] == "resource-location"
    assert view.dataframe().index[1] == "unknown-tin_ore|china"
    assert view.dataframe().loc["unknown-tin_ore|china", ft.AMOUNT] == pytest.approx(3332.9626, abs=1e-3)
    assert view.dataframe().loc["unknown-tin_ore|china", ft.SHARE] == pytest.approx(3.947, abs=1e-3)
    assert view.dataframe().index[19] == "unknown-copper_ore|australia"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(42909.5659, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.UNIT] == "g"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(50.8146, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.TYPE] == "resource-location"


def test_mnt_reform_analyse_Di_childlabour():
    view = mnt_reform.analyse("Di[child-labour]").view()
    assert view.number_of_all_entities() == 1
    assert view.sum_of_all_amounts() == pytest.approx(1746.2082, abs=1e-3)
    assert view.dataframe().shape == (1, 7)
    assert "Indicators" in view.title()
    assert "child labour" in view.title()
    assert view.dataframe().loc["mnt-reform", ft.AMOUNT] == pytest.approx(1746.2082, abs=1e-3)
    assert view.dataframe().loc["mnt-reform", ft.UNIT] == "g"
    assert view.dataframe().loc["mnt-reform", ft.TYPE] == "device"


def test_mnt_reform_analyse_Di_childlabourinverse():
    view = mnt_reform.analyse("Di[child-labour-inverse]").view()
    assert view.dataframe().loc["mnt-reform", ft.AMOUNT] == pytest.approx(41008.2475, abs=1e-3)


def test_mnt_reform_analyse_Di_childlabourunknown():
    view = mnt_reform.analyse("Di[child-labour-unknown]").view()
    assert view.dataframe().loc["mnt-reform", ft.AMOUNT] == pytest.approx(41688.9312, abs=1e-3)


def test_mnt_reform_analyse_Pi_childlabour():
    view = mnt_reform.analyse("Pi[child-labour]").view_top_other(10)
    assert view.number_of_all_entities() == 233
    assert view.sum_of_all_amounts() == pytest.approx(1746.2082)
    assert view.dataframe().shape == (11, 9)
    assert view.dataframe().index[0] == "pcbway-mnt_reform_motherboard_pcb"
    assert view.dataframe().loc["pcbway-mnt_reform_motherboard_pcb", ft.AMOUNT] == pytest.approx(253.3559, abs=1e-3)
    assert view.dataframe().loc["pcbway-mnt_reform_motherboard_pcb", ft.UNIT] == "g"
    assert view.dataframe().loc["pcbway-mnt_reform_motherboard_pcb", ft.SHARE] == pytest.approx(14.5089, abs=1e-3)
    assert view.dataframe().loc["pcbway-mnt_reform_motherboard_pcb", ft.TYPE] == "part"
    assert view.dataframe().index[9] == "neuform-mnt_MREFCSCB20R01"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(617.9731, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.UNIT] == "g"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(35.3894, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.TYPE] == "part"


def test_mnt_reform_analyse_Mi_childlabour():
    view = mnt_reform.analyse("Mi[child-labour]").view_top_other(10)
    assert view.dataframe().index[0] == "unknown-gold"
    assert view.dataframe().loc["unknown-gold", ft.AMOUNT] == pytest.approx(787.5418, abs=1e-3)
    assert view.dataframe().loc["unknown-gold", ft.SHARE] == pytest.approx(45.1001, abs=1e-3)
    assert view.dataframe().index[1] == "unknown-copper"
    assert view.dataframe().loc["unknown-copper", ft.AMOUNT] == pytest.approx(436.4257, abs=1e-3)
    assert view.dataframe().loc["unknown-copper", ft.SHARE] == pytest.approx(24.9928, abs=1e-3)
    assert view.dataframe().index[9] == "unknown-brass"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(27.0912, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(1.5514, abs=1e-3)


def test_mnt_reform_analyse_Ri_childlabour():
    view = mnt_reform.analyse("Ri[child-labour]").view_top_other(10)
    assert view.dataframe().index[0] == "unknown-gold_ground"
    assert view.dataframe().loc["unknown-gold_ground", ft.AMOUNT] == pytest.approx(787.5418, abs=1e-3)
    assert view.dataframe().loc["unknown-gold_ground", ft.SHARE] == pytest.approx(45.1001, abs=1e-3)
    assert view.dataframe().index[2] == "unknown-tin_ore"
    assert view.dataframe().loc["unknown-tin_ore", ft.AMOUNT] == pytest.approx(182.61, abs=1e-3)
    assert view.dataframe().loc["unknown-tin_ore", ft.SHARE] == pytest.approx(10.4575, abs=1e-3)
    assert view.dataframe().index[9] == "unknown-ilmenite_ore"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(16.2228, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(0.9290, abs=1e-3)


def test_mnt_reform_analyse_Ci_childlabour():
    view = mnt_reform.analyse("Ci[child-labour]").view_top_other(10)
    assert view.dataframe().index[0] == "guinea"
    assert view.dataframe().loc["guinea", ft.AMOUNT] == pytest.approx(256.3852, abs=1e-3)
    assert view.dataframe().loc["guinea", ft.SHARE] == pytest.approx(14.6824, abs=1e-3)
    assert view.dataframe().index[1] == "peru"
    assert view.dataframe().loc["peru", ft.AMOUNT] == pytest.approx(235.1548, abs=1e-3)
    assert view.dataframe().loc["peru", ft.SHARE] == pytest.approx(13.4666, abs=1e-3)
    assert view.dataframe().index[9] == "cote_divoire"
    assert view.dataframe().loc["cote_divoire", ft.AMOUNT] == pytest.approx(48.3191, abs=1e-3)
    assert view.dataframe().loc["cote_divoire", ft.SHARE] == pytest.approx(2.7671, abs=1e-3)
    assert view.dataframe().index[10] == ft.OTHER_SLUG
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(476.3177, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(27.2773, abs=1e-3)


def test_mnt_reform_analyse_PMi_childlabour():
    view = mnt_reform.analyse("PMi[child-labour]").view_top_other(10)
    assert view.dataframe().index[0] == "pcbway-mnt_reform_keyboard_pcb|unknown-gold"
    assert view.dataframe().loc["pcbway-mnt_reform_keyboard_pcb|unknown-gold", ft.AMOUNT] == \
        pytest.approx(174.2144, abs=1e-3)
    assert view.dataframe().index[9] == "neuform-mnt_MREFCSCB20R01|unknown-a6061"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(807.2867, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(46.2308, abs=1e-3)


def test_mnt_reform_analyse_MCi_childlabour():
    view = mnt_reform.analyse("MCi[child-labour]").view_top_other(10)
    assert view.number_of_all_entities() == 4921
    assert view.sum_of_all_amounts() == pytest.approx(1746.2082)
    assert view.dataframe().shape == (11, 14)
    assert view.dataframe().index[0] == "unknown-copper|congo_dr"
    assert view.dataframe().loc["unknown-copper|congo_dr", ft.AMOUNT] == pytest.approx(164.5713, abs=1e-3)
    assert view.dataframe().index[1] == "unknown-a6061|guinea"
    assert view.dataframe().loc["unknown-a6061|guinea", ft.AMOUNT] == pytest.approx(129.0518, abs=1e-3)
    assert view.dataframe().index[9] == "unknown-gold|cote_divoire"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(818.433, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(46.8692, abs=1e-3)


def test_mnt_reform_analyse_RCi_childlabour():
    view = mnt_reform.analyse("RCi[child-labour]").view_top_other(10)
    assert view.number_of_all_entities() == 659
    assert view.sum_of_all_amounts() == pytest.approx(1746.2082)
    assert view.dataframe().shape == (11, 12)
    assert view.dataframe().index[0] == "unknown-copper_ore|congo_dr"
    assert view.dataframe().loc["unknown-copper_ore|congo_dr", ft.AMOUNT] == pytest.approx(170.5965, abs=1e-3)
    assert view.dataframe().index[9] == "unknown-gold_ground|cote_divoire"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(788.9673, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(45.1817, abs=1e-3)


def test_mnt_reform_analyse_PMCi_childlabour():
    view = mnt_reform.analyse("PMCi[child-labour]").view_top_other(10)
    assert view.number_of_all_entities() == 68979
    assert view.sum_of_all_amounts() == pytest.approx(1746.2082)
    assert view.dataframe().shape == (11, 17)
    assert view.dataframe().index[0] == "neuform-mnt_MREFCMBT20R01|unknown-a6061|guinea"
    assert view.dataframe().loc["neuform-mnt_MREFCMBT20R01|unknown-a6061|guinea", ft.AMOUNT] ==\
        pytest.approx(53.6738, abs=1e-3)
    assert view.dataframe().loc["neuform-mnt_MREFCMBT20R01|unknown-a6061|guinea", ft.UNIT] == "g"
    assert view.dataframe().loc["neuform-mnt_MREFCMBT20R01|unknown-a6061|guinea", ft.SHARE] ==\
        pytest.approx(3.0737, abs=1e-3)
    assert view.dataframe().loc["neuform-mnt_MREFCMBT20R01|unknown-a6061|guinea", ft.TYPE] == "part-material-location"
    assert view.dataframe().index[9] == "unknown-cable_229981|unknown-copper|peru"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.AMOUNT] == pytest.approx(1411.8558, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.UNIT] == "g"
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.SHARE] == pytest.approx(80.8527, abs=1e-3)
    assert view.dataframe().loc[ft.OTHER_SLUG, ft.TYPE] == "part-material-location"


def test_mnt_reform_analyse_percentage_Dci_childlabour_Dc():
    value = mnt_reform.analyse("Dci[child-labour]").view().percentageof(
                              mnt_reform.analyse("Dc").view()).value()
    assert value == pytest.approx(2.0679041, abs=1e-7)


def test_mnt_reform_analyse_conflict_minerals():
    analysis = mnt_reform.analyse("Pmr[unknown-gold_ground|unknown-tin_ore|unknown-tantalum_ore|unknown-tungsten_ore]"
                                  "c[congo_dr|rwanda|uganda|burundi]")
    view = analysis.view().grouped("part manufacturer").top(10)
    assert view.dataframe().shape == (10, 6)
    assert view.dataframe().iloc[0, 0] == "Unknown"
    assert view.dataframe().iloc[0, 1] == pytest.approx(543.3904, abs=1e-3)
    assert view.dataframe().iloc[1, 0] == "PCBWay"
    assert view.dataframe().iloc[1, 1] == pytest.approx(135.4692, abs=1e-3)
    assert view.dataframe().iloc[9, 0] == "Texas Instruments"
    assert view.dataframe().iloc[9, 1] == pytest.approx(10.4656, abs=1e-3)


def test_nager_it_mouse_analyse_Dm():
    view = nager_it_mouse.analyse("Dm").view()
    assert view.number_of_all_entities() == 1
    assert view.sum_of_all_amounts() == pytest.approx(66.8051, abs=1e-3)


def test_nager_it_mouse_analyse_Dr():
    view = nager_it_mouse.analyse("Dr").view()
    assert view.number_of_all_entities() == 1
    assert view.sum_of_all_amounts() == pytest.approx(4349.4881, abs=1e-3)


def test_mnt_reform_explain_c_peru_r_tinground():
    explanation = mnt_reform.explain("c[peru]r[unknown-tin_ore]")
    text = explanation.write()
    assert all(keyword in text for keyword in {"Laptop", "5602g", "Peru", "989g", "Tin Ore"})


def test_mnt_reform_explain_c_peru_i_forcedlabour_p_keyboardpcb():
    explanation = mnt_reform.explain("c[peru]i[forced-labour]p[pcbway-mnt_reform_keyboard_pcb]")
    text = explanation.write()
    assert all(keyword in text for keyword in {"Laptop", "5602g", "Peru", "40g", "Forced Labour", "3g", "PCB Keyboard"})


def test_mnt_reform_explain_r_bauxite_p_screenfrontcase_c_guinea():
    explanation = mnt_reform.explain("r[unknown-bauxite_ore]p[neuform-mnt_MREFCSCF20R01]c[guinea]")
    text = explanation.write()
    assert all(keyword in text for keyword in {"Laptop", "4859g", "Bauxite", "488g",
                                               "Screen Front Case", "112g", "Guinea"})
