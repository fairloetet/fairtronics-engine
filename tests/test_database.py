import pandas as pd
from fairtronics_engine.database import PART, MATERIAL
from fairtronics_engine.model import PartAssignment
from fairtronics_engine.statics import SLUG
from tests.test_helper import database, manufacturer, portfolio, device, part, material, Slug
from slugify import slugify
import pytest


@pytest.fixture
def data():
    return database(
        portfolio("portfolio1", devices={"device1": 1.0, "device2": 1.0}),
        device("device1", parts={"part1": 1.0, "part2": 1.0}),
        device("device2", parts={"part1": 1.0, "part2": 1.0}),
        manufacturer("man1", "Man"),
        part("part1", materials={"material1": 1.0, "material2": 1.0}),
        part("part2", materials={"material1": 1.0}, manufacturer="man1"),
        material("material1", category="cat", cas="cas1"),
        material("material2", cas="cas2")
    )


def test_list_with_tier(data):
    db_list = data.list(PART)
    assert len(db_list.dataframe().index) == 2
    assert "part1" in db_list.dataframe().index
    assert db_list.dataframe()["manufacturer"]["part2"] == "Man"


def test_list_with_int_as_tier(data):
    db_list_int = data.list(MATERIAL.position())
    db_list = data.list(MATERIAL)
    pd.testing.assert_frame_equal(db_list.dataframe(), db_list_int.dataframe())


def test_add_device_without_parts(data):
    device = data.add_device(slug="d3", name="device3", short_name="dev3", manufacturer="man1",
                                  parts={})
    assert device.slug == "d3"
    assert device == data.devices[Slug("d3")]
    assert len(device.parts) == 0
    assert device.name == "device3"
    assert device.short_name == "dev3"
    assert device.manufacturer == "man1"
    assert device.dependency is None


def test_add_device_with_known_part(data):
    device = data.add_device(slug="d4", name="device4", short_name="dev4", manufacturer="man1",
                             parts={"part1": 3.0})
    assert len(device.parts) == 1
    part_assignment = device.parts[Slug("part1")]
    assert isinstance(part_assignment, PartAssignment)
    assert part_assignment.entity_slug == Slug("d4")
    assert part_assignment.assigned_slug == Slug("part1")
    assert part_assignment.assigned_slug == "part1"
    assert part_assignment.amount == 3.0
    assert part_assignment.source == ""


def test_add_device_with_unknown_part(data):
    with pytest.raises(KeyError):
        data.add_device(slug="d5", name="device5", short_name="dev5", manufacturer="man1",
                        parts={"part3": 1.0})


def test_add_device_with_two_parts(data):
    device = data.add_device(slug="d6", name="device6", short_name="dev6", manufacturer="man1",
                             parts={"part1": 3.0, "part2": 2.5})
    assert len(device.parts) == 2
    assert device.parts[Slug("part2")].assigned_slug == Slug("part2")


def test_add_device_with_generated_slug(data):
    device = data.add_device(name="Device7", short_name="Dev 7", manufacturer="man1", parts={})
    assert device.slug == slugify("man1-Dev 7")


def test_add_device_with_known_slug(data):
    with pytest.raises(KeyError):
        data.add_device(slug="device1", name="Some Device", short_name="SmDvc", manufacturer="man1", parts={})


def test_add_device_with_unknown_manufacturer(data):
    with pytest.raises(KeyError):
        data.add_device(slug="d9", name="device9", short_name="dev9", parts={},
                        manufacturer="man2")


def test_add_device_without_manufacturer(data):
    device = data.add_device(slug="d9", name="device9", short_name="dev9", parts={})
    assert device.manufacturer == data.unknown_manufacturer


def test_add_device_without_short_name(data):
    device = data.add_device(slug="d10", name="Device10", manufacturer="man1", parts={})
    assert device.short_name == device.name


def test_add_device_simple(data):
    device = data.add_device("Device11", manufacturer="man1", parts={})
    assert device.slug == slugify("man1-Device11")
    assert device.short_name == device.name


def test_add_part_to_device(data):
    device = data.add_device(slug="d12", name="device12", short_name="dev12", manufacturer="man1",
                             parts={"part1": 3.0})
    part = data.add_part_to_device("d12", "part2", amount=2.5)
    assert part in device.parts
    assert len(device.parts) == 2
    assert device.parts[Slug("part2")].assigned_slug == Slug("part2")


def test_add_part_again_to_device(data):
    device = data.add_device(slug="d13", name="device13", short_name="dev13", manufacturer="man1",
                             parts={"part1": 3.0})
    data.add_part_to_device("d13", "part1", amount=2.5)
    assert len(device.parts) == 1
    assert device.parts[Slug("part1")].amount == 3.0 + 2.5


def test_add_unknown_part_to_device(data):
    data.add_device(slug="d14", name="device14", short_name="dev14", manufacturer="man1", parts={})
    with pytest.raises(KeyError):
        data.add_part_to_device("d14", "part3", amount=2.5)


def test_add_part_to_unknown_device(data):
    with pytest.raises(KeyError):
        data.add_part_to_device("unknown", "part1", amount=1.0)


def test_delete_portfolio_part(data):
    data.delete_device("device1")
    assert "device1" not in data.devices
    for pf in data.portfolios.values():
        assert "device1" not in pf.devices


def test_delete_added_part(data):
    device = data.add_device("added_device", manufacturer="man1", parts={})
    data.delete_device(device.slug)
    assert "added_device" not in data.devices


def test_delete_unknown_part(data):
    with pytest.raises(KeyError):
        data.delete_device("unknown")


def test_add_manufacturer_standard(data):
    manufacturer = data.add_manufacturer(name="Manufacturer2", short_name="Man2", slug="man2")
    assert manufacturer.slug in data.manufacturers


def test_add_manufacturer_simple(data):
    manufacturer = data.add_manufacturer("Manufacturer3")
    assert manufacturer.name == "Manufacturer3"
    assert manufacturer.name == manufacturer.short_name
    assert manufacturer.slug == slugify(manufacturer.short_name)


def test_add_manufacturer_with_known_slug(data):
    with pytest.raises(KeyError):
        data.add_manufacturer(name="Some Manufacturer", slug="man1")
