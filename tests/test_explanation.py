import pandas as pd
import pytest
from fairtronics_engine.statics import NAME, OTHER_BOTTOM_SLUG, OTHER_NAME, OTHER_SHORT, OTHER_SLUG, \
                                       WITH_OTHER_SINGLE, WITH_OTHER_TOP_BOTTOM, OTHER_BOTTOM_NAME, \
                                       OTHER_TOP_NAME, OTHER_TOP_SLUG, WITH_OTHER_TOP_BOTTOM_SINGLE, \
                                       WITH_OTHER_NONE
from tests.test_helper import database, portfolio, device, part, material, resource, location, indicator, \
                              dataframe, entity_row, other_row, \
                              Database, Slug, Entity, Chain, Tier, Portfolio, DeviceAssignment, Device, \
                              PartAssignment, Part, MaterialAssignment, Material, ResourceAssignment, Resource, \
                              LocationAssignment, Location, IndicatorAssignment, Indicator, \
                              PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, LOCATION, INDICATOR, CHAIN
from fairtronics_engine.unit import Unit
from fairtronics_engine.analysis import StaticCube
from fairtronics_engine import AMOUNT, NAME, SHORT, SHARE, RANK, TYPE, UNIT, \
                               SHOW_STANDARD_ALL


data = database(
        portfolio("portfolio1", "Portfolio1", "Ptf1", {"device1": 1.0}),
        device("device1", "Device1", "Dev1", {"part1": 0.6, "part2": 0.4}),
        part("part1", "Part1", "Pt1", {"material1": 0.6, "material2": 0.4}),
        part("part2", "Part2", "Pt2", {"material1": 0.3, "material2": 0.7}),
        material("material1", "Material1", "Mat1", {"resource1": 10.0}),
        material("material2", "Material2", "Mat2", {"resource2": 10.0}),
        resource("resource1", "Resource1", "Res1", {"location1": 0.8, "location2": 0.2}),
        resource("resource2", "Resource2", "Res2", {"location1": 0.6, "location2": 0.4}),
        location("location1", "Location1", "Ctry1", {"indicator1": 0.05, "indicator2": 0.02,
                                                  "indicator3": 0.01, "indicator4": 0.001}),
        location("location2", "Location2", "Ctry2", {"indicator1": 0.2, "indicator2": 0.1,
                                                  "indicator3": 0.01, "indicator4": 0.001}),
        indicator("indicator1", "Indicator1", "Ind1"),
        indicator("indicator2", "Indicator2", "Ind2"),
        indicator("indicator3", "Indicator3", "Ind3"),
        indicator("indicator4", "Indicator4", "Ind4")
    )


def test_explanation_single():
    unit = Unit(data, DEVICE, "device1")
    explanation = unit.explain("p[part1]")
    assert len(explanation.sentences) == 1
    assert "Dev1" in explanation.sentences[0]
    assert "Pt1" in explanation.sentences[0]
    assert "0.6" in explanation.sentences[0]


def test_explanation_double():
    unit = Unit(data, DEVICE, "device1")
    explanation = unit.explain("m[material2]p[part1]")
    assert len(explanation.sentences) == 2
    assert "Dev1" in explanation.sentences[0]
    assert "Mat2" in explanation.sentences[0]
    assert str(0.6*0.4 + 0.4*0.7) in explanation.sentences[0]
    assert "Pt1" in explanation.sentences[1]
    assert str(0.6*0.4) in explanation.sentences[1]


def test_explanation_only_devices():
    unit = Unit(data, PART, ".*")
    with pytest.raises(ValueError):
        unit.explain("whatever")
