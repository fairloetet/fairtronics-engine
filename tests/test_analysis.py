import pandas as pd
import pytest
from fairtronics_engine.statics import NAME, OTHER_BOTTOM_SLUG, OTHER_NAME, OTHER_SHORT, OTHER_SLUG, \
                                       WITH_OTHER_SINGLE, WITH_OTHER_TOP_BOTTOM, OTHER_BOTTOM_NAME, \
                                       OTHER_TOP_NAME, OTHER_TOP_SLUG, WITH_OTHER_TOP_BOTTOM_SINGLE, \
                                       WITH_OTHER_NONE
from tests.test_helper import database, portfolio, device, part, material, resource, location, indicator, \
                              dataframe, entity_row, other_row, \
                              Database, Slug, Entity, Chain, Tier, Portfolio, DeviceAssignment, Device, \
                              PartAssignment, Part, MaterialAssignment, Material, ResourceAssignment, Resource, \
                              LocationAssignment, Location, IndicatorAssignment, Indicator, \
                              PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, LOCATION, INDICATOR, CHAIN
from fairtronics_engine.unit import Unit
from fairtronics_engine.analysis import StaticCube
from fairtronics_engine import AMOUNT, NAME, SHORT, SHARE, RANK, TYPE, UNIT, \
                               SHOW_STANDARD_ALL


data = database(
        portfolio("portfolio1", "Portfolio1", "Ptf1", {"device1": 1.0}),
        device("device1", "Device1", "Dev1", {"part1": 0.6, "part2": 0.4}),
        part("part1", "Part1", "Pt1", {"material1": 0.6, "material2": 0.4}),
        part("part2", "Part2", "Pt2", {"material1": 0.3, "material2": 0.7}),
        material("material1", "Material1", "Mat1", {"resource1": 10.0}),
        material("material2", "Material2", "Mat2", {"resource2": 10.0}),
        resource("resource1", "Resource1", "Res1", {"location1": 0.8, "location2": 0.2}),
        resource("resource2", "Resource2", "Res2", {"location1": 0.6, "location2": 0.4}),
        location("location1", "Location1", "Ctry1", {"indicator1": 0.05, "indicator2": 0.02,
                                                  "indicator3": 0.01, "indicator4": 0.001}),
        location("location2", "Location2", "Ctry2", {"indicator1": 0.2, "indicator2": 0.1,
                                                  "indicator3": 0.01}),
        indicator("indicator1", "Indicator1", "Ind1"),
        indicator("indicator2", "Indicator2", "Ind2"),
        indicator("indicator3", "Indicator3", "Ind3"),
        indicator("indicator4", "Indicator4", "Ind4")
    )


def test_analysis_init():
    unit = Unit(data, PART, "part.*")
    analysis = unit.analyse("P")
    assert analysis.unit_tier is PART
    assert "Part1" in analysis.unit_names
    assert "Part2" in analysis.unit_names
    assert analysis.unit_tier_name == PART.name_plural


def test_analysis_spec():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse("dP[part.*]mr[resource2]")
    assert analysis.selection == [PART]
    assert analysis.filters == {PART: {Slug("part1"), Slug("part2")}, RESOURCE: {Slug("resource2")}}
    assert analysis.depth == RESOURCE


def test_analysis_cube():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[DEVICE, PART, INDICATOR])
    assert isinstance(analysis.cube, StaticCube)
    assert analysis.cube.shape() == (1, 2, 4)  # 1 device, 2 parts, 4 indicators


# TODO Needs way more tests
def test_analysis_view_default():
    unit = Unit(data, PORTFOLIO, "portfolio1")
    # Dm[material1]
    analysis = unit.analyse(selection=[DEVICE], depth=MATERIAL, filter_spec={MATERIAL: "material1"})
    view = analysis.view()
    assert PORTFOLIO.name in view.object_title
    assert "Portfolio1" in view.object_title
    assert DEVICE.name in " ".join(view.title_fragments)
    assert MATERIAL.name in " ".join(view.title_fragments)
    assert "Material1" in " ".join(view.title_fragments)
    assert isinstance(view.df, pd.DataFrame)
    assert view.weight_count == len(view.df.index)
    assert view.weight_count == 1
    assert view.weight_sum == view.df[AMOUNT].sum()
    # 0.6 = part1 of device1, 0.6 = material1 of part1, 0.4 = part2 of device1, 0.3 = material1 of part2
    assert view.weight_sum == 0.6 * 0.6 + 0.4 * 0.3
    assert view.df.index[0] == "device1"
    assert view.df.at["device1", AMOUNT] == view.weight_sum
    assert view.df.at["device1", NAME] == "Device1"
    assert view.df.at["device1", SHORT] == "Dev1"
    assert view.df.at["device1", SHARE] == 100.0
    assert view.df.at["device1", RANK] == 1
    assert view.df.at["device1", TYPE] == DEVICE.name.lower()


def test_analysis_view_default_other():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=0, end=2)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            )[[NAME]]
        )


def test_analysis_view_no_other():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=0, end=2, with_other=WITH_OTHER_NONE)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            )[[NAME]]
        )


def test_analysis_view_other_single():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=0, end=2, with_other=WITH_OTHER_SINGLE)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            other_row(OTHER_SLUG, name=OTHER_NAME),
            )[[NAME]]
        )


def test_analysis_view_other_bottom():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=0, end=2, with_other=WITH_OTHER_TOP_BOTTOM)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            other_row(OTHER_BOTTOM_SLUG, name=OTHER_BOTTOM_NAME)
            )[[NAME]]
        )


def test_analysis_view_other_top():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=2, end=4, with_other=WITH_OTHER_TOP_BOTTOM)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            other_row(OTHER_TOP_SLUG, name=OTHER_TOP_NAME),
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            entity_row(INDICATOR, "indicator4", name="Indicator4")
            )[[NAME]]
        )


def test_analysis_view_other_top_bottom():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=2, end=3, with_other=WITH_OTHER_TOP_BOTTOM)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            other_row(OTHER_TOP_SLUG, name=OTHER_TOP_NAME),
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            other_row(OTHER_BOTTOM_SLUG, name=OTHER_BOTTOM_NAME)
            )[[NAME]]
        )


def test_analysis_view_other_single_over_top_bottom():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=0, end=2, with_other=WITH_OTHER_TOP_BOTTOM_SINGLE)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"), 
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            other_row(OTHER_SLUG, name=OTHER_NAME),
            )[[NAME]]
        )


def test_analysis_view_other_top_bottom_over_single():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=2, end=3, with_other=WITH_OTHER_TOP_BOTTOM_SINGLE)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            other_row(OTHER_TOP_SLUG, name=OTHER_TOP_NAME),
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            other_row(OTHER_BOTTOM_SLUG, name=OTHER_BOTTOM_NAME)
            )[[NAME]]
        )


def test_analysis_view_ascending():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=0, end=2, ascending=True, with_other=WITH_OTHER_SINGLE)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator4", name="Indicator4"),
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            other_row(OTHER_SLUG, name=OTHER_NAME),
            )[[NAME]]
        )


def test_analysis_view_negative_start():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    view = analysis.view(start=-2)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            entity_row(INDICATOR, "indicator4", name="Indicator4")
            )[[NAME]]
        )


def test_analysis_view_no_cutoff():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR], filter_spec={LOCATION: "location2"})
    view = analysis.view(cutoff=None)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            entity_row(INDICATOR, "indicator4", name="Indicator4")  # Although not in location2, 0.0 is included here
            )[[NAME]]
        )


def test_analysis_view_cutoff_greater_zero():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    # With too-low cutoff, Other is available. (Already tested, included only for comparison.)
    view = analysis.view(start=0, end=3, cutoff=0.0, with_other=WITH_OTHER_TOP_BOTTOM)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            entity_row(INDICATOR, "indicator3", name="Indicator3"),
            other_row(OTHER_BOTTOM_SLUG, name=OTHER_BOTTOM_NAME, amount=0.01),
            )[[NAME]]
        )
    # ... but with relevant cutoff, Other isn't. (Note that the cutoff is an exclusive threshold, i.e. 0.01 is cut away.)
    view = analysis.view(start=0, end=3, cutoff=0.01, with_other=WITH_OTHER_TOP_BOTTOM)
    pd.testing.assert_frame_equal(
        view.df[[NAME]],
        dataframe(
            entity_row(INDICATOR, "indicator1", name="Indicator1"),
            entity_row(INDICATOR, "indicator2", name="Indicator2"),
            entity_row(INDICATOR, "indicator3", name="Indicator3")
            )[[NAME]]
        )


def test_analysis_view_other_wrong_option():
    unit = Unit(data, DEVICE, "device1")
    analysis = unit.analyse(selection=[INDICATOR])
    with pytest.raises(ValueError):
        analysis.view(start=2, end=3, with_other="alien option")
