from fairtronics_engine.model import Portfolio, Device, Part, Material, Resource, Location, Indicator, Entity
from fairtronics_engine.statics import *
from fairtronics_engine.cube import Cube
from typing import List, Dict, Tuple
import numpy as np
import re
from fairtronics_engine.database import Database, PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, LOCATION, INDICATOR
# ordered_set is available in the conda-forge package/repository/channel https://conda.anaconda.org/conda-forge
# TODO Maybe replaceable by dict (with index as value) which is guarenteed to preserve order since python 3.7
from ordered_set import OrderedSet

DTYPE = np.float32

class DynamicCube(Cube):
    """
    Note that this currently isn't a correct implementation of a Cube
    It's the old implementation of Engine V1.0 with dynamically expanding and shrinking cube size
    and only rescued here as code for later use
    """
    
    database: Database
    cube: np.ndarray
    portfolios: OrderedSet[Portfolio] = None
    devices: OrderedSet[Device] = None
    parts: OrderedSet[Part] = None
    materials: OrderedSet[Material] = None
    resources: OrderedSet[Resource] = None
    countries: OrderedSet[Location] = None
    indicators: OrderedSet[Indicator] = None
    current_afar_axis: int
    root_axis: int
    focus_on: Dict[int, List[str]]  # For each axis/dimension of the cube, the list of slugs in the database
    
    def __init__(self, database: Database, focus_on: Dict[int, List[str]]):
        self.database = database
        # Set supply_tier initially to 0, i.e. nothing yet
        self.current_afar_axis = 0
        # Set root object and initialize object tables only
        root_object_type = next(iter(focus_on))  # It's always the first in this (ordered) dict
        root_object_slugs = focus_on[root_object_type]
        
        # TODO Das geht einfacher, zumal ich am Ende auch den object_dict von self hole
        # TODO Kann ich mir das auch sparen weil dies im view sowieso noch rechtzeitig aufgebaut wird?
        if root_object_type == PORTFOLIO:
            self.portfolios = OrderedSet([self.database.portfolios[os] for os in root_object_slugs])
        elif PORTFOLIO in focus_on.keys():
            self.portfolios = OrderedSet([])
        if root_object_type == DEVICE:
            self.devices = OrderedSet([self.database.devices[os] for os in root_object_slugs])
        elif DEVICE in focus_on.keys():
            self.devices = OrderedSet([])
        if root_object_type == PART:
            self.parts = OrderedSet([self.database.parts[os] for os in root_object_slugs])
        elif PART in focus_on.keys():
            self.parts = OrderedSet([])
        if root_object_type == MATERIAL:
            self.materials = OrderedSet([self.database.materials[os] for os in root_object_slugs])
        elif MATERIAL in focus_on.keys():
            self.materials = OrderedSet([])
        if root_object_type == RESOURCE:
            self.resources = OrderedSet([self.database.resources[os] for os in root_object_slugs])
        elif RESOURCE in focus_on.keys():
            self.resources = OrderedSet([])
        if root_object_type == LOCATION:
            self.countries = OrderedSet([self.database.locations[os] for os in root_object_slugs])
        elif LOCATION in focus_on.keys():
            self.countries = OrderedSet([])
        if root_object_type == INDICATOR:
            self.indicators = OrderedSet([self.database.indicators[os] for os in root_object_slugs])
        elif INDICATOR in focus_on.keys():
            self.indicators = OrderedSet([])
        
        # Retain init data
        self.root_axis = root_object_type
        self.focus_on = focus_on

        # Start initial cube: Each object geht amount of 1, regardless of the unit (i.e. 1 item oder 1 gramm)
        root_object_count = len(root_object_slugs)
        self.cube = np.array([1.0] * root_object_count, dtype=DTYPE)
        if VERBOSE:
            print("Created new inventory cube with initial shape " + str(self.cube.shape))
            object_type_name = get_entity_type_name_for_axis(root_object_type, singular=root_object_count == 1).lower()
            print("%d %s inventoried" % (root_object_count, object_type_name))
    
    
    def view(self, selection: List[int], depth: int, filters: Dict[int, str]):
        
        # First, expand self.cube according to wanted depth of supply chain if not already done before
        self.prepare_supply_tier(depth)

        # Decode slugs of filter objects to indices in the Xable's data (usually OrderedSets of EntityObjects)
        # TODO Die filter_entities werden nur zum Bilden des Titels benötigt. Kann man darauf nicht vezichten
        #      und es mit den filter_indices machen? Dann wird der Code der Unterroutine viel einfacher.
        filter_entities, filter_indices = self.map_filters_to_entities_and_data_indices(filters)

        # Slice cube to focussed-upon objects only
        restricted_cube = self.cube
        for filter_axis in filter_indices:
            restricted_cube = self.leave_filtered_indices_of_axis(restricted_cube, filter_indices[filter_axis],
                                                     self.count_axes_from_object_type_to_axis(filter_axis))
            if VERBOSE:
                print("View's restricted cube has shape %s = %d elements" %
                      (restricted_cube.shape, restricted_cube.size))

        # Reduce cube to interested-in axes
        relative_positions_of_available_axes_in_cube = self.count_axes_from_object_type_to_axes(
            self.get_available_axes())
        relative_positions_of_selection_axes_in_cube = self.count_axes_from_object_type_to_axes(selection)
        relative_position_of_supply_tier_in_cube = self.count_axes_from_object_type_to_current_afar_axis()
        view_cube = self.view_along_selection(restricted_cube,
                                     relative_positions_of_available_axes_in_cube,
                                     relative_positions_of_selection_axes_in_cube,
                                     relative_position_of_supply_tier_in_cube)        

        if VERBOSE:
            print("View's reduced cube has shape %s = %d elements" %
                  (view_cube.shape, view_cube.size))
            
        # TODO: Statt die remaining:indicaes erst zu transposen und dann in einer Schleife wieder auseinanderzunehmen
        #       könnte man vermutlich einfach remaining_indices = nonzero(view_cube) machen und dann mit
        #       view_cube[remaining_indices] den Ergebnisvektor direkt bekommen. Man muss nur aufpassen mit
        #       tupels versus nparrays und scalaren.. irgendwie, Siehe https://numpy.org/doc/stable/reference/generated/numpy.nonzero.html
        # TODO: Das hätte den Vorteil, dass das Ergebnis genau die Teile in einem Tupel, die die selection vorgibt und
        #       pro Dimension in genau der Reihenfolge wie es der filter vorgibt
        # For the dataframe, we are only interested in values of significance
        remaining_indices = self.get_significant_indices(view_cube)
        remaining_values = [view_cube[tuple(index)] if not np.isscalar(view_cube) else view_cube
                            for index in remaining_indices]
        remaining = zip(remaining_indices, remaining_values)

        # --------->
        # TODO Das Ziel sollte sein, die remaining_indices und am besten gleich auch deren Werte zurückzugeben,
        # noch idealerweise als Iterator, dann auch gerne enumeration-mäßig nebeneinander her
        # TODO Das Ergebnis sind die indices in diesem Cube. Besser wäre es, als Ergebnis einen neuen Cube zu liefern,
        #       dessen Entities noch idealer direkt aus der Datenbank geholt werden könnten
        #       d.h. es müssten auch die slugs mitgegeben werden
        return remaining, filter_entities, filter_indices


    def prepare_supply_tier(self, supply_tier: int):

        # This works and doesn't duplicate code, but comes at the cost ob temporaly having two big cubes at the same time
        def __add_device_tier_to_portfolio_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in devices at all, leave cube unchanged
            if DEVICE not in self.focus_on:
                return cube
            # First add new devices to the index according to all the portfolios, restricted on focussed devices.
            for portfolio in self.portfolios:
                self.devices |= OrderedSet([self.database.devices[device_slug] for device_slug in portfolio.devices
                                            if device_slug in self.focus_on[DEVICE]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.devices),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for portfolio_index, portfolio in enumerate(self.portfolios):
                portfolio_slice = cube[..., portfolio_index]
                for device_index, device in enumerate(self.devices):
                    if device.slug not in portfolio.devices:
                        continue
                    device_factor = portfolio.devices[device.slug].amount
                    cube_of_weights[..., portfolio_index, device_index] = portfolio_slice * device_factor
            return cube_of_weights

        def __add_part_tier_to_device_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in parts at all, leave cube unchanged
            if PART not in self.focus_on:
                return cube
            # First add new parts to the index according to all the devices, restricted on focussed parts.
            for device in self.devices:
                self.parts |= OrderedSet([self.database.parts[part_slug] for part_slug in device.parts
                                        if part_slug in self.focus_on[PART]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.parts),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for device_index, device in enumerate(self.devices):
                device_slice = cube[..., device_index]
                for part_index, part in enumerate(self.parts):
                    if part.slug not in device.parts:
                        continue
                    part_factor = device.parts[part.slug].amount
                    cube_of_weights[..., device_index, part_index] = device_slice * part_factor
            return cube_of_weights

        def __add_part_tier_to_missing_device_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in parts at all, leave cube unchanged
            if PART not in self.focus_on:
                return cube
            # First add new parts to the index according to all the portfolios, restricted on focussed parts.
            for portfolio in self.portfolios:
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    self.parts |= OrderedSet([self.database.parts[part_slug] for part_slug in device.parts
                                            if part_slug in self.focus_on[PART]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.parts),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for portfolio_index, portfolio in enumerate(self.portfolios):
                portfilio_slice = cube[..., portfolio_index]
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    device_factor = portfolio.devices[device_slug].amount
                    for part_index, part in enumerate(self.parts):
                        if part.slug not in device.parts:
                            continue
                        part_factor = device.parts[part.slug].amount
                        cube_of_weights[..., portfolio_index, part_index] += portfilio_slice * device_factor * part_factor
            return cube_of_weights

        def __add_material_tier_to_part_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in materials at all, leave cube unchanged
            if MATERIAL not in self.focus_on:
                return cube
            # First add new materials to the index according to all the parts, restricted on focussed materials
            for part in self.parts:
                self.materials |= OrderedSet([self.database.materials[material_slug] for material_slug in part.materials
                                            if material_slug in self.focus_on[MATERIAL]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.materials),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for part_index, part in enumerate(self.parts):
                part_slice = cube[..., part_index]
                for material_index, material in enumerate(self.materials):
                    if material.slug not in part.materials:
                        continue
                    material_factor = part.materials[material.slug].amount
                    cube_of_weights[..., part_index, material_index] = part_slice * material_factor
            return cube_of_weights

        def __add_material_tier_to_missing_part_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in resources at all, leave cube unchanged
            if MATERIAL not in self.focus_on:
                return cube
            # First add new materials to the index according to all the devices, restricted on focussed ones
            for device in self.devices:
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    self.materials |= OrderedSet([self.database.materials[material_slug] for material_slug in part.materials
                                                if material_slug in self.focus_on[MATERIAL]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.materials),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for device_index, device in enumerate(self.devices):
                device_slice = cube[..., device_index]
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    part_factor = device.parts[part_slug].amount
#                    part_source = device.parts[part_slug].source
                    for material_index, material in enumerate(self.materials):
                        if material.slug not in part.materials:
                            continue
                        material_factor = part.materials[material.slug].amount
#                        material_source = part.materials[material.slug].source
                        cube_of_weights[..., device_index, material_index] += device_slice * part_factor * material_factor
#                        print("Ergänzt: Device-Part '" + part_source + "' * Part-Material '" + material_source + "'")
            return cube_of_weights

        def __add_material_tier_to_missing_part_device_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in resources at all, leave cube unchanged
            if MATERIAL not in self.focus_on:
                return cube
            # First add new materials to the index according to all the portfolios, restricted on focussed ones
            for portfolio in self.portfolios:
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        self.materials |= OrderedSet([self.database.materials[material_slug] for material_slug in part.materials
                                                    if material_slug in self.focus_on[MATERIAL]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.materials),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for portfolio_index, portfolio in enumerate(self.portfolios):
                portfolio_slice = cube[..., portfolio_index]
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    device_factor = portfolio.devices[device_slug].amount
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        part_factor = device.parts[part_slug].amount
                        for material_index, material in enumerate(self.materials):
                            if material.slug not in part.materials:
                                continue
                            material_factor = part.materials[material.slug].amount
                            cube_of_weights[..., portfolio_index, material_index] += portfolio_slice * device_factor * part_factor * material_factor
            return cube_of_weights

        def __add_resource_tier_to_material_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in resources at all, leave cube unchanged
            if RESOURCE not in self.focus_on:
                return cube
            # First add new resources to the index according to all the materials, restricted on focussed ones
            for material in self.materials:
                self.resources |= OrderedSet([self.database.resources[resource_slug] for resource_slug in material.resources
                                            if resource_slug in self.focus_on[RESOURCE]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.resources),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for material_index, material in enumerate(self.materials):
                material_slice = cube[..., material_index]
                for resource_index, resource in enumerate(self.resources):
                    if resource.slug not in material.resources:
                        continue
                    resource_factor = material.resources[resource.slug].amount
                    cube_of_weights[..., material_index, resource_index] = material_slice * resource_factor
            return cube_of_weights

        def __add_resource_tier_to_missing_material_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in resources at all, leave cube unchanged
            if RESOURCE not in self.focus_on:
                return cube
            # First add new resources to the index according to all the parts, restricted on focussed ones
            for part in self.parts:
                for material_slug in part.materials:  # TODO Could be optimized to collect materials in OrderedSet as well
                    material = self.database.materials[material_slug]
                    self.resources |= OrderedSet([self.database.resources[resource_slug] for resource_slug in material.resources
                                                if resource_slug in self.focus_on[RESOURCE]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.resources),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for part_index, part in enumerate(self.parts):
                part_slice = cube[..., part_index]
                for material_slug in part.materials:
                    material = self.database.materials[material_slug]
                    material_factor = part.materials[material_slug].amount
                    for resource_index, resource in enumerate(self.resources):
                        if resource.slug not in material.resources:
                            continue
                        resource_factor = material.resources[resource.slug].amount
                        cube_of_weights[..., part_index, resource_index] += part_slice * material_factor * resource_factor
            return cube_of_weights

        def __add_resource_tier_to_missing_material_part_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in resources at all, leave cube unchanged
            if RESOURCE not in self.focus_on:
                return cube
            # First add new resources to the index according to all the devices, restricted on focussed ones
            for device in self.devices:
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    for material_slug in part.materials:
                        material = self.database.materials[material_slug]
                        self.resources |= OrderedSet([self.database.resources[resource_slug] for resource_slug in material.resources
                                                    if resource_slug in self.focus_on[RESOURCE]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.resources),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for device_index, device in enumerate(self.devices):
                device_slice = cube[..., device_index]
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    part_factor = device.parts[part_slug].amount
                    for material_slug in part.materials:
                        material = self.database.materials[material_slug]
                        material_factor = part.materials[material_slug].amount
                        for resource_index, resource in enumerate(self.resources):
                            if resource.slug not in material.resources:
                                continue
                            resource_factor = material.resources[resource.slug].amount
                            cube_of_weights[..., device_index, resource_index] += device_slice * part_factor * material_factor * resource_factor
            return cube_of_weights

        def __add_resource_tier_to_missing_material_part_device_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in resources at all, leave cube unchanged
            if RESOURCE not in self.focus_on:
                return cube
            # First add new resources to the index according to all the portfolios, restricted on focussed ones
            for portfolio in self.portfolios:
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        for material_slug in part.materials:
                            material = self.database.materials[material_slug]
                            self.resources |= OrderedSet([self.database.resources[resource_slug] for resource_slug in material.resources
                                                        if resource_slug in self.focus_on[RESOURCE]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.resources),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for portfolio_index, portfolio in enumerate(self.portfolios):
                portfolio_slice = cube[..., portfolio_index]
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    device_factor = portfolio.devices[device_slug].amount
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        part_factor = device.parts[part_slug].amount
                        for material_slug in part.materials:
                            material = self.database.materials[material_slug]
                            material_factor = part.materials[material_slug].amount
                            for resource_index, resource in enumerate(self.resources):
                                if resource.slug not in material.resources:
                                    continue
                                resource_factor = material.resources[resource.slug].amount
                                cube_of_weights[..., portfolio_index, resource_index] += portfolio_slice * device_factor * part_factor * material_factor * resource_factor
            return cube_of_weights

        def __add_country_tier_to_resource_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in countries at all, leave cube unchanged
            if LOCATION not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new countries to the index according to all the resources, restricted on focussed ones
            for resource in self.resources:
                self.countries |= OrderedSet([self.database.locations[country_slug] for country_slug in resource.countries
                                             if country_slug in self.focus_on[LOCATION]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.countries),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for resource_index, resource in enumerate(self.resources):
                resource_slice = cube[..., resource_index]
                for country_index, country in enumerate(self.countries):
                    if country.slug not in resource.countries:
                        continue
                    country_factor = resource.countries[country.slug].amount
                    cube_of_weights[..., resource_index, country_index] = resource_slice * country_factor
            return cube_of_weights

        def __add_country_tier_to_missing_resource_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in countries at all, leave cube unchanged
            if LOCATION not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new countries to the index according to all the materials, restricted on focussed ones
            for material in self.materials:
                for resource_slug in material.resources:  # TODO Could be optimized to collect materials in OrderedSet as well
                    resource = self.database.resources[resource_slug]
                    self.countries |= OrderedSet(
                        [self.database.locations[country_slug] for country_slug in resource.countries
                        if country_slug in self.focus_on[LOCATION]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.countries),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for material_index, material in enumerate(self.materials):
                material_slice = cube[..., material_index]
                for resource_slug in material.resources:
                    resource = self.database.resources[resource_slug]
                    resource_factor = material.resources[resource_slug].amount
                    for country_index, country in enumerate(self.countries):
                        if country.slug not in resource.countries:
                            continue
                        country_factor = resource.countries[country.slug].amount
                        cube_of_weights[..., material_index, country_index] += material_slice * resource_factor * country_factor
            return cube_of_weights

        def __add_country_tier_to_missing_resource_material_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in countries at all, leave cube unchanged
            if LOCATION not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new countries to the index according to all the parts, restricted on focussed ones
            for part in self.parts:
                for material_slug in part.materials:
                    material = self.database.materials[material_slug]
                    for resource_slug in material.resources:
                        resource = self.database.resources[resource_slug]
                        self.countries |= OrderedSet(
                            [self.database.locations[country_slug] for country_slug in resource.countries
                            if country_slug in self.focus_on[LOCATION]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.countries),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for part_index, part in enumerate(self.parts):
                part_slice = cube[..., part_index]
                for material_slug in part.materials:
                    material = self.database.materials[material_slug]
                    material_factor = part.materials[material_slug].amount
                    for resource_slug in material.resources:
                        resource = self.database.resources[resource_slug]
                        resource_factor = material.resources[resource_slug].amount
                        for country_index, country in enumerate(self.countries):
                            if country.slug not in resource.countries:
                                continue
                            country_factor = resource.countries[country.slug].amount
                            cube_of_weights[..., part_index, country_index] += part_slice * material_factor * resource_factor * country_factor
            return cube_of_weights

        def __add_country_tier_to_missing_resource_material_part_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in countries at all, leave cube unchanged
            if LOCATION not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new countries to the index according to all the devices, restricted on focussed ones
            for device in self.devices:
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    for material_slug in part.materials:
                        material = self.database.materials[material_slug]
                        for resource_slug in material.resources:
                            resource = self.database.resources[resource_slug]
                            self.countries |= OrderedSet(
                                [self.database.locations[country_slug] for country_slug in resource.countries
                                if country_slug in self.focus_on[LOCATION]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.countries),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for device_index, device in enumerate(self.devices):
                device_slice = cube[..., device_index]
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    part_factor = device.parts[part_slug].amount
                    for material_slug in part.materials:
                        material = self.database.materials[material_slug]
                        material_factor = part.materials[material_slug].amount
                        for resource_slug in material.resources:
                            resource = self.database.resources[resource_slug]
                            resource_factor = material.resources[resource_slug].amount
                            for country_index, country in enumerate(self.countries):
                                if country.slug not in resource.countries:
                                    continue
                                country_factor = resource.countries[country.slug].amount
                                cube_of_weights[..., device_index, country_index] += device_slice * part_factor * material_factor * resource_factor * country_factor
            return cube_of_weights

        def __add_country_tier_to_missing_resource_material_part_device_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in countries at all, leave cube unchanged
            if LOCATION not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new countries to the index according to all the portfolios, restricted on focussed ones
            for portfolio in self.portfolios:
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        for material_slug in part.materials:
                            material = self.database.materials[material_slug]
                            for resource_slug in material.resources:
                                resource = self.database.resources[resource_slug]
                                self.countries |= OrderedSet(
                                    [self.database.locations[country_slug] for country_slug in resource.countries
                                    if country_slug in self.focus_on[LOCATION]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.countries),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_weights = np.zeros(cube_shape, dtype=DTYPE)
            for portfolio_index, portfolio in enumerate(self.portfolios):
                portfolio_slice = cube[..., portfolio_index]
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    device_factor = portfolio.devices[device_slug].amount
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        part_factor = device.parts[part_slug].amount
                        for material_slug in part.materials:
                            material = self.database.materials[material_slug]
                            material_factor = part.materials[material_slug].amount
                            for resource_slug in material.resources:
                                resource = self.database.resources[resource_slug]
                                resource_factor = material.resources[resource_slug].amount
                                for country_index, country in enumerate(self.countries):
                                    if country.slug not in resource.countries:
                                        continue
                                    country_factor = resource.countries[country.slug].amount
                                    cube_of_weights[..., portfolio_index, country_index] += portfolio_slice * device_factor * part_factor * material_factor * resource_factor * country_factor
            return cube_of_weights

        def __add_indicator_tier_to_country_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in indicators at all, leave cube unchanged
            if INDICATOR not in self.focus_on:
                return cube
            # First add new indicators to the index according to all the countries, restricted on focussed ones
            for country in self.countries:
                self.indicators |= OrderedSet(
                    [self.database.indicators[indicator_slug] for indicator_slug in country.indicators
                    if indicator_slug in self.focus_on[INDICATOR]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.indicators),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_harms = np.zeros(cube_shape, dtype=DTYPE)
            for country_index, country in enumerate(self.countries):
                country_slice = cube[..., country_index]
                for indicator_index, indicator in enumerate(self.indicators):
                    if indicator.slug not in country.indicators:
                        continue
                    indicator_factor = country.indicators[indicator.slug].amount
                    cube_of_harms[..., country_index, indicator_index] = country_slice * indicator_factor
            return cube_of_harms

        def __add_indicator_tier_to_missing_country_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in indicators at all, leave cube unchanged
            if INDICATOR not in self.focus_on:
                return cube
            # First add new indicators to the index according to all the resources, restricted on focussed ones
            for resource in self.resources:
                for country_slug in resource.countries:
                    country = self.database.locations[country_slug]
                    self.indicators |= OrderedSet(
                        [self.database.indicators[indicator_slug] for indicator_slug in country.indicators
                        if indicator_slug in self.focus_on[INDICATOR]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.resources),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_harms = np.zeros(cube_shape, dtype=DTYPE)
            for resource_index, resource in enumerate(self.resources):
                resource_slice = cube[..., resource_index]
                for country_slug in resource.countries:
                    country = self.database.locations[country_slug]
                    country_factor = resource.countries[country_slug].amount
                    for indicator_index, indicator in enumerate(self.indicators):
                        if indicator.slug not in country.indicators:
                            continue
                        indicator_factor = country.indicators[indicator.slug].amount
                        cube_of_harms[..., resource_index, indicator_index] += resource_slice * country_factor * indicator_factor
            return cube_of_harms

        def __add_indicator_tier_to_missing_country_resource_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in indicators at all, leave cube unchanged
            if INDICATOR not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new indicators to the index according to all the materials, restricted on focussed ones
            for material in self.materials:
                for resource_slug in material.resources:
                    resource = self.database.resources[resource_slug]
                    for country_slug in resource.countries:
                        country = self.database.locations[country_slug]
                        self.indicators |= OrderedSet(
                            [self.database.indicators[indicator_slug] for indicator_slug in country.indicators
                            if indicator_slug in self.focus_on[INDICATOR]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.indicators),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_harms = np.zeros(cube_shape, dtype=DTYPE)
            for material_index, material in enumerate(self.materials):
                material_slice = cube[..., material_index]
                for resource_slug in material.resources:
                    resource = self.database.resources[resource_slug]
                    resource_factor = material.resources[resource_slug].amount
                    for country_slug in resource.countries:
                        country = self.database.locations[country_slug]
                        country_factor = resource.countries[country_slug].amount
                        for indicator_index, indicator in enumerate(self.indicators):
                            if indicator.slug not in country.indicators:
                                continue
                            indicator_factor = country.indicators[indicator.slug].amount
                            cube_of_harms[..., material_index, indicator_index] += \
                                material_slice * resource_factor * country_factor * indicator_factor
            return cube_of_harms

        def __add_indicator_tier_to_missing_country_resource_material_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in indicators at all, leave cube unchanged
            if INDICATOR not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new indicators to the index according to all the parts, restricted on focussed ones
            for part in self.parts:
                for material_slug in part.materials:
                    material = self.database.materials[material_slug]
                    for resource_slug in material.resources:
                        resource = self.database.resources[resource_slug]
                        for country_slug in resource.countries:
                            country = self.database.locations[country_slug]
                            self.indicators |= OrderedSet(
                                [self.database.indicators[indicator_slug] for indicator_slug in country.indicators
                                if indicator_slug in self.focus_on[INDICATOR]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.indicators),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_harms = np.zeros(cube_shape, dtype=DTYPE)
            for part_index, part in enumerate(self.parts):
                part_slice = cube[..., part_index]
                for material_slug in part.materials:
                    material = self.database.materials[material_slug]
                    material_factor = part.materials[material_slug].amount
                    for resource_slug in material.resources:
                        resource = self.database.resources[resource_slug]
                        resource_factor = material.resources[resource_slug].amount
                        for country_slug in resource.countries:
                            country = self.database.locations[country_slug]
                            country_factor = resource.countries[country_slug].amount
                            for indicator_index, indicator in enumerate(self.indicators):
                                if indicator.slug not in country.indicators:
                                    continue
                                indicator_factor = country.indicators[indicator.slug].amount
                                cube_of_harms[..., part_index, indicator_index] += \
                                    part_slice * material_factor * resource_factor * country_factor * indicator_factor
            return cube_of_harms

        def __add_indicator_tier_to_missing_country_resource_material_part_cube(cube: np.ndarray) -> np.ndarray:
            # If not interested in indicators at all, leave cube unchanged
            if INDICATOR not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new indicators to the index according to all the devices, restricted on focussed ones
            for device in self.devices:
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    for material_slug in part.materials:
                        material = self.database.materials[material_slug]
                        for resource_slug in material.resources:
                            resource = self.database.resources[resource_slug]
                            for country_slug in resource.countries:
                                country = self.database.locations[country_slug]
                                self.indicators |= OrderedSet(
                                    [self.database.indicators[indicator_slug] for indicator_slug in country.indicators
                                    if indicator_slug in self.focus_on[INDICATOR]])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.indicators),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_harms = np.zeros(cube_shape, dtype=DTYPE)
            for device_index, device in enumerate(self.devices):
                device_slice = cube[..., device_index]
                for part_slug in device.parts:
                    part = self.database.parts[part_slug]
                    part_factor = device.parts[part_slug].amount
                    for material_slug in part.materials:
                        material = self.database.materials[material_slug]
                        material_factor = part.materials[material_slug].amount
                        for resource_slug in material.resources:
                            resource = self.database.resources[resource_slug]
                            resource_factor = material.resources[resource_slug].amount
                            for country_slug in resource.countries:
                                country = self.database.locations[country_slug]
                                country_factor = resource.countries[country_slug].amount
                                for indicator_index, indicator in enumerate(self.indicators):
                                    if indicator.slug not in country.indicators:
                                        continue
                                    indicator_factor = country.indicators[indicator.slug].amount
                                    cube_of_harms[..., device_index, indicator_index] += \
                                        device_slice * part_factor * material_factor * resource_factor * \
                                        country_factor * indicator_factor
            return cube_of_harms

        def __add_indicator_tier_to_missing_country_resource_material_part_device_cube(cube: np.ndarray) -> np.ndarray:
            # TODO Hier ist der Code wie er zu alten Analysis-Zeiten für die gleiche Routine war.
            # TODO   Man kann also wohl diese Schleife durch eine Multiplikation ersetzen, so wie im 
            # TODO   Assessment der cube mit den harms (also den Indikatorwerten pro Country) multipliziert wurde
            # for indicator_index, indicator in enumerate(self.indicators):
            #     # Actually compute assessment here
            #     assessment = Assessment(self.inventory, [indicator.orig_slug], indicator.usage)
            #     for portfolio_index, portfolio in enumerate(self.inventory.portfolios):
            #         indicator_value = assessment.cube[..., portfolio_index]
            #         cube_of_harms[..., portfolio_index, indicator_index] = indicator_value
            #     del assessment  # Not needed anymore (included just to document garbage collection)
            # return cube_of_harms
            # If not interested in indicators at all, leave cube unchanged
            if INDICATOR not in self.focus_on:  # TODO Use has_supply_tier
                return cube
            # First add new indicators to the index according to all the devices, restricted on focussed ones
            for portfolio in self.portfolios:
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        for material_slug in part.materials:
                            material = self.database.materials[material_slug]
                            for resource_slug in material.resources:
                                resource = self.database.resources[resource_slug]
                                for country_slug in resource.countries:
                                    country = self.database.locations[country_slug]
                                    self.indicators |= OrderedSet(
                                        [self.database.indicators[indicator_slug] for indicator_slug in country.indicators
                                        if indicator_slug in self.focus_on[INDICATOR] and indicator.probability_based and indicator.direction == "ascending"])
            # Then build new extended cube based on the index
            new_vector_shape = (len(self.indicators),)
            cube_shape = cube.shape + new_vector_shape
            cube_of_harms = np.zeros(cube_shape, dtype=DTYPE)
            for portfolio_index, portfolio in enumerate(self.portfolios):
                portfolio_slice = cube[..., portfolio_index]
                for device_slug in portfolio.devices:
                    device = self.database.devices[device_slug]
                    device_factor = portfolio.devices[device_slug].amount
                    for part_slug in device.parts:
                        part = self.database.parts[part_slug]
                        part_factor = device.parts[part_slug].amount
                        for material_slug in part.materials:
                            material = self.database.materials[material_slug]
                            material_factor = part.materials[material_slug].amount
                            for resource_slug in material.resources:
                                resource = self.database.resources[resource_slug]
                                resource_factor = material.resources[resource_slug].amount
                                for country_slug in resource.countries:
                                    country = self.database.locations[country_slug]
                                    country_factor = resource.countries[country_slug].amount
                                    for indicator_index, indicator in enumerate(self.indicators):
                                        if indicator.slug not in country.indicators:
                                            continue
                                        indicator_factor = country.indicators[indicator.slug].amount
                                        cube_of_harms[..., portfolio_index, indicator_index] += \
                                            portfolio_slice * device_factor * part_factor * material_factor * \
                                            resource_factor * country_factor * indicator_factor
            return cube_of_harms

        # TODO Lässt sich allgemeiner programmieren, man muss ja nicht alles einzeln machen, ist ja schematisch.
        # TODO Alle Cubes pro supply tier einfach merken, dann muss nicht beim Zurückgehen alles gelöscht werden
        if self.current_afar_axis > supply_tier:
            # TODO Das ist natürlich ineffizient, hier alles zu löschen und wieder alles nochmal zu berechnen
            if VERBOSE:
                print("Rebuilding inventory cube")
            del self.cube
            # For each object have "one" (= 1.0)
            self.cube = np.ones_like(np.arange(len(self.focus_on[self.root_axis])), dtype=DTYPE)
            self.current_afar_axis = 0
        if self.current_afar_axis < PORTFOLIO <= supply_tier and self.root_axis <= PORTFOLIO:
            self.current_afar_axis = PORTFOLIO
            if VERBOSE:
                print("%d portfolios inventoried" % (len(self.portfolios)))
        if self.current_afar_axis < DEVICE <= supply_tier and self.root_axis <= DEVICE:
            if self.root_axis < DEVICE:
                # if self.has_supply_tier(PORTFOLIO_TIER):
                self.cube = __add_device_tier_to_portfolio_cube(self.cube)
            self.current_afar_axis = DEVICE
            if VERBOSE:
                print("%d devices inventoried" % (len(self.devices)))
        if self.current_afar_axis < PART <= supply_tier and self.root_axis <= PART:
            if self.root_axis < PART:
                if self.has_supply_tier(DEVICE):
                    self.cube = __add_part_tier_to_device_cube(self.cube)
                else:  # self.has_supply_tier(PORTFOLIOS_AXIS):
                    self.cube = __add_part_tier_to_missing_device_cube(self.cube)
            self.current_afar_axis = PART
            if VERBOSE:
                print("%d parts inventoried" % (len(self.parts)))
        if self.current_afar_axis < MATERIAL <= supply_tier and self.root_axis <= MATERIAL:
            if self.root_axis < MATERIAL:
                if self.has_supply_tier(PART):
                    self.cube = __add_material_tier_to_part_cube(self.cube)
                elif self.has_supply_tier(DEVICE):
                    self.cube = __add_material_tier_to_missing_part_cube(self.cube)
                else:  # self.has_supply_tier(PORTFOLIOS_AXIS):
                    self.cube = __add_material_tier_to_missing_part_device_cube(self.cube)
            self.current_afar_axis = MATERIAL
            if VERBOSE:
                print("%d materials inventoried" % (len(self.materials)))
        if self.current_afar_axis < RESOURCE <= supply_tier and self.root_axis <= RESOURCE:
            if self.root_axis < RESOURCE:
                if self.has_supply_tier(MATERIAL):
                    self.cube = __add_resource_tier_to_material_cube(self.cube)
                elif self.has_supply_tier(PART):
                    self.cube = __add_resource_tier_to_missing_material_cube(self.cube)
                elif self.has_supply_tier(DEVICE):
                    self.cube = __add_resource_tier_to_missing_material_part_cube(self.cube)
                else:  # self.has_supply_tier(PORTFOLIOS_AXIS):
                    self.cube = __add_resource_tier_to_missing_material_part_device_cube(self.cube)
            self.current_afar_axis = RESOURCE
            if VERBOSE:
                print("%d resources inventoried" % (len(self.resources)))
        if self.current_afar_axis < LOCATION <= supply_tier and self.root_axis <= LOCATION:
            if self.root_axis < LOCATION:
                if self.has_supply_tier(RESOURCE):
                    self.cube = __add_country_tier_to_resource_cube(self.cube)
                elif self.has_supply_tier(MATERIAL):
                    self.cube = __add_country_tier_to_missing_resource_cube(self.cube)
                elif self.has_supply_tier(PART):
                    self.cube = __add_country_tier_to_missing_resource_material_cube(self.cube)
                elif self.has_supply_tier(DEVICE):
                    self.cube = __add_country_tier_to_missing_resource_material_part_cube(self.cube)
                else:  # self.has_supply_tier(PORTFOLIOS_AXIS):
                    self.cube = __add_country_tier_to_missing_resource_material_part_device_cube(self.cube)
            self.current_afar_axis = LOCATION
            if VERBOSE:
                print("%d countries inventoried" % (len(self.countries)))
        if self.current_afar_axis < INDICATOR <= supply_tier and self.root_axis <= INDICATOR:
            if self.root_axis < INDICATOR:
                if self.has_supply_tier(LOCATION):
                    self.cube = __add_indicator_tier_to_country_cube(self.cube)
                elif self.has_supply_tier(RESOURCE):
                    self.cube = __add_indicator_tier_to_missing_country_cube(self.cube)
                elif self.has_supply_tier(MATERIAL):
                    self.cube = __add_indicator_tier_to_missing_country_resource_cube(self.cube)
                elif self.has_supply_tier(PART):
                    self.cube = __add_indicator_tier_to_missing_country_resource_material_cube(self.cube)
                elif self.has_supply_tier(DEVICE):
                    self.cube = __add_indicator_tier_to_missing_country_resource_material_part_cube(self.cube)
                else:  # self.has_supply_tier(PORTFOLIOS_AXIS):
                    self.cube = __add_indicator_tier_to_missing_country_resource_material_part_device_cube(self.cube)
            self.current_afar_axis = INDICATOR
            if VERBOSE:
                print("%d indicators assessed" % (len(self.indicators)))
        
        if VERBOSE:
            print("Inventory's cube has shape %s = %d elements" %
                  (self.cube.shape, self.cube.size))

    # ---------------------------------------
    
    def view_along_selection(self, cube: np.ndarray, all_axes: List[int], view_axes: List[int], max_axis: int) -> np.ndarray:
        all_but_view_axes_thru_max_axis = tuple(
            [ix for ix, axis in enumerate(all_axes) if axis not in view_axes and axis <= max_axis])
        return np.sum(cube, axis=all_but_view_axes_thru_max_axis)  # TODO Sehr speicheraufwändig

    def leave_filtered_indices_of_axis(self, cube: np.ndarray, indices: List[int], axis: int) -> np.ndarray:
        # Beispiel mnt_reform_chromite.cube = np.take(mnt_reform_chromite.cube, [27], RESOURCES_AXIS - DEVICES_AXIS)
        return np.take(cube, indices, axis)
        
    def has_supply_tier(self, supply_tier: int) -> bool:
        return supply_tier in self.focus_on

    def get_entities_according_to_axis(self, axis: int) -> List[Entity]:
        if axis is PORTFOLIO:
            return list(self.portfolios) if self.portfolios is not None else None
        elif axis is DEVICE:
            return list(self.devices) if self.devices is not None else None
        elif axis is PART:
            return list(self.parts) if self.parts is not None else None
        elif axis is MATERIAL:
            return list(self.materials) if self.materials is not None else None
        elif axis is RESOURCE:
            return list(self.resources) if self.resources is not None else None
        elif axis is LOCATION:
            return list(self.countries) if self.countries is not None else None
        else:
            assert axis is INDICATOR
            return list(self.indicators) if self.indicators is not None else None

    def count_axes_from_object_type_to_axes(self, axes: List[int]) -> List[int]:
        return [self.count_axes_from_object_type_to_axis(axis) for axis in axes]

    def count_axes_from_object_type_to_axis(self, axis: int) -> int:
        return list(self.focus_on).index(axis)

    def count_axes_from_object_type_to_current_afar_axis(self) -> int:
        return self.count_axes_from_object_type_to_axis(self.current_afar_axis)

    def get_available_axes(self) -> List[int]:
        return list(self.focus_on.keys())

    # Helper function fpr interpreting filter specification strings
    def map_filters_to_entities_and_data_indices(self, filters: Dict[int, str]) \
            -> Tuple[Dict[int, List[Entity]], Dict[int, List[int]]]:
        # TODO Das ist aktuell O(n3), sogar mehrmals :-(
        all_indices: Dict[int, List[int]] = {}
        all_entities: Dict[int, List[Entity]] = {}
        for filter_axis in filters:
            for object_filter in filters[filter_axis].split(","):
                # A , in filter seperation means: all conditions needs matching, i.e. an intersection
                # First, collect all entities matching this new condition
                condition_indices: List[int] = []
                condition_entities: List[Entity] = []
                object_attr_pattern = object_filter.split('=', 1)
                if len(object_attr_pattern) == 1:
                    object_attr = "slug"
                    object_pattern = object_filter
                else:
                    object_attr = object_attr_pattern[0]
                    object_pattern = object_attr_pattern[1]
                # TODO Use list comprehension
                for index__, entity__ in enumerate(self.get_entities_according_to_axis(filter_axis)):
                    if re.compile(r"^" + object_pattern.strip().replace("(", "\\(").replace(")", "\\)") + "$"). \
                            match(getattr(entity__, object_attr)):
                        condition_indices.append(index__)
                        condition_entities.append(entity__)
                # Secondly, intersect these new with the global ones
                if filter_axis not in all_entities:  # Seems to be the first
                    all_entities[filter_axis] = condition_entities
                    all_indices[filter_axis] = condition_indices
                else:  # Leave global entities only, if available in latest condition as well
                    all_entities[filter_axis] = [x for x in all_entities[filter_axis] if x in condition_entities]
                    all_indices[filter_axis] = [x for x in all_indices[filter_axis] if x in condition_indices]
            if filter_axis not in all_indices:  # and in entities
                all_indices[filter_axis] = []
                all_entities[filter_axis] = []
        return all_entities, all_indices    

    def get_significant_indices(self, cube: np.ndarray) -> np.ndarray:
        # Default: Get rid of all zero values
        # TODO Support a cut-off (maybe better to not include small values in the cube from the start)
        # TODO or: sorted_indices = get_indices_of_n_largest_values(view_cube, number_of_displayed_values), see below
        non_zero_indices = np.nonzero(cube)
        # TODO Does this also work with zip()?
        remaining_indices = np.transpose(non_zero_indices)  # simply transposing it
        if VERBOSE:
            print("  and %d non-zero elements to display" % len(remaining_indices))
        return remaining_indices

# -------------------------------------------------------------------

def get_indices_of_n_largest_values(array: np.ndarray, n: int) -> Tuple[np.ndarray, ...]:
    """
        Returns the n largest indices from a numpy array in tuples of arrays. Each position in the
        tupel represents a dimension with the indices in the list being the index in this dimension
        Taken from https://stackoverflow.com/a/38884051
        See also note on N-dimensional arrays in https://numpy.org/doc/stable/reference/generated/numpy.argsort.html
        Alternative for only one dimension: np.flip(np.argsort(array))[:n]
    """
    flat = array.flatten()
    indices = np.argpartition(flat, -n)[-n:]
    indices = indices[np.argsort(-flat[indices])]
    return np.unravel_index(indices, array.shape) if array.ndim > 0 else ([0],)


def list_axis_element_combinations(object_axis: int, tier_axis: int, reverse: bool = False):
    result = [[]]
    for axis in LIST_OF_AXES[(object_axis + 1):(tier_axis + 1)]:
        result += [s + [axis] for s in result]
    result = result[1:]  # Remove the initial empty list
    if reverse:
        result = reversed(result)
    result = sorted(result, key=list.__len__, reverse=reverse)
    return result
