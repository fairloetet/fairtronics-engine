import olca
import uuid
from datetime import datetime
from pocs.analysis.model import Device
from pocs.analysis.database import Database, DEVICE_TYPE


class Background:
    items_unit = olca.Unit()
    gram_unit = olca.Unit()
    items_factor = olca.FlowPropertyFactor()
    mass_factor = olca.FlowPropertyFactor()


class FlowCategories:
    devices_category = olca.Category()
    parts_category = olca.Category()
    materials_category = olca.Category()
    resources_category = olca.Category()
    work_category = olca.Category()


class ProcessCategories:
    countries_process_category = olca.Category()
    materials_process_category = olca.Category()
    components_process_category = olca.Category()
    products_process_category = olca.Category()


def create_background(client: olca.Client) -> Background:
    background = Background()
    print()
    # 0 Create some background data: Locations, UnitGroups & Units, FlowProperties
    # 0.1 Create item Unit, UnitGroup, FlowProperty, and FlowPropertyFactor
    background.items_unit.id = str(uuid.uuid4())
    background.items_unit.olca_type = olca.Unit.__name__
    background.items_unit.name = "Item(s)"
    background.items_unit.conversion_factor = 1.0
    background.items_unit.reference_unit = True
    response = client.insert(background.items_unit)
    assert str(response) == 'ok'
    items_group = olca.UnitGroup()
    items_group.id = str(uuid.uuid4())
    items_group.olca_type = olca.UnitGroup.__name__
    items_group.name = "Units of items"
    items_group.units = [background.items_unit]
    response = client.insert(items_group)
    assert str(response) == 'ok'
    items_property = olca.FlowProperty()
    items_property.id = str(uuid.uuid4())
    items_property.olca_type = olca.FlowProperty.__name__
    items_property.name = "Number of items"
    items_property.unit_group = items_group
    response = client.insert(items_property)
    assert str(response) == 'ok'
    items_property_ref = client.find(olca.FlowProperty, items_property.name)
    background.items_factor.conversion_factor = 1.0
    background.items_factor.flow_property = items_property_ref
    background.items_factor.reference_flow_property = True
    # 0.2 Create gram Unit, UnitGroup, FlowProperty, and FlowPropertyFactor
    background.gram_unit.id = str(uuid.uuid4())
    background.gram_unit.olca_type = olca.Unit.__name__
    background.gram_unit.name = "g"
    background.gram_unit.conversion_factor = 1.0
    background.gram_unit.reference_unit = True
    response = client.insert(background.gram_unit)
    assert str(response) == 'ok'
    mass_group = olca.UnitGroup()
    mass_group.id = str(uuid.uuid4())
    mass_group.olca_type = olca.UnitGroup.__name__
    mass_group.name = "Units of mass"
    mass_group.units = [background.gram_unit]
    response = client.insert(mass_group)
    assert str(response) == 'ok'
    mass_property = olca.FlowProperty()
    mass_property.id = str(uuid.uuid4())
    mass_property.olca_type = olca.FlowProperty.__name__
    mass_property.name = "Mass"
    mass_property.unit_group = mass_group
    response = client.insert(mass_property)
    assert str(response) == 'ok'
    mass_property_ref = client.find(olca.FlowProperty, mass_property.name)
    background.mass_factor.conversion_factor = 1.0
    background.mass_factor.flow_property = mass_property_ref
    background.mass_factor.reference_flow_property = True
    return background


def create_locations(client: olca.Client, data: Database):
    # 0.3 Create countries as Locations
    print("Converting countries to Locations:")
    # country_category = olca.Category()
    # country_category.id = str(uuid.uuid4())
    # country_category.model_type = olca.ModelType.LOCATION
    # country_category.olca_type = olca.Category.__name__
    # country_category.name = "Countries"
    # response = client.insert(country_category)
    # country_category_ref = client.find(olca.Category, country_category.name)
    for country in data.countries.values():
        country_location = olca.Location()
        country_location.id = str(uuid.uuid4())
        country_location.olca_type = olca.Location.__name__
        # country_location.category = country_category_ref
        country_location.name = country.name
        country_location.code = country.code
        response = client.insert(country_location)
        print(2 * ' ' + country_location.name + ": " + response)
        assert str(response) == 'ok'


def create_flow_categories(client: olca.Client) -> FlowCategories:
    # 1 Flows
    categories = FlowCategories()
    # 1.0 Add Device Category (left empty)
    categories.devices_category.id = str(uuid.uuid4())
    categories.devices_category.model_type = olca.ModelType.FLOW
    categories.devices_category.olca_type = olca.Category.__name__
    categories.devices_category.name = "Devices"
    response = client.insert(categories.devices_category)
    assert str(response) == 'ok'
    categories.parts_category.id = str(uuid.uuid4())
    categories.parts_category.model_type = olca.ModelType.FLOW
    categories.parts_category.olca_type = olca.Category.__name__
    categories.parts_category.name = "Parts"
    response = client.insert(categories.parts_category)
    assert str(response) == 'ok'
    categories.materials_category.id = str(uuid.uuid4())
    categories.materials_category.model_type = olca.ModelType.FLOW
    categories.materials_category.olca_type = olca.Category.__name__
    categories.materials_category.name = "Materials"
    response = client.insert(categories.materials_category)
    assert str(response) == 'ok'
    categories.resources_category.id = str(uuid.uuid4())
    categories.resources_category.model_type = olca.ModelType.FLOW
    categories.resources_category.olca_type = olca.Category.__name__
    categories.resources_category.name = "Resources"
    response = client.insert(categories.resources_category)
    assert str(response) == 'ok'
    categories.work_category.id = str(uuid.uuid4())
    categories.work_category.model_type = olca.ModelType.FLOW
    categories.work_category.olca_type = olca.Category.__name__
    categories.work_category.name = "Work"
    response = client.insert(categories.work_category)
    assert str(response) == 'ok'
    return categories


def create_component_flows(client: olca.Client, data: Database, background: Background, categories: FlowCategories):
    # 1.1 Add Flows for each component of the product as well with FlowProperty Number of Items and Category Parts
    print("Converting parts to Flows:")
    parts_category_ref = client.find(olca.Category, categories.parts_category.name)
    for part in data.parts.values():
        part_flow = olca.Flow()
        part_flow.id = str(uuid.uuid4())
        part_flow.flow_type = olca.FlowType.PRODUCT_FLOW
        part_flow.olca_type = olca.Flow.__name__
        part_flow.category = parts_category_ref
        part_flow.name = part.name
        # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
        part_flow.flow_properties = [background.items_factor]
        response = client.insert(part_flow)
        print(2*' ' + part_flow.name + ": " + response)
        assert str(response) == 'ok'


def create_material_flows(client: olca.Client, data: Database, background: Background, categories: FlowCategories):
    # 1.2.1 Convert all materials to Flows with FlowProperty Mass and Category Materials
    print("Converting materials to Flows as materials:")
    materials_category_ref = client.find(olca.Category, categories.materials_category.name)
#        country_ids = set()
#        if id_of_unknown_origin:
#            country_ids.add(id_of_unknown_origin)
#        if id_of_secondary_origin:
#            country_ids.add(id_of_secondary_origin)
#        for material_id in material_ids:
#            material = data.materials[material_id-1]
    for material in data.materials.values():
        material_flow = olca.Flow()
        material_flow.id = str(uuid.uuid4())
        material_flow.flow_type = olca.FlowType.PRODUCT_FLOW
        material_flow.olca_type = olca.Flow.__name__
        material_flow.category = materials_category_ref
        material_flow.name = material.name
        # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
        material_flow.flow_properties = [background.mass_factor]
        response = client.insert(material_flow)
        print(2*' ' + material_flow.name + ": " + response)
        assert str(response) == 'ok'
    # 1.2.2 Convert all materials as resources to Flows with FlowProperty Mass and Category Resources
    print("Converting materials to Flows as resources:")
    resources_category_ref = client.find(olca.Category, categories.resources_category.name)
    #        country_ids = set()
    #        if id_of_unknown_origin:
    #            country_ids.add(id_of_unknown_origin)
    #        if id_of_secondary_origin:
    #            country_ids.add(id_of_secondary_origin)
    #        for material_id in material_ids:
    #            material = data.materials[material_id-1]
    for material in data.materials.values():
        resource_flow = olca.Flow()
        resource_flow.id = str(uuid.uuid4())
        resource_flow.flow_type = olca.FlowType.ELEMENTARY_FLOW
        resource_flow.olca_type = olca.Flow.__name__
        resource_flow.category = resources_category_ref
        resource_flow.name = material.name + " in ground"
        # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
        resource_flow.flow_properties = [background.mass_factor]
        response = client.insert(resource_flow)
        print(2 * ' ' + resource_flow.name + ": " + response)
        assert str(response) == 'ok'


def create_country_flows(client: olca.Client, data: Database, background: Background, categories: FlowCategories):
    # 1.3 Create Flow for work and for each indicator with FlowProperty Mass (!) within Category Work
    print("Converting countries to Flows:")
    work_category_ref = client.find(olca.Category, categories.work_category.name)
    for country in data.countries.values():
        work_flow = olca.Flow()
        work_flow.id = str(uuid.uuid4())
        work_flow.flow_type = olca.FlowType.ELEMENTARY_FLOW
        work_flow.olca_type = olca.Flow.__name__
        work_flow.category = work_category_ref
        work_flow.name = "Work in " + country.name
        work_flow.flow_properties = [background.mass_factor]
        response = client.insert(work_flow)
        print(2 * ' ' + work_flow.name + ": " + response)
        assert str(response) == 'ok'


def create_process_categories(client: olca.Client):
    categories = ProcessCategories()
    categories.countries_process_category.id = str(uuid.uuid4())
    categories.countries_process_category.model_type = olca.ModelType.PROCESS
    categories.countries_process_category.olca_type = olca.Category.__name__
    categories.countries_process_category.name = "Countries extraction"
    response = client.insert(categories.countries_process_category)
    assert str(response) == 'ok'
    categories.materials_process_category.id = str(uuid.uuid4())
    categories.materials_process_category.model_type = olca.ModelType.PROCESS
    categories.materials_process_category.olca_type = olca.Category.__name__
    categories.materials_process_category.name = "Material countries"
    response = client.insert(categories.materials_process_category)
    assert str(response) == 'ok'
    categories.components_process_category.id = str(uuid.uuid4())
    categories.components_process_category.model_type = olca.ModelType.PROCESS
    categories.components_process_category.olca_type = olca.Category.__name__
    categories.components_process_category.name = "Part materials"
    response = client.insert(categories.components_process_category)
    assert str(response) == 'ok'
    # 2.4 Prepare Process Category for products
    categories.products_process_category.id = str(uuid.uuid4())
    categories.products_process_category.model_type = olca.ModelType.PROCESS
    categories.products_process_category.olca_type = olca.Category.__name__
    categories.products_process_category.name = "Device components"
    response = client.insert(categories.products_process_category)
    assert str(response) == 'ok'
    return categories


def create_extraction_processes(client: olca.Client, data: Database, background: Background, categories: ProcessCategories):
    # 2 Processes
    # 2.1 Create Processes with Exchanges from the countries and its indicators
    print("Converting countries to Processes:")
    countries_process_category_ref = client.find(olca.Category, categories.countries_process_category.name)
    gram_unit_ref = client.find(olca.Unit, background.gram_unit.name)
    for country in data.countries.values():
        for material_assignment in country.materials:
            material = data.materials[material_assignment]
            extraction_process = olca.Process()
            extraction_process.id = str(uuid.uuid4())
            extraction_process.process_type = olca.ProcessType.UNIT_PROCESS
            extraction_process.olca_type = olca.Process.__name__
            extraction_process.category = countries_process_category_ref
            extraction_process.location = client.find(olca.Location, country.name)
            extraction_process.name = country.name + " x " + material.name
            print(2 * ' ' + extraction_process.name + ": ", end='')
            extraction_resource_exchange = olca.Exchange()
            extraction_resource_exchange.id = str(uuid.uuid4())
            extraction_resource_exchange.olca_type = olca.Exchange.__name__
            extraction_resource_exchange.quantitative_reference = False
            extraction_resource_exchange.input = True
            extraction_resource_exchange.id = 1
            extraction_resource_exchange.avoided_product = False
            extraction_resource_exchange.flow = client.find(olca.Flow, material.name + " in ground")
            extraction_resource_exchange.flow_property = background.mass_factor
            extraction_resource_exchange.unit = gram_unit_ref
            extraction_resource_exchange.amount = 1.0
            extraction_work_exchange = olca.Exchange()
            extraction_work_exchange.id = str(uuid.uuid4())
            extraction_work_exchange.olca_type = olca.Exchange.__name__
            extraction_work_exchange.quantitative_reference = False
            extraction_work_exchange.input = False
            extraction_work_exchange.id = 2
            extraction_work_exchange.avoided_product = False
            extraction_work_exchange.flow = client.find(olca.Flow, "Work in " + country.name)
            extraction_work_exchange.flow_property = background.mass_factor
            extraction_work_exchange.unit = gram_unit_ref  # TODO This really isn't gram!
            extraction_work_exchange.amount = 1.0
            extraction_material_exchange = olca.Exchange()
            extraction_material_exchange.id = str(uuid.uuid4())
            extraction_material_exchange.olca_type = olca.Exchange.__name__
            extraction_material_exchange.quantitative_reference = True
            extraction_material_exchange.input = False
            extraction_material_exchange.id = 2
            extraction_material_exchange.avoided_product = False
            extraction_material_exchange.flow = client.find(olca.Flow, material.name)
            extraction_material_exchange.flow_property = background.mass_factor
            extraction_material_exchange.unit = gram_unit_ref
            extraction_material_exchange.amount = 1.0
            extraction_process.exchanges = \
                [extraction_resource_exchange, extraction_material_exchange, extraction_work_exchange]
            response = client.insert(extraction_process)
            print(response)
            assert str(response) == 'ok'


def create_material_processes(client: olca.Client, data: Database, background: Background, categories: ProcessCategories):
    # 2.2 Create Processes with Exchanges from the materials and its countries
    print("Converting materials to Processes:")
    gram_unit_ref = client.find(olca.Unit, background.gram_unit.name)
    materials_process_category_ref = client.find(olca.Category, categories.materials_process_category.name)
    for material in data.materials.values():
        material_process = olca.Process()
        material_process.id = str(uuid.uuid4())
        material_process.process_type = olca.ProcessType.UNIT_PROCESS
        material_process.olca_type = olca.Process.__name__
        material_process.category = materials_process_category_ref
        material_process.name = material.name
        print(2 * ' ' + material_process.name + ": ", end='')
        material_exchange = olca.Exchange()
        material_exchange.id = str(uuid.uuid4())
        material_exchange.olca_type = olca.Exchange.__name__
        material_exchange.quantitative_reference = True
        material_exchange.input = False
        material_exchange.id = 1
        material_exchange.avoided_product = False
        material_exchange.flow = client.find(olca.Flow, material.name)
        material_exchange.flow_property = background.mass_factor
        material_exchange.unit = gram_unit_ref
        material_exchange.amount = 1.0
        material_process.exchanges = [material_exchange]
        exchange_id = 2
        if len(material.countries) == 0:  # and (secondary_origin_flow or unknown_origin_flow):
            print("no origin known")
        else:
            for country_assignment in material.countries:
                country = data.countries[country_assignment]
                country_exchange = olca.Exchange()
                country_exchange.id = str(uuid.uuid4())
                country_exchange.olca_type = olca.Exchange.__name__
                country_exchange.quantitative_reference = False
                country_exchange.input = True
                country_exchange.id = exchange_id
                exchange_id += 1
                country_exchange.avoided_product = False
                country_exchange.flow = client.find(olca.Flow, material.name)
                country_exchange.flow_property = background.mass_factor
                country_exchange.unit = gram_unit_ref
                country_exchange.amount = material.countries[country_assignment].amount
                extraction_process = client.find(olca.Process, country.name + " x " + material.name)
                country_exchange.default_provider = extraction_process
                material_process.exchanges.append(country_exchange)
                print('.', end='')
            response = client.insert(material_process)
            print(response)
            assert str(response) == 'ok'


def create_component_processes(client: olca.Client, data: Database, background: Background, categories: ProcessCategories):
    # 2.3 Create Processes with Exchanges from the components and its materials
    components_process_category_ref = client.find(olca.Category, categories.components_process_category.name)
    print("Converting components to Processes:")
    items_unit_ref = client.find(olca.Unit, background.items_unit.name)
    gram_unit_ref = client.find(olca.Unit, background.gram_unit.name)
    for component in data.components.values():
        component_process = olca.Process()
        component_process.id = str(uuid.uuid4())
        component_process.process_type = olca.ProcessType.UNIT_PROCESS
        component_process.olca_type = olca.Process.__name__
        component_process.category = components_process_category_ref
        component_process.name = component.name
        print(2*' ' + component_process.name + ": ", end='')
        component_exchange = olca.Exchange()
        component_exchange.id = str(uuid.uuid4())
        component_exchange.olca_type = olca.Exchange.__name__
        component_exchange.quantitative_reference = True
        component_exchange.input = False
        component_exchange.id = 1
        component_exchange.avoided_product = False
        component_exchange.flow = client.find(olca.Flow, component.name)
        component_exchange.flow_property = background.items_factor
        component_exchange.unit = items_unit_ref
        component_exchange.amount = 1.0
        component_process.exchanges = [component_exchange]
        exchange_id = 2
        for material_assignment in component.materials:
            material = data.materials[material_assignment]
            material_exchange = olca.Exchange()
            material_exchange.id = str(uuid.uuid4())
            material_exchange.olca_type = olca.Exchange.__name__
            material_exchange.quantitative_reference = False
            material_exchange.input = True
            material_exchange.id = exchange_id
            exchange_id += 1
            material_exchange.avoided_product = False
            material_exchange.flow = client.find(olca.Flow, material.name)
            material_exchange.flow_property = background.mass_factor
            material_exchange.unit = gram_unit_ref
            material_exchange.amount = component.weight * component.materials[material_assignment].amount
            material_process = client.find(olca.Process, material.name)
            material_exchange.default_provider = material_process
            component_process.exchanges.append(material_exchange)
            print('.', end='')
        response = client.insert(component_process)
        print(response)
        assert str(response) == 'ok'


def create_impact_assessment_method(client: olca.Client, data: Database):
    # 3 Impact Assessment Method
    impact_method = olca.ImpactMethod()
    impact_method.id = str(uuid.uuid4())
    impact_method.olca_type = olca.ImpactMethod.__name__
    impact_method.name = "FIA"
    impact_method.description = "Fairtronics Impact Assessment (FIA)"
    impact_method.impact_categories = []
    response = client.insert(impact_method)
    assert str(response) == 'ok'
    print("Converting indicators to Impact Categories:")
    for indicator in data.indicators.values():
        impact_category = olca.ImpactCategory()
        impact_category.id = str(uuid.uuid4())
        impact_category.olca_type = olca.ImpactCategory.__name__
        impact_category.name = indicator.name
        impact_category.reference_unit_name = "g"
        impact_category.impact_factors = []
        print(2 * ' ' + impact_category.name + ": ", end='')
        for country_assignment in indicator.countries:
            country = data.countries[country_assignment]
            impact_factor = olca.ImpactFactor()
            impact_factor.id = str(uuid.uuid4())
            impact_factor.olca_type = olca.ImpactFactor.__name__
            impact_factor.flow = client.find(olca.Flow, "Work in " + country.name)
            impact_factor.value = indicator.countries[country_assignment].amount / 100.0
            impact_category.impact_factors.append(impact_factor)
            print('.', end='')
        response = client.insert(impact_category)
        print(response)
        assert str(response) == 'ok'
        impact_method.impact_categories.append(impact_category)
    response = client.update(impact_method)
    assert str(response) == 'ok'


def create_product(client: olca.Client, data: Database, product: Device, background: Background,
                   flow_categories: FlowCategories, process_categories: ProcessCategories) -> olca.ProductSystem:
    print()
    print("For product " + product.name + ":")
    item_unit_ref = client.find(olca.Unit, background.items_unit.name)
    # 1 Create product Flow
    if client.find(olca.Flow, product.name) is None:
        print("  Converting to Flow: ", end='')
        products_flow_category_ref = client.find(olca.Category, flow_categories.devices_category.name)
        product_flow = olca.Flow()
        product_flow.id = str(uuid.uuid4())
        product_flow.flow_type = olca.FlowType.PRODUCT_FLOW
        product_flow.olca_type = olca.Flow.__name__
        product_flow.category = products_flow_category_ref
        product_flow.name = product.name
        product_flow.flow_properties = [background.items_factor]
        response = client.insert(product_flow)
        print(response)
        assert str(response) == 'ok'
    # 2 Create product Process with Exchanges from the products and its components
    print("  Convertig to Process: ", end='')
    products_process_category_ref = client.find(olca.Category, process_categories.products_process_category.name)
    device_process = olca.Process()
    device_process.id = str(uuid.uuid4())
    device_process.process_type = olca.ProcessType.UNIT_PROCESS
    device_process.olca_type = olca.Process.__name__
    device_process.category = products_process_category_ref
    device_process.name = product.name + " (" + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ")"
    device_exchange = olca.Exchange()
    device_exchange.id = str(uuid.uuid4())
    device_exchange.olca_type = olca.Exchange.__name__
    device_exchange.quantitative_reference = True
    device_exchange.input = False
    device_exchange.id = 1
    device_exchange.avoided_product = False
    device_exchange.flow = client.find(olca.Flow, product.name)
    device_exchange.flow_property = background.items_factor
    device_exchange.unit = item_unit_ref
    device_exchange.amount = 1.0
    device_process.exchanges = [device_exchange]
    #       print(2*' ' + product.name + "'s components:")
    exchange_id = 2
    for part_assignment in product.parts:
        part = data.parts[part_assignment]
        part_exchange = olca.Exchange()
        part_exchange.id = str(uuid.uuid4())
        part_exchange.olca_type = olca.Exchange.__name__
        part_exchange.quantitative_reference = False
        part_exchange.input = True
        part_exchange.id = exchange_id
        exchange_id += 1
        part_exchange.avoided_product = False
        part_exchange.flow = client.find(olca.Flow, part.name)
        part_exchange.flow_property = background.items_factor
        part_exchange.unit = item_unit_ref
        part_exchange.amount = product.parts[part_assignment].amount
        device_process.exchanges.append(part_exchange)
        print('.', end='')
    response = client.insert(device_process)
    print(response)
    # 3 Create Product System
    print("  Creating Process System: ", end='')
    product_process_ref = client.find(olca.Process, device_process.name)
    product_product_system_ref = client.create_product_system(
        product_process_ref.id, default_providers='prefer', preferred_type='UNIT_PROCESS')
    print("ok")
    product_product_system = client.find(olca.ProductSystem, device_process.name)
    return product_product_system


def calculation(client: olca.Client, product_system: olca.ProductSystem) -> olca.SimpleResult:
    setup = olca.CalculationSetup()
    setup.calculation_type = olca.CalculationType.UPSTREAM_ANALYSIS
    setup.product_system = product_system
    impact_method = client.find(olca.ImpactMethod, "FIA")
    setup.impact_method = impact_method
    setup.amount = 1.0
    result = client.calculate(setup)
#    client.excel_export(result, 'result.xlsx')
    input_known_origin = {}
    input_unknown_origin = {}
    output_work = {}
    for lci_input in client.lci_inputs(result):
        if "Materials" in lci_input.flow.category_path:
            input_unknown_origin[lci_input.flow.name] = lci_input.value
        elif "Resources" in lci_input.flow.category_path:
            input_known_origin[lci_input.flow.name.split(" ")[0]] = lci_input.value
    for output in client.lci_outputs(result):
        output_work[output.flow.name.split(" ")[2]] = output.value
    print()
    print("Inventory results:")
    print("  Materials with known origin:")
    for flow in sorted(input_known_origin, key=input_known_origin.get, reverse=True):
        print(4*' ' + flow + " : " + str(round(input_known_origin[flow], 6)) + 'g')
    print("  Materials with unknown origin:")
    for flow in sorted(input_unknown_origin, key=input_unknown_origin.get, reverse=True):
        print(4*' ' + flow + " : " + str(round(input_unknown_origin[flow], 6)) + 'g')
    print("  Countries:")
    for flow in sorted(output_work, key=output_work.get, reverse=True):
        print(4*' ' + flow + " : " + str(round(output_work[flow], 6)) + 'g')
    # client.lci_location_contributions(result, flow) nicht so interessant
    # client.lci_total_requirements(result) nicht so interessant
    print()
    print("Impact results:")
    for impact_result in client.lcia(result):
        print(2 * ' ' + impact_result.impact_category.name + ": "
              + str(round(impact_result.value, 6)) + impact_result.impact_category.ref_unit)
        print(4*' ' + "Contribution by country (Location):")
        country_contribution = {}
        for contribution in client.lcia_location_contributions(result, impact_result.impact_category):
            country_contribution[contribution.item.name] = contribution.amount
        for flow in sorted(country_contribution, key=country_contribution.get, reverse=True):
            print(6*' ' + flow + " : " + str(round(country_contribution[flow], 6)) + 'g')
        # lcia_flow_contributions gives the same data. TODO What does this redundancy mean?
        print("  Contribution by country x material (Process):")
        country_x_material_contribution = {}
        for contribution in client.lcia_process_contributions(result, impact_result.impact_category):
            country_x_material_contribution[contribution.item.name] = contribution.amount
        for flow in sorted(country_x_material_contribution, key=country_x_material_contribution.get, reverse=True):
            print(6*' ' + flow + " : " + str(round(country_x_material_contribution[flow], 6)) + 'g')
    return result


def cleanup(client: olca.Client, product_system: olca.ProductSystem, result: olca.SimpleResult):
    client.dispose(result)
#    client.delete(product_system)
#    client.delete(product_system.reference_process)
#    client.delete(product_system.reference_exchange.flow)


def main():
    # Some flags
    product_slug = "nager-it_mouse"  # nager-it_mouse, mnt-reform, device1
    indicator_index = 1  # 1 - New Child Labour, 5 - Forced Labour - 0 - Child Labour
    sub_folder = ""  # normally "", can be used for test data e.g. "demo_tables"
    with_raw_material = True

    # Read data
    data = Database()
    data.init("pocs/data/full", DEVICE_TYPE, product_slug)
    # Select focus
    is_test = "test" in sub_folder
    product = data.devices[product_slug] if not is_test else list(data.devices.values())[0]
    # indicator = list(data.indicators.values())[indicator_index if not is_test else 0]
    # Extablish connection to local OpenLCA
    client = olca.Client(8080)
    # Background
    background = create_background(client)
    create_locations(client, data)
    # Categories
    flow_categories = create_flow_categories(client)
    process_categories = create_process_categories(client)
    # Flows
    create_component_flows(client, data, background, flow_categories)
    create_material_flows(client, data, background, flow_categories)
    create_country_flows(client, data, background, flow_categories)
    # Processes
    create_extraction_processes(client, data, background, process_categories)
    create_material_processes(client, data, background, process_categories)
    create_component_processes(client, data, background, process_categories)
    # LCIA
    create_impact_assessment_method(client, data)
    # Product and Product system
    product_system = create_product(client, data, product, background, flow_categories, process_categories)
    # Calculate it
    result = calculation(client, product_system)
    cleanup(client, product_system, result)


if __name__ == "__main__":
    main()
