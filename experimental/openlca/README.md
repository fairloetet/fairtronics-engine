# OpenLCA conversion

## Aim

This contains code for converting data to OpenLCA and performing the computation within LCA software OpenLCA, 
retrieving the results in return.
One of the aims of this is to use LCA databases like ecoinvent for - at least - the material-materials conversion, 
i.e. getting the base materials (ores and other raw materials), 
which currently is manually taken from old ecoinvent v2 documentation PDF files.

## Preqs

This software has been developed and tested only using Python 3.8 and OpenLCA 1.10.3 installed locally on Windows 10

## Usage

### OpenLCA initial database creation

Within OpenLCA, create a New Database with any name of type "Local" with no contents ("Empty database").
This database is selected now having a few standard empty folders. Start the RPC server using 
Tools -> Developer tools -> IPC server with Port 8080. This preparation is needed for any run and can't be automated.

### Configuration of convert_openlca.py script

Apart from the stadard datetime and uuid libraries, this requires the olca ("OpenLCA") database, available via pip for example. 

In its main() method the flags beforehand selects following data and options:
* product_slug: value in first column (named "slug") of the product, selected from products.csv
* indicator_index: index (starting with 0) of indicator in indicators.csv counted from top... but wait:  
  Currently, the global ids_of_indicators_based_on_probability is relevant for this. It lists indices which should be included.
* sub_folder: if stated, take this data folder within pocs/data instead of pocs/data/full
* with_raw_material: if True, .... well, not implemented yet.

### Running script

Start the script within working directory orgs/pocs, no parameters required.

The run takes quite a few minutes, but at least progress shows up in the console. (#TODO Document it!) 
It starts with simple object (OpenLCA "Flows") creation and progresses with more complex productions (OpenLCA "Process"), i.e. linkages between the Flows.

Immediately after conversion of the data a calculation is done and the result for each indicator. (#TODO Document it!)

The connection is closed by the script. (If it stops errorneously, OpenLCA needs to be restarted for the server to accept connections again.)

The data in OpenLCA persists. To see it, though, the OpenLCA database needs to be closed and re-opned again for the client to catch up (no automatic refresh).
