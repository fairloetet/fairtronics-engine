This contains first experiments with Brightway2, a Python-based open-source library for LCA calculation with features 
probably fitting our (fairly simple) needs for calculation, and with welcome gifts like
* including ecoinvent datasets in the calculation
* including exiobase database in the calculation (reading dowsn't seem to work?)
* quite some samples for e.g. graphics
* uncertaintly calculation (seems to be a key feature)
* a common dictionary of flows and geographies, based on ecoinvent 3.x (with original ecoinvent UUIDs)
* python ways of manipulating given databases (e.g. ecoinvent) with extensions needed for own sake
* a world-wide and innovative LCA community
* a graphical user interface ("activity browser"), if needed 
which we might not easily benefit from using OpenLCA (which is Java-based) or even doing it all by ourselves.  
(Regarding OpenLCA, there's nothing Brightway2 offers out of the box, which OpenLCA doesn't and vise versa, but 
integrating OpenLCA in a Python-based project is neither straightforward nor fast.)
  
Downside is: Bightway isn't easy to understand, at least for someone who doesn't know much about LCIA and is looking 
for a full featured LCA solution. Brightway isn't customer-oriented software, it's a bunch of python routines with
some spare-time written documentation next to it.
  
Having said that in front, we additionally need to be aware of the fact that although our basic idea is to have a risk analysis 
similar to S-LCA, we don't have a "life cycle" approach, and we don't want to compare the results of two products. 
Instead, we treat each LCIA result for a product as _bad_ and are mainly doing contribution analysis on this. 
Especially, we focus of the contribution hotspots and try to find ways to handle them. One of the innovative ideas
is to look for special contribution patterns like "What combination of material and origin (location) contributes most?" 
This is possible because we do _not_ allow a general input-output production graph, but streamline the supply chain to 
a product-material-location sequence of processes. Therefore, we don't need general LCA solutions, and 
self-made calculation code could be easier to understand, extend and present, and faster in the end.

Additionally, we would like to add a new dimension to the LCI (= inventory) and LCIA (= impact assessment) world: the
improvement. It's not part of the standard LCA set, therefore lacking any support by LCA software

Anyway, here you find documentation on Brightway2:
* The main introduction and documentation page is https://2.docs.brightway.dev/. 
  Especially see the part on "Understanding the manual" which suggests reading the docs in the following order:
  * Reading https://2.docs.brightway.dev/intro.html#intro, whohc requires a basic idea on what LCA is
  * Looking at examples within https://2.docs.brightway.dev/notebooks.html#example-notebooks, starting with
    * https://github.com/brightway-lca/brightway2/blob/master/notebooks/Getting%20Started%20with%20Brightway2.ipynb
    * https://github.com/brightway-lca/brightway2/blob/master/notebooks/Activities%20and%20exchanges.ipynb
    * https://github.com/brightway-lca/brightway2/blob/master/notebooks/Using%20calculation%20setups.ipynb
    * https://2.docs.brightway.dev/lca.html
    * https://github.com/brightway-lca/brightway2/blob/master/notebooks/Contribution%20analysis%20and%20comparison.ipynb
  * There are thrid-party introductions like
    * https://github.com/maxkoslowski/Brightway2_Intro/blob/master/BW2_tutorial.ipynb
    * https://github.com/PoutineAndRosti/Brightway-Seminar-2020 and https://github.com/PoutineAndRosti/Brightway-Seminar-2017
    * https://github.com/BenPortner/brightway_recipes
    * More code based: https://github.com/massimopizzol/B4B
* The case base is at https://github.com/brightway-lca (??? #TODO). It was on bitbucket.org?
* For installation #TODO

The current stable version is 2.3, but 2.5 seems to be on its way: https://github.com/brightway-lca/brightway25. 
In fact, the next generation version 3 will simply be named Brightway, see 
* https://chris.mutel.org/next-brightway.html
* https://github.com/brightway-lca/docs/blob/master/notebooks/First%20steps%20with%20Brightway%20version%203.ipynb

Note that there's an old website https://brightwaylca.org which doesn't work anymore although quite a few links still
point to it; in this case replace the subdomain "docs.brightway.org" in the link with "2.docs.brightway.dev". 
The same applies to general https://brightway.dev/ and #TODO

