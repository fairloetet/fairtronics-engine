import brightway2 as bw
import bw2analyzer as ba
from pprint import pprint
from pocs.analysis.database import Database, DEVICE_TYPE

# Some Brightway constants
GRAM_UNIT = 'gram'  # ?? TODO: Is it ok to have just this string? Should it be the more standard 'kilogram'?
NATURAL_RESOURCE_EMISSION_CATEGORY = 'natural resource'  # means: this emission is taken from environment (i.e. input)
PROCESS_ACTIVITY_TYPE = 'process'  # means: this defines an process
PRODUCT_ACTIVITY_TYPE = 'product'  # means: this defines an product flow
EMISSION_ACTIVITY_TYPE = 'emission'  # means: this defines an elementary flow
TECHNOSPHERE_EXCHANGE_TYPE = 'technosphere'  # means: input from other process, i.e. inside the scope
PRODUCTION_EXCHANGE_TYPE = 'production'  # means: output of a process, i.e. input to other processes or final product
BIOSPHERE_EXCHANGE_TYPE = 'biosphere'  # means: input from / output to environment, i.e. outside the scope

# Some Fairtronics constants
ITEM_UNIT = 'item'  # ?? TODO: Is it ok to have just this string? Should it be the more standard 'kilogram'?
WORK_EMISSION_CATEGORY = 'work'
RESOURCES_EMISSION_CATEGORY = 'resource'
PART_MATERIALS_PROCESS_CATEGORY = 'part'
MATERIALS_COUNTRY_PROCESS_CATEGORY = 'material'
COUNTRIES_EXTRACTION_PROCESS_CATEGORY = 'extraction'
DEVICE_PARTS_PROCESS_CATEGORY = 'device'


def create_work_flows(db: bw.Database, data: Database):
    print("Converting work in country to flows:")
    for country in data.countries.values():
        activity = db.new_activity(
            "work_in_" + country.slug,
            name="Work in " + country.name,
            type=EMISSION_ACTIVITY_TYPE,
            unit=GRAM_UNIT,
            location=country.code,
            # There's no standard Brightway/Ecoinvent category for work, they only have 'air', 'soil', and 'water'
            #   for emissions into the natural environment and 'natural resource' for consumption of nature.
            #   TODO Should I really use NATURAL_RESOURCE_EMISSION_CATEGORY = 'natural resource' here ???
            categories=[WORK_EMISSION_CATEGORY, NATURAL_RESOURCE_EMISSION_CATEGORY])
        # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
        activity.save()
        print(2 * ' ' + activity['name'])


def create_resource_flows(db: bw.Database, data: Database):
    print("Converting resources (materials in ground) to flows:")
    for material in data.materials.values():
        activity = db.new_activity(
            material.slug + "_in_ground",
            name=material.name + " in ground",
            type=EMISSION_ACTIVITY_TYPE,
            unit=GRAM_UNIT,
            # For location take the default from user preferences, usually 'GLO'
            # 'natural resource' is expected by Brightway/Ecoinvent for bioshere flows from the natural environment!
            categories=[RESOURCES_EMISSION_CATEGORY, NATURAL_RESOURCE_EMISSION_CATEGORY])
        # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
        activity.save()
        print(2 * ' ' + activity['name'])


def create_extraction_processes(db: bw.Database, data: Database):
    print("Converting countries to activities:")
    for country in data.countries.values():
        for material_assignment in country.materials:
            material = data.materials[material_assignment]
            # Create activity of process type for production of material in country, i.e. a flow 'country x material'
            activity = db.new_activity(
                country.slug + "_x_" + material.slug,
                name=material.name + " from " + country.name,
                type=PROCESS_ACTIVITY_TYPE,
                unit=GRAM_UNIT,
                supply_chain_tier='extraction',
                location=country.code,
                categories=[COUNTRIES_EXTRACTION_PROCESS_CATEGORY])
            activity.save()
            print(2 * ' ' + activity['name'], end='')
            # Create production result exchange (not really necessary if amount = 1 g)
            # TODO Is this a problem in the analysis? Is this more ecoinvent-like?
            production_exchange = activity.new_exchange(
                input=activity,  # Production result treated as an input as well
                type=PRODUCTION_EXCHANGE_TYPE,  # means: result of the process
                amount=1.0,  # 1 gram
                uncertainty_type=0)  # means: unknown uncertainty
            production_exchange.save()
            print('.', end='')
            # Create a resource as input from the biosphere (elementary flow)
            ground_exchange = activity.new_exchange(
                # TODO Here, the transformation to the weight "in ground" (ie. ore, etc.) is missing
                input=(db.name, material.slug + "_in_ground"),  # as defined in create_resource_flows()
                type=BIOSPHERE_EXCHANGE_TYPE,  # means: elementary input from outside the scope
                amount=1.0,  # in gram TODO Is it better to have negative value here because it consumes?
                uncertainty_type=0)  # means: unknown uncertainty
            ground_exchange.save()
            print('.', end='')
            # Create work as input (not emission!) to the biosphere (elementary flow)
            work_exchange = activity.new_exchange(
                input=(db.name, "work_in_" + country.slug),
                type=BIOSPHERE_EXCHANGE_TYPE,  # means: input to a process
                amount=1.0,  # we still stuck for weight, i.e. this is work for 1g of the material, positive = emission
                uncertainty_type=0)  # means: unknown uncertainty
            work_exchange.save()
            print('.', end='')
            print()


def create_material_processes(db: bw.Database, data: Database):
    print("Converting materials to processes with input from countries:")
    for material in data.materials.values():
        activity = db.new_activity(
            material.slug,
            name=material.name,
            type=PROCESS_ACTIVITY_TYPE,
            unit=GRAM_UNIT,
            supply_chain_tier='material',
            # for location take the default from user preferences, usually 'GLO'
            categories=[MATERIALS_COUNTRY_PROCESS_CATEGORY])
        # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
        activity.save()
        print(2 * ' ' + activity['name'] + ' ', end='')
        if len(material.countries) == 0:
            print("- no origin known")
        else:
            # Create production result exchange (not really necessary if amount = 1 g)
            # TODO Is this a problem in the analysis? Is this more ecoinvent-like?
            production_exchange = activity.new_exchange(
                input=activity,  # Production result treated as an input as well
                type=PRODUCTION_EXCHANGE_TYPE,  # means: result of the process
                amount=1.0,  # 1 gram
                uncertainty_type=0)  # means: unknown uncertainty
            production_exchange.save()
            print('.', end='')
            for country_assignment in material.countries:
                country = data.countries[country_assignment]
                amount = material.countries[country_assignment].amount
                material_country_exchange = activity.new_exchange(
                    input=(db.name, country.slug + "_x_" + material.slug),  # defined in create_extraction_processes()
                    type=TECHNOSPHERE_EXCHANGE_TYPE,
                    amount=amount,  # in gram
                    uncertainty_type=0)  # means: unknown uncertainty
                material_country_exchange.save()
                print(".", end='')
            print()


def create_part_processes(db: bw.Database, data: Database):
    print("Converting parts/components to processes:")
    for part in data.parts.values():
        manufacturer = data.manufacturers[part.manufacturer]
        activity = db.new_activity(
            part.slug,
            name=part.name,
            type=PROCESS_ACTIVITY_TYPE,
            unit=GRAM_UNIT,
            supply_chain_tier='part',
            # for location take the default from user preferences, usually 'GLO'
            categories=[PART_MATERIALS_PROCESS_CATEGORY, str(manufacturer.slug), str(part.category)])
        activity.save()
        # Create production result exchange (not really necessary if amount = 1 g)
        # TODO Is this a problem in the analysis? Is this more ecoinvent-like?
        print(2*' ' + activity['name'] + ' ', end='')
        production_exchange = activity.new_exchange(
            input=activity,  # Production result treated as an input as well
            type=PRODUCTION_EXCHANGE_TYPE,  # means: result of the process
            amount=1,  # 1 gram TODO It's strange that we currently model parts with refenece unit Gram instead of Item
            uncertainty_type=0)  # means: unknown uncertainty
        production_exchange.save()
        print('.', end='')
        for material_assignment in part.materials:
            material = data.materials[material_assignment]
            amount = part.materials[material_assignment].amount
            component_material_exchange = activity.new_exchange(
                input=(db.name, material.slug),  # as defined in create_material_processes()
                type=TECHNOSPHERE_EXCHANGE_TYPE,
                amount=amount,  # in linear percent of 1 gram of the component/part
                uncertainty_type=0)  # means: unknown uncertainty
            component_material_exchange.save()
            print('.', end='')
        print()


def create_device_processes(db: bw.Database, data: Database):
    print("Converting product to process:")
    for product in data.devices.values():
        # We need the complete weight of the device
        product_weight = 0.0
        for part_assignment in product.parts:
            part = data.parts[part_assignment]
            amount = product.parts[part_assignment].amount
            product_weight += part.weight * amount
        activity = db.new_activity(
            product.slug,
            name=product.name,
            type=PROCESS_ACTIVITY_TYPE,
            unit=ITEM_UNIT,
            supply_chain_tier='device',
            # for location take the default from user preferences, usually 'GLO'
            categories=[DEVICE_PARTS_PROCESS_CATEGORY])
        activity.save()
        print(2*' ' + product.name + ' ', end='')
        production_exchange = activity.new_exchange(
            input=(db.name, product.slug),  # Production result treated as an input as well
            type=PRODUCTION_EXCHANGE_TYPE,  # means: result of the process
            amount=1,  # 1 unit
            uncertainty_type=0)  # means: unknown uncertainty
        production_exchange.save()
        print('.', end='')
        for part_assignment in product.parts:
            part = data.parts[part_assignment]
            amount = product.parts[part_assignment].amount
            product_part_exchange = activity.new_exchange(
                input=(db.name, part.slug),
                type=TECHNOSPHERE_EXCHANGE_TYPE,
                amount=amount * part.weight / product_weight,  # in items
                uncertainty_type=0)  # means: unknown uncertainty
            product_part_exchange.save()
            print('.', end='')
        print()


def create_impact_assessment_methods(db: bw.Database, data: Database):
    print("Converting indicators to impact methods:")
    for indicator in data.indicators.values():
        print(2 * ' ' + indicator.name + " ", end='')
        method_key = (indicator.slug,)
        method = bw.Method(method_key)
        method_metadata = {
            'name': indicator.name,
            'unit': GRAM_UNIT,
            'description': "Part of the Fairtronics Impact Assessment (FIA)"
        }
        method.register(**method_metadata)
        method_factors = []
        # Judging the work in the countries
        for country_assignment in indicator.countries:
            country = data.countries[country_assignment]
            impact_factor = [(db.name, "work_in_" + country.slug),
                             indicator.countries[country_assignment].amount / 100.0]
            method_factors.append(impact_factor)
            print('.', end='')
        assert method.validate(method_factors)
        method.write(method_factors)
        print()


def main(project_name: str, database_name: str, data: Database) -> bw.Database:
    # Set up brightway
    # bw.bw2setup()  # Sets up ecoinvent-like flows as a standard
    bw.projects.set_current(project_name)
    if database_name in bw.databases:
        del bw.databases[database_name]
    db = bw.Database(database_name)
    db.register()
    method_keys = []
    for method_key in bw.methods:
        method_keys.append(method_key)
    for method_key in method_keys:
        if method_key in bw.methods.df:
            method = bw.Method(method_key)
            method.deregister()

    # Create elementary flows
    create_resource_flows(db, data)  # for each material, we have a resource = material in ground
    create_work_flows(db, data)  # for each country, we have a

    # Note that some Brigthway routines doesn't work well with seperating product flows from their production process,
    #   so we don't create stand-alone product flows of type PRODUCT_ACTIVITY_TYPE, but expect them to be implicitly
    #   meant as result of process activities output.

    # Create production processes
    create_extraction_processes(db, data)  # extraction work of materials in countries
    create_material_processes(db, data)  # market of materials with input from all countries
    create_part_processes(db, data)  # material composition of parts/components
    create_device_processes(db, data)  # bill of components of device/product

    # Create Methods
    create_impact_assessment_methods(db, data)

    return db


def display_activities(db: bw.Database):
    for act in db.search("*"):
        print(act['name'] + " (of type " + act['type'] + ")")
        print("  technosphere:")
        for exc in act.technosphere():
            print("    " + str(exc))
        print("  biosphere:")
        for exc in act.biosphere():
            print("    " + str(exc))
        print("  production:")
        for exc in act.production():
            print("    " + str(exc))


def display_lca_dicts():
    print("Processes:")
    pprint(lca.reverse_dict()[0])
    print("Products:")
    pprint(lca.reverse_dict()[1])
    print("Emissions:")
    pprint(lca.reverse_dict()[2])


def display_lca_result():
    print("Contribution analysis:")
    ba.print_recursive_calculation(device, impact)
    print()
    print("Top emissions:")
    for x in ba.ContributionAnalysis().annotated_top_emissions(lca):
        if float(x[0]) > 0.0:
            print(x)
    print()
    print("Top processes:")
    for x in ba.ContributionAnalysis().annotated_top_processes(lca):
        if float(x[0]) > 0.0:
            print(x)
    print()
    print("The result: " + str(lca.score))
    print()


def display_methods(db: bw.Database):
    print("Impact methods:")
    for method_key in bw.methods:
        method = bw.Method(method_key)
        print("  " + method.metadata['name'])
        for cf in method.load():
            print("    " + str(cf[1]) + " for " + db.get(cf[0][1])['name'])


if __name__ == "__main__":
    # Some flags
    bw_project = "ft_table"  # fairtronics, ft_table
    bw_database = "ft_table"  # fairtronics, ft_table
    product_slug = "device1"  # nager-it_mouse/mnt-reform, device1
    indicator_slug = "indicator1"  # child-labour/forced-labour/wage-risk/wage-chance, indicator1
    sub_folder = "demo_tables"  # normally "", can be used for test data e.g. "demo_tables"

    # Convert data, show resulting database, and calculate LCA result
    print()
    print("Conversion")
    print("----------")
    fairtronics_data = Database()
    fairtronics_data.init("pocs/data/full", DEVICE_TYPE, product_slug)
    brightway_database = main(bw_project, bw_database, fairtronics_data)
    device = brightway_database.get(product_slug)
    impact = (indicator_slug,)
    print()
    # print("Database")
    # print("--------")
    # display_activities(brightway_database)
    # display_methods(brightway_database)
    print()
    print("Supply chain")
    print("------------")
    ba.print_recursive_supply_chain(device)
    print()
    print("LCA (all values in gram)")
    print("------------------------")
    one_of_a_device = {device: 1}
    lca = bw.LCA(one_of_a_device, impact)
    lca.lci()
    lca.lcia()
    # display_lca_dicts()
    display_lca_result()
    pass


def create_sample_brightway_data() -> bw.LCA:
    if "testdb" in bw.databases:
        del bw.databases["testdb"]
    t_db = bw.Database("testdb")
    t_db.write({
        ("testdb", "Electricity production"): {
            'name': 'Electricity production',
            'unit': 'kWh',
            'exchanges': [{
                'input': ('testdb', 'Fuel production'),
                'amount': 2,
                'unit': 'kg',
                'type': 'technosphere'
            }, {
                'input': ('testdb', 'Carbon dioxide'),
                'amount': 1,
                'unit': 'kg',
                'type': 'biosphere'
            }, {
                'input': ('testdb', 'Sulphur dioxide'),
                'amount': 0.1,
                'unit': 'kg',
                'type': 'biosphere'
            }, {
                'input': ('testdb', 'Electricity production'),  # important to write the same process name in output
                'amount': 10,
                'unit': 'kWh',
                'type': 'production'
            }]
        },
        ('testdb', 'Fuel production'): {
            'name': 'Fuel production',
            'unit': 'kg',
            'exchanges': [{
                'input': ('testdb', 'Carbon dioxide'),
                'amount': 10,
                'unit': 'kg',
                'type': 'biosphere'
            }, {
                'input': ('testdb', 'Sulphur dioxide'),
                'amount': 2,
                'unit': 'kg',
                'type': 'biosphere'
            }, {
                'input': ('testdb', 'Crude oil'),
                'amount': -50,
                'unit': 'kg',
                'type': 'biosphere'
            }, {
                'input': ('testdb', 'Fuel production'),
                'amount': 100,
                'unit': 'kg',
                'type': 'production'
            }]
        },
        ('testdb', 'Carbon dioxide'): {'name': 'Carbon dioxide', 'unit': 'kg', 'type': 'emission'},
        ('testdb', 'Sulphur dioxide'): {'name': 'Sulphur dioxide', 'unit': 'kg', 'type': 'emission'},
        ('testdb', 'Crude oil'): {'name': 'Crude oil', 'unit': 'kg', 'type': 'emission'}
    })
    # First, for a demonstration, do the LCI only
    functional_unit = {t_db.get("Electricity production"): 1000}
    lca = bw.LCA(functional_unit)
    lca.lci()
    print("LCA inventory (0 values not displayed):")
    print(lca.inventory)
    # The results are as follows:
    #
    #               | Carbon | Sulphur |  Oil
    #               |    0   |    1    |   2
    # --------------+--------+---------+-------
    # Electricity 0 |   100  |    10   |    0
    # --------------+--------+---------+-------
    #        Fuel 1 |    20  |     4   | -100
    #
    # 1000kWh Electricity needs 100 x the Electricity process, i.e. 100kg Carbon, 10kg Sulphur, and 200kg Fuel,
    #   which itself needs 2x the Fuel process, i.e. another 20kg Carbon and 4kg Sulphur, plus -100kg Oil
    # Now, do a LCIA which doubles the value of Sulphur in comparison to Carbon and ignores the Oil
    lcia_data = [[('testdb', 'Carbon dioxide'), 1.0],
                 [('testdb', 'Sulphur dioxide'), 2.0]]
    method_key = ('testmethod', 'imaginaryendpoint', 'imaginarymidpoint')
    my_method = bw.Method(method_key)
    my_method.validate(lcia_data)  # returns "TRUE"
    my_method.register()
    my_method.write(lcia_data)
    my_method.load()  # check everything works
    lca = bw.LCA(functional_unit, method_key)  # run LCA calculations again with method
    lca.lci()
    lca.lcia()
    print("LCIA inventory:")
    print(lca.characterized_inventory)
    # The follwoing lists the both activity in order of contribution
    print("LCIA top activites:")
    for a in lca.top_activities():
        print(a)
    # For the LCA mathematics h = CBA-1f, following are the matrices
    # Here's the processes and what it expects from other processes or what it produces (A)
    print("Technosphere matrix (0 values not displayed):")
    print(lca.technosphere_matrix)
    # Here's what comes from or goes to the environment, as specified in the processes (B):
    print("Biosphere matrix (0 values not displayed):")
    print(lca.biosphere_matrix)
    # Here's the LCIA method vector (C):
    print("Characterization matrix:")
    print(lca.characterization_matrix)
    # Here's what we wanted (f): 1000kWh Electricity (and 0kWh Oil)
    print("Demand array:")
    print(lca.demand_array)
    # Result (h), which is just lca.characterized_inventory.sum()
    print("Result: ", end='')
    print(lca.score)
    return lca


def create_work_flows_direct(db: bw.Database, data: Database):
    print("Converting work in country to flows:")
    for country in data.countries.values():
        activity_name = "Work in " + country.name
        db.write({
            # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
            (db.name, "work_in_" + country.slug): {
                'name': activity_name,
                'type': EMISSION_ACTIVITY_TYPE,
                'unit': GRAM_UNIT,
                # For location take the default from user preferences, usually 'GLO'
                # There's no standard Brightway/Ecoinvent category for work, they only have 'air', 'soil', and 'water'
                #   for emissions into the natural environment. TODO Should I really use 'natural resource' instead ???
                'categories': [WORK_EMISSION_CATEGORY, NATURAL_RESOURCE_EMISSION_CATEGORY]}
        })
        print(2 * ' ' + activity_name)


def create_resource_flows_direct(db: bw.Database, data: Database):
    print("Converting resources (materials in ground) to flows:")
    for material in data.materials.values():
        activity_name = material.name + " in ground"
        db.write({
            # TODO Set description, cas number, formula and synonyms as well. Adding categories would be great.
            (db.name, material.slug + "_in_ground"): {
                'name': activity_name,
                'type': EMISSION_ACTIVITY_TYPE,
                'unit': GRAM_UNIT,
                # For location take the default from user preferences, usually 'GLO'
                # 'natural resource' is expected by Brightway/Ecoinvent for bioshere flows from the natural environment!
                'categories': [RESOURCES_EMISSION_CATEGORY, NATURAL_RESOURCE_EMISSION_CATEGORY]
            }
        })
        print(2 * ' ' + activity_name)


