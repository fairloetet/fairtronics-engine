from fairtronics_engine.model import Assignment
from fairtronics_engine.debug import Debug
from typing import Callable, Generic, TypeVar  # , Type, get_args
import numpy as np


T = TypeVar("T")
NT = TypeVar("NT", bound=np.generic)
OT = TypeVar("OT", bound=object)


class CubeMethod(Generic[T]):
    one: T
    zero: T
    func: Callable[[Assignment], T]

    def __init__(self, one: T, zero: T, func: Callable[[Assignment], T]):
        # self.type = get_args(self.__orig_bases__[0])[0]  # TODO Really?? Odd...
        self.zero = zero
        self.one = one
        self.func = func


class NumpyCubeMethod(CubeMethod[NT]):
    pass


class ObjectCubeMethod(CubeMethod[OT]):
    dtype = object
    pass


class ViewMethod(NumpyCubeMethod[np.float64]):
    dtype = np.float64

    def __init__(self):
        super().__init__(np.float64(1.0), np.float64(0.0), lambda assignment: np.float64(assignment.amount))


class DebugMethod(ObjectCubeMethod[Debug]):
    def __init__(self):
        super().__init__(Debug("1"), Debug(""),
                         lambda assignment: Debug(f"{assignment.entity_slug}->{assignment.assigned_slug}"))
