# from line_profiler_decorator import profiler as profile
from typing import List, Dict, Any, Optional, Set
import pandas as pd
from fairtronics_engine.cube import Cube
from fairtronics_engine.database import Database, Tier, Slug, CHAIN, PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, \
                                        LOCATION, INDICATOR
from fairtronics_engine.static_cube import StaticCube
from fairtronics_engine.cube_method import ViewMethod
from fairtronics_engine.model import BaseModel
from fairtronics_engine.view import View
from fairtronics_engine.statics import NAME, SHORT, UNIT, AMOUNT, SHARE, TYPE, RANK, \
                                       OTHER_BOTTOM_NAME, OTHER_BOTTOM_SHORT, OTHER_BOTTOM_SLUG, \
                                       OTHER_NAME, OTHER_SHORT, OTHER_SLUG, OTHER_TOP_NAME, \
                                       OTHER_TOP_SHORT, OTHER_TOP_SLUG, WITH_OTHER_NONE, \
                                       WITH_OTHER_SINGLE, WITH_OTHER_TOP_BOTTOM, WITH_OTHER_TOP_BOTTOM_SINGLE


NO_MORE_ENTITY_TYPE = 0
INDICATORS_ENTITY_TYPE = 1
LOCATIONS_ENTITY_TYPE = 2
RESOURCES_ENTITY_TYPE = 3
MATERIALS_ENTITY_TYPE = 4
PARTS_ENTITY_TYPE = 5
PART_MANUFACTURER_ENTITY_TYPE = 51
PART_CATEGORY_ENTITY_TYPE = 52
DEVICES_ENTITY_TYPE = 6
PORTFOLIOS_ENTITY_TYPE = 7

entity_type_for_tier: Dict[Tier, int] = {
    INDICATOR: INDICATORS_ENTITY_TYPE,
    LOCATION: LOCATIONS_ENTITY_TYPE,
    RESOURCE: RESOURCES_ENTITY_TYPE,
    MATERIAL: MATERIALS_ENTITY_TYPE,
    PART: PARTS_ENTITY_TYPE,
    DEVICE: DEVICES_ENTITY_TYPE,
    PORTFOLIO: PORTFOLIOS_ENTITY_TYPE
}


class Analysis():
    database: Database
    cube: Cube
    unit_tier: Tier
    unit_tier_name: str
    unit_names: List[str]
    selection: List[Tier]
    filters: Dict[Tier, Set[Slug]]   # TODO: Rename it to filter
    depth: Tier

    # TODO object-Angaben sollten eigentlich erst hier wichtig sein, sind aber
    #   schon in die Database eingewoben
    # TODO Die supply tier sollte sich aus den Anfragen ergeben und nicht hier
    #   fest vorgegeben
    def __init__(self, database: Database, unit_tier: Tier, supply_chain: Dict[Tier, Set[Slug]],
                 selection: List[Tier], depth: Tier, filters: Dict[Tier, Set[Slug]]):

        self.database = database
        self.unit_tier = unit_tier

        # Collect focussed objects
        # We rely on having the selection in supply chain order
        self.selection = sorted(selection)

        # Retain filters only for those below the object_type, ignore the others
        self.filters = dict((k, filters[k]) for k in filters if k in CHAIN.get_chain_below(unit_tier))

        # Depth remains as is
        self.depth = depth

        # Create data cube
        self.cube = StaticCube(database, ViewMethod(), supply_chain, self.selection, self.filters, self.depth)
        # TODO Calculation could be deferred
        self.cube.calculate()

        # Some basic data for the root object
        objects = self.database.get_entities(unit_tier, supply_chain[unit_tier])
        object_count = len(objects)
        self.unit_names = [obj.name for obj in objects]
        self.unit_tier_name = unit_tier.singular() \
            if object_count == 1 else unit_tier.plural()

    # ------------------------

    # TODO eval is evil
    def query(self, statement: str) -> View:
        return eval("self." + statement)

    # TODO eval is evil
    def plot(self, statement: str):
        return eval("self." + statement)

    # @profile
    def view_cut_other(self, cutoff: float) -> View:
        return self.view(0, cutoff=cutoff, ascending=False, with_other=WITH_OTHER_SINGLE)

    def view_top_other(self, n: int, cutoff: float = 0.0) -> View:
        return self.view_head(n, ascending=False, cutoff=cutoff, with_other=WITH_OTHER_SINGLE)

    def view_top(self, n: int, cutoff: float = 0.0, with_other: str = WITH_OTHER_NONE) -> View:
        return self.view_head(n, ascending=False, cutoff=cutoff, with_other=with_other)

    def view_bottom_other(self, n: int, cutoff: float = 0.0) -> View:
        return self.view_tail(n, ascending=False, cutoff=cutoff, with_other=WITH_OTHER_SINGLE)

    def view_bottom(self, n: int, cutoff: float = 0.0, with_other: str = WITH_OTHER_NONE) -> View:
        return self.view_tail(n, ascending=False, cutoff=cutoff, with_other=with_other)

    def view_head(self, n: int, cutoff: float = 0.0, ascending: bool = False,
                  with_other: str = WITH_OTHER_NONE) -> View:
        return self.view(start=0, end=n, cutoff=cutoff, ascending=ascending, with_other=with_other)

    def view_tail(self, n: int, cutoff: float = 0.0, ascending: bool = False,
                  with_other: str = WITH_OTHER_NONE) -> View:
        return self.view(start=-n, cutoff=cutoff, ascending=ascending, with_other=with_other)

    # TODO Provide documentation
    # TODO Add more functionality here, e.g. columns to view or sorted_by
    def view(self, start: Optional[int] = 0, end: Optional[int] = None, cutoff: Optional[float] = 0.0,
             ascending: bool = False, with_other: str = WITH_OTHER_NONE) -> View:
        # Has been: remaining, filter_entities, filter_indices = self.cube.view(selection, depth, filters)
        # ------------>
        # TODO filter_entities und filter_indices sind eigentlich keine Sache des Cube
        # TODO remaining_indices wird nur zum iterieren benutzt. Es enthält Koordinaten im Ergebniswürfel, die der
        #   interessiert und nicht 0 sind.
        #   Kann man da nicht besser die Werte in einer Liste hintereinander aufreihen zusammen mit ihrer Koordinate?
        #   remaining_indices ist das schon, fehlen nur noch die Werte parallel, die aus view_cude geholt werden
        #   Wenn ich also zwei parallele Listen habe, Koordinate und Wert, dann liefert zip() einen Iterator von Tupeln
        # TODO view_sum ließe sich berechnen aus der Liste der Werte, wenn ich sie vorlägen (bei Iterator ja nicht)
        #   Also sollte Cube auch die sum selbst parat haben. Die lässt sich bei dessen Aufbau ja leicht ermitteln

        # Schön wäre es, einen Iterator von Cube zu bekommen. (Der bekommt später die View-Wünsche,
        # um lazy agieren zu können. zunächst aber liefert er einfach alles, es wird hier ein
        # Datafarme zusammengebaut und nachträglich die View-Wünsche ausgerechnt.) Dieser
        # Iterator liefert in beliebiger Reihenfolge Tupel (Koordinate, Wert). Die Koordinate (selbst ein n-Tupel)
        # orientiert sich an der sortierten Selection, d.h. erster Wert ist erste Selection bis
        # zur n-ten Selection. Der Würfel selbst liest aus der Database aufgrund der Selection, Filter und Depth
        # aus. Die Database liefert dazu eine Methode get_entity(type, slug) -> EntityObject und
        # get_association(type, slug1, slug2) -> EntityAssociation und get_entities(type). Genau die slugs
        # liefert er auch wieder zurück. Aussparen tut er die Null-Werte. Der Cube muss sich die slugs merken,
        # nicht mehr die Entities selbst.

        # Get entities for each axis in reduced and restricted cube
        # Beispiel mnt_reform_chromite.resources = mnt_reform_chromite.resources[[27]]
        # TODO Schon so ähnlich mit den filter_entities ermittelt?
        # Has been: entities_along_axes = self.get_list_of_entities_according_to_axes(selection, filter_indices)
        # entity_type_names = get_entity_type_names_according_to_axes(axes)
        # heading = " x ".join(entity_type_names)
        slugs_n_values, top_sum, middle_sum, bottom_sum, size = \
            self.cube.values(cutoff=cutoff, start=start, end=end, ascending=ascending)
        all_sum = top_sum + middle_sum + bottom_sum

        # Build the data for the resulting DataFrame
        values: List[Any] = []
        entities_names: List[str] = []
        entities_short_names: List[str] = []
        entities_keys: List[str] = []
        data: Dict[str, List[Any]] = {}
        additional_data: Dict[str, List[Any]] = {}
        ranks: List[int] = []
        last_rank_value = 0.0
        last_rank: int = 0
        rank_increase = 1
        for slugs_list, value in slugs_n_values:
            all_entities = [self.database.get_entity(self.selection[index], slug)
                            for index, slug in enumerate(slugs_list)]
            entities_key = "|".join([e.slug for e in all_entities])
            entities_keys.append(entities_key)
            entities_name = " x ".join([e.name for e in all_entities])
            entities_names.append(entities_name)
            entities_short_name = " x ".join([e.short_name for e in all_entities])
            entities_short_names.append(entities_short_name)
            values.append(value)
            if value != last_rank_value:
                last_rank_value = value
                last_rank += rank_increase
                rank_increase = 1
            else:
                rank_increase += 1
            ranks.append(last_rank)
            # Add additional data
            for i in range(len(slugs_list)):
                entity_slug = slugs_list[i]
                additional_columns = self.get_entity_type_specific_columns(entity_type_for_tier[self.selection[i]])
                # Add additional name columns if more than one entity is selected
                # TODO Should be set in front of all these additional columns
                # TODO This includes double fething of entities
                if len(self.selection) > 1:
                    additional_columns[SHORT] = NO_MORE_ENTITY_TYPE
                entity = self.database.get_entity(self.selection[i], entity_slug)
                for column in additional_columns:
                    column_name = self.selection[i].singular().lower() + " " + \
                                  column.replace("_", " ")
                    if additional_columns[column] != NO_MORE_ENTITY_TYPE:
                        # Ok, we have a sub entity type specified
                        attr_entity_type = int(additional_columns[column])
                        attr_slug = getattr(entity, column)
                        if len(attr_slug) == 0:  # No sub-entity specified for this entity
                            value = ''
                        else:
                            attr_entity = self.get_entity(attr_entity_type, attr_slug)
                            # TODO Currently, only "short_name" is taken as specific column value
                            column = SHORT
                            column = column.replace(" ", "_")
                            value = getattr(attr_entity, column)  # TODO except AttributeError:
                    else:
                        column = column.replace(" ", "_")
                        value = getattr(entity, column)  # TODO except AttributeError:
                    if column_name in additional_data:
                        additional_data[column_name].append(value)
                    else:
                        additional_data[column_name] = [value]
        if len(values) > 0:
            data[RANK] = ranks
            data[AMOUNT] = values
            data[UNIT] = self.depth.unit
            if all_sum > 0.0:
                data[SHARE] = [value * 100.0 / all_sum for value in values]
            else:
                data[SHARE] = 0.0
            data[NAME] = entities_names
            data[SHORT] = entities_short_names
            data.update(additional_data)
            data[TYPE] = "-".join(self.database.get_names_of_tiers(self.selection, True)).lower()
            # Add other row, depending of kind of display (with_other) and cutoff threshold
            other_sum = top_sum + bottom_sum
            if with_other != WITH_OTHER_NONE and other_sum > cutoff:
                if with_other == WITH_OTHER_SINGLE or \
                   ((start == 0 or end == size) and with_other == WITH_OTHER_TOP_BOTTOM_SINGLE):
                    entities_keys.append(OTHER_SLUG)
                    data[RANK].append(last_rank + 1)
                    data[AMOUNT].append(other_sum)
                    data[SHARE].append(other_sum * 100.0 / all_sum)
                    data[NAME].append(OTHER_NAME)
                    data[SHORT].append(OTHER_SHORT)
                    for additional_column in additional_data:
                        data[additional_column].append('')
                elif WITH_OTHER_TOP_BOTTOM in with_other:
                    if top_sum > 0.0:
                        top_entities_keys = [OTHER_TOP_SLUG]
                        top_rank = [0]
                        top_amount = [top_sum]
                        top_share = [top_sum * 100.0 / all_sum]
                        top_name = [OTHER_TOP_NAME]
                        top_short = [OTHER_TOP_SHORT]
                        top_additional = [""]
                    else:
                        top_entities_keys = []
                        top_rank = []
                        top_amount = []
                        top_share = []
                        top_name = []
                        top_short = []
                        top_additional = []
                    if bottom_sum > 0.0:
                        bottom_entities_keys = [OTHER_BOTTOM_SLUG]
                        bottom_rank = [last_rank + 1]
                        bottom_amount = [bottom_sum]
                        bottom_share = [bottom_sum * 100.0 / all_sum]
                        bottom_name = [OTHER_BOTTOM_NAME]
                        bottom_short = [OTHER_BOTTOM_SHORT]
                        bottom_additional = [""]
                    else:
                        bottom_entities_keys = []
                        bottom_rank = []
                        bottom_amount = []
                        bottom_share = []
                        bottom_name = []
                        bottom_short = []
                        bottom_additional = []
                    entities_keys = top_entities_keys + entities_keys + bottom_entities_keys
                    data[RANK] = top_rank + data[RANK] + bottom_rank
                    data[AMOUNT] = top_amount + data[AMOUNT] + bottom_amount
                    data[SHARE] = top_share + data[SHARE] + bottom_share
                    data[NAME] = top_name + data[NAME] + bottom_name
                    data[SHORT] = top_short + data[SHORT] + bottom_short
                    for additional_column in additional_data:
                        data[additional_column] = top_additional + data[additional_column] + bottom_additional
                else:
                    raise ValueError(f"Unit.view() parameter 'with_only' only accepts options {WITH_OTHER_NONE}, "
                                     "{WITH_OTHER_SINGLE}, {WITH_OTHER_TOP_BOTTOM} or {WITH_OTHER_TOP_BOTTOM_SINGLE}")
        # print(f"(Top sum: {top_sum})")
        # print(f"(Middle sum: {middle_sum})")
        # print(f"(Bottom sum: {bottom_sum})")

        # Construct title
        singular: bool = (self.selection[0] is self.unit_tier)
        title = ("combinations of " if len(self.selection) > 1 else "") + \
            "/".join(self.database.get_names_of_tiers(self.selection, singular))
        number_of_filter_elements = sum(map(len, self.filters.values()))
        if 0 < number_of_filter_elements <= 5:
            title = title + " (" + (" & ".join([",".join([entity.name for entity in
                                                          self.database.get_entities(tier, self.filters[tier])])
                                                for tier in self.filters])) + " only)"
        elif number_of_filter_elements > 5:
            title = title + " (" + str(number_of_filter_elements) + " of them only)"
        if self.depth is not self.selection[-1]:
            level = self.database.get_name_of_tier(self.depth, False) + " level"
            title = level + " of " + title

        # That's it.
        ix = pd.Index(entities_keys, str, False, 'slug', False)
        df = pd.DataFrame(data, index=ix)

        # Add contribution column
        # TODO Einfach die Summe bilden ist vielleicht je nach View nicht korrekt,
        #   wichtig ist ja die Gewichtsebene zu beachten, mit der verglichen wird
        # TODO Schauen, ob diese nicht lazy berechnet und gespeichert werden sollte
        # TODO Und tja, nach entfernen von Assessment geht das nicht mehr so einfach, daher erstmal auskommentiert
        # inventory_sum = self.cube.sum()
        # contrib_column = 100 * inventory_view.df[AMOUNT] / inventory_sum
        # inventory_view.df.insert(inventory_view.df.columns.get_loc(SHARE)+1, CONTRIB, contrib_column)

        # Add title
        object_title = f"{self.unit_tier_name} '{','.join(self.unit_names)}'" \
            if len(self.unit_names) < 5 \
            else f"{len(self.unit_names)} {self.unit_tier_name}"
        # Früher wurde mal schon hier sortiert: df.sort_values(by=[AMOUNT], ascending=False)
        return View(df, all_sum, size, object_title, [title])

    # -------------------------

    # TODO Should be part of database, but here, part manufacturer and part category is included as well
    def get_entity(self, entity_type: int, slug: Slug) -> BaseModel:
        return {
            INDICATORS_ENTITY_TYPE: self.database.indicators,
            LOCATIONS_ENTITY_TYPE: self.database.locations,
            RESOURCES_ENTITY_TYPE: self.database.resources,
            MATERIALS_ENTITY_TYPE: self.database.materials,
            PARTS_ENTITY_TYPE: self.database.parts,
            PART_MANUFACTURER_ENTITY_TYPE: self.database.manufacturers,
            PART_CATEGORY_ENTITY_TYPE: self.database.part_categories,
            DEVICES_ENTITY_TYPE: self.database.devices,
            PORTFOLIOS_ENTITY_TYPE: self.database.portfolios
        }[entity_type][slug]

    # TODO Should be part of database, but here, part manufacturer and part category is included as well
    def get_entity_type_specific_columns(self, entity_type: int) \
            -> Dict[str, int]:
        return {INDICATORS_ENTITY_TYPE: {'direction': NO_MORE_ENTITY_TYPE,
                                         'usage': NO_MORE_ENTITY_TYPE},
                LOCATIONS_ENTITY_TYPE: {'region': NO_MORE_ENTITY_TYPE,
                                        'sub_region': NO_MORE_ENTITY_TYPE},
                RESOURCES_ENTITY_TYPE: {'circularity': NO_MORE_ENTITY_TYPE},
                MATERIALS_ENTITY_TYPE: {'cas': NO_MORE_ENTITY_TYPE,
                                        'category': NO_MORE_ENTITY_TYPE,
                                        'density_g_cm3': NO_MORE_ENTITY_TYPE},
                PART_MANUFACTURER_ENTITY_TYPE: {},
                PART_CATEGORY_ENTITY_TYPE: {},
                PARTS_ENTITY_TYPE: {'manufacturer': PART_MANUFACTURER_ENTITY_TYPE,
                                    'category': PART_CATEGORY_ENTITY_TYPE},
                DEVICES_ENTITY_TYPE: {},
                PORTFOLIOS_ENTITY_TYPE: {}
                }[entity_type]
