from __future__ import annotations
from typing import List, Dict, Any, Callable, Optional
import pandas as pd
from IPython.core.display import HTML
from fairtronics_engine.statics import SLUG, NAME, SHORT, UNIT, AMOUNT, SHARE, TYPE, RANK
from warnings import warn


class View:
    df: pd.DataFrame
    weight_sum: float
    weight_count: int
    object_title: str
    title_fragments: List[str]

    def __init__(self, data: pd.DataFrame, weight_sum: float, weight_count: int, object_title: str, title_fragments: List[str]):
        self.df = data
        self.weight_count = weight_count
        self.weight_sum = weight_sum
        self.title_fragments = title_fragments
        self.object_title = object_title

    def title(self, prefix: str = "", suffix: str = ":"):
        title = prefix + (" ".join(self.title_fragments))
        title = title.capitalize().replace(" ,", ",").replace("( ", "(").replace(" )", ")")
        title = title.replace("\u201c ", "\u201c").replace(" \u201d", "\u201d")
        if len(self.object_title) > 0:
            title = ((title + " in ") if len(self.title_fragments) > 0 else "") + self.object_title
        title = title + suffix
        return title

    def sum_of_all_amounts(self) -> float:
        return self.weight_sum

    def number_of_all_entities(self) -> int:
        return self.weight_count

    def dataframe(self):
        return self.df

    def slugs(self):
        return ",".join(list(self.df.index.values))

    # ---------------------

    def value(self):
        print(self.title("Value of "))
        if self.df.empty:
            return 0.0
        return self.df[AMOUNT].squeeze()

    def value_unit(self):
        print(self.title("Value with unit of "))
        value = self.df[AMOUNT].squeeze() if not self.df.empty else 0.0
        unit = self.df[UNIT].squeeze()
        return f"{str(value)} {str(unit)}"

    def amount(self):
        print(self.title("Amount of "))
        if self.df.empty:
            return 0.0
        return self.df[AMOUNT].squeeze()

    def count(self):
        print(self.title("Number of "))
        if self.df.empty:
            return 0
        return len(self.df.index)

    def sum(self, column: str = AMOUNT):
        print(self.title("Sum of amount of "))
        if self.df.empty:
            return "No data available"
        return self.df[column].sum()

    def dump(self, index: bool = False, **kwargs: Any) -> str:
        print(self.title())
        if self.df.empty:
            return "No data available"
        # if TYPE in self.df:
        #    self.df.drop([TYPE], axis=1, inplace=True)
        # if SHORT in self.df:
        #    self.df.drop([SHORT], axis=1, inplace=True)
        # return self.sorted().df.to_string(index=index, **kwargs) # TODO .sorted() weg? Wirklich Zeit kostet es nicht
        return self.df.to_string(index=index, **kwargs)

    def table(self, max_rows: Optional[int] = None, index: bool = False, columns: Optional[List[str]] = None, **kwargs):
        print(self.title("%d " % (max_rows,) if max_rows else ""))
        if TYPE in self.df and (columns is None or TYPE not in columns):
            self.df.drop([TYPE], axis=1, inplace=True)
        if SHORT in self.df and (columns is None or SHORT not in columns):
            self.df.drop([SHORT], axis=1, inplace=True)
        # TODO Check if already sorted by previous view, if not call self.sorted() before calling show()
        return HTML(self.show(columns).df.to_html(
            index=index, float_format='{:.4f}'.format, max_cols=None,
            justify='right', max_rows=max_rows, columns=columns, **kwargs))

    def pie(self, y: str = AMOUNT, label: str = '', column: str = SHORT, legend: bool = False, **kwargs):
        print(self.title())
        sorted_pable = self.sorted()
        if 'textprops' not in kwargs:
            kwargs['textprops'] = {}
        if 'fontsize' not in kwargs['textprops']:
            kwargs['textprops']['fontsize'] = 12
        # TODO  autopct='%.2f%%' zeigt nicht den share an, sondern den Anteil mit Gesamtkreis = 100
        sorted_pable.df.plot.pie(y=y, ylabel=label, labels=sorted_pable.df[column], legend=legend, **kwargs)

    def bar(self, x: str = SHORT, y: str = AMOUNT, xlabel: str = '',
            ylabel: str = AMOUNT.capitalize(), legend: bool = False, **kwargs):
        print(self.title())
        # TODO Check if already sorted
        sorted_pable = self.sort(by=AMOUNT, ascending=True)  # .sorted()
        if 'fontsize' not in kwargs:
            kwargs['fontsize'] = 12
        sorted_pable.df.plot.barh(x=x, y=y, xlabel=xlabel, ylabel=ylabel, legend=legend, **kwargs)

    # ---------------------

    def sort(self, by: str = AMOUNT, ascending: bool = False) -> View:
        return View(self.df.sort_values(by=by, ascending=ascending), self.weight_sum, self.weight_count, self.object_title,
                    self.title_fragments + (["(", "sorted by", by, ")"] if by != AMOUNT else []))

    def sorted(self) -> View:
        if RANK in self.df:
            return self.sort(by=RANK, ascending=True)
        if AMOUNT in self.df:
            return self.sort(by=AMOUNT, ascending=False)
        if SHARE in self.df:
            return self.sort(by=SHARE, ascending=False)
        if NAME in self.df:
            return self.sort(by=NAME, ascending=True)
        return self

    def cut(self, threshold: float = 1.0, column: str = SHARE) -> View:
        return View(self.df[self.df[column] >= threshold].copy(), self.weight_sum, self.weight_count, self.object_title,
                    self.title_fragments + ["with", column, "of at least", str(threshold)])

    def cutoff(self, threshold: float = 1.0, column: str = SHARE) -> View:
        return self.cut(threshold, column)

    def cutother(self, threshold: float = 1.0, column: str = SHARE, other_name: str = 'Other') -> View:
        other_df = self.df[self.df[column] < threshold]
        if len(other_df) > 0:
            other_row = other_df.sum(numeric_only=True)
            cut_pable = self.cut(threshold, column)
            self.add_standard_columns_for_other_row(other_row, other_name, len(cut_pable.df) + 1)
            return self.create_pable_including_other_row(other_row, cut_pable, other_name)
        else:
            return self.cutoff(threshold, column)

    def grouped(self, by: str) -> View:
        return self.group(by)

    def group(self, by: str) -> View:
        # TODO Ich habe hier vieles versucht... agg geht nicht, weil so viel unklar ist.
        try:
            # TODO Unit muss gerettet werden, weil nicht-numerisch. Das ist hässlich.
            unit = None
            if UNIT in self.df:
                unit = self.df[UNIT].max()  # Retrieves the one unit
            gfo = self.df.groupby(by, as_index=False).sum(numeric_only=True)
            if unit and AMOUNT in gfo:
                gfo.insert(gfo.columns.get_loc(AMOUNT) + 1, UNIT, unit)
            gfo[NAME] = gfo[by]
            gfo[SHORT] = gfo[by]
            if RANK in self.df:
                gfo.drop([RANK], axis=1, inplace=True)
            return View(gfo, self.weight_sum, self.weight_count, self.object_title, self.title_fragments + [",", "grouped by", by])
        except KeyError:
            raise ValueError("Unknown column " + by + " in grouping operation")

    # TODO: top sollte das ganze nicht sortiert hinterlassen
    def top(self, number: int = 5, by: str = AMOUNT) -> View:
        return View(self.sort(by=by).head(number).df, self.weight_sum, self.weight_count, self.object_title,
                    ["top", str(number)] + ([] if by == AMOUNT else ["(", "by", by, ")"])
                    + self.title_fragments)

    def topother(self, number: int = 5, by: str = AMOUNT, other_name: str = 'Other') -> View:
        row_count = len(self.df) - number
        if row_count > 0:
            other_df = self.sort(by=by).tail(row_count).df
            if len(other_df) > 0:
                other_row = other_df.sum(numeric_only=True)
                top_pable = self.top(number, by)
                self.add_standard_columns_for_other_row(other_row, other_name, len(top_pable.df) + 1)
                return self.create_pable_including_other_row(other_row, top_pable, other_name)
        return self.top(number, by)

    # TODO: bottom sollte das ganze nicht sortiert hinterlassen
    def bottom(self, number: int = 5, by: str = AMOUNT) -> View:
        return View(self.sort(by=by, ascending=True).head(number).df, self.weight_sum, self.weight_count, self.object_title,
                    ["bottom", str(number)] + ([] if by == AMOUNT else ["(", "by", by, ")"])
                    + self.title_fragments)

    def bottomother(self, number: int = 5, by: str = AMOUNT, other_name: str = 'Other') -> View:
        row_count = len(self.df) - number
        if row_count > 0:
            other_df = self.sort(by=by, ascending=True).tail(row_count).df
            if len(other_df) > 0:
                other_row = other_df.sum(numeric_only=True)
                bottom_pable = self.bottom(number, by)
                self.add_standard_columns_for_other_row(other_row, other_name, 1)
                return self.create_pable_including_other_row(other_row, bottom_pable, other_name)
        return self.bottom(number, by)

    def other(self, number: int = 5, by: str = AMOUNT) -> View:
        result = self.bottom(len(self.df) - number, by)
        result.title_fragments[0] = "All but top"
        result.title_fragments[1] = str(number)
        return result

    def range(self, start: int = 0, end: int = 5, by: str = AMOUNT, ascending: bool = False,
              other_names: List[str] = ['Other (top)', 'Other (bottom)']) -> View:
        start = max(0, start)
        end = min(self.count(), end)
        if end <= start:
            warn(f'Range endpoint {end} should be greater than start {start}.', RuntimeWarning)
            return View(pd.DataFrame(columns=self.df.columns), self.weight_sum, self.weight_count, self.object_title,
                        self.title_fragments)
        if start > self.count():
            warn(f'Range startpoint and endpoint lie outside of the Pable.', RuntimeWarning)
            return View(pd.DataFrame(columns=self.df.columns), self.weight_sum, self.weight_count, self.object_title,
                        self.title_fragments)
        sorted_pable = self.sort(by=by, ascending=ascending)
        range_pable = sorted_pable.head(end).tail(end - start)
        if end < self.count():
            bottom_other_row = sorted_pable.tail(self.count() - end).dataframe().sum(numeric_only=True)
            self.add_standard_columns_for_other_row(bottom_other_row, other_names[1], self.count() + 1)
            range_pable = self.create_pable_including_other_row(bottom_other_row, range_pable, other_names[1])
        if start > 0:
            top_other_row = sorted_pable.head(start).dataframe().sum(numeric_only=True)
            self.add_standard_columns_for_other_row(top_other_row, other_names[0], 0)
            range_pable = self.create_pable_including_other_row(top_other_row, range_pable, other_names[0])
        return range_pable.sort(by=RANK, ascending=True)

    def all(self) -> View:
        return View(self.df, self.weight_sum, self.weight_count, self.object_title, ["all"] + self.title_fragments)

    def head(self, number: int = 5) -> View:
        return View(self.df.head(number), self.weight_sum, self.weight_count, self.object_title, 
                    ["first", str(number), "of"] + self.title_fragments)

    def tail(self, number: int = 5) -> View:
        return View(self.df.tail(number), self.weight_sum, self.weight_count, self.object_title, 
                    ["last", str(number), "of"] + self.title_fragments)

    def select(self, column: str, value: object = None) -> View:
        if value is None:
            # Take column parameter as value for index/slug
            return View(self.df.loc[[column]], self.weight_sum, self.weight_count, self.object_title,
                        self.title_fragments + ["index", column, "is", column])
        else:
            return View(self.df[self.df[column].str.match('^' + str(value) + "$")], self.weight_sum, self.weight_count, self.object_title,
                        self.title_fragments + ["where", column, "is", str(value)])

    def exclude(self, column: str, value) -> View:
        return View(self.df[self.df[column] != value], self.weight_sum, self.weight_count, self.object_title,
                    self.title_fragments + [",", "without", str(value), column])

    def show(self, columns: Optional[List[str]] = None) -> View:
        if columns is None:
            return self
        available_columns = [c for c in columns if c in self.df]
        return View(self.df[available_columns], self.weight_sum, self.weight_count, self.object_title, self.title_fragments)

    # ----------------------------------------------------

    def percentageof(self, whole: View, column: str = AMOUNT) -> View:
        result = self.calculate_columns_for_relations(whole.df, {UNIT: '%'},
                                                      lambda df: 100.0 * df[column + '_x'] / df[column + '_y'])
        return View(result, self.weight_sum, self.weight_count, self.object_title,
                    ["percentage of", "\u201c"] + self.title_fragments + ["\u201d", "within", "\u201c"] +
                    whole.title_fragments + ["\u201d"]).sorted()

    def differenceto(self, whole: View, column: str = AMOUNT) -> View:
        result = self.calculate_columns_for_relations(whole.df, {UNIT: 'diff.'},
                                                      lambda df: df[column + '_x'] - df[column + '_y'])
        return View(result, self.weight_sum, self.weight_count, self.object_title,
                    ["difference between", "\u201c"] + self.title_fragments + ["\u201d", "and", "\u201c"] +
                    whole.title_fragments + ["\u201d"]).sorted()

    def distanceto(self, whole: View, column: str = AMOUNT) -> View:
        result = self.calculate_columns_for_relations(whole.df, {UNIT: 'dist.'},
                                                      lambda df: abs(df[column + '_x'] - df[column + '_y']))

        return View(result, self.weight_sum, self.weight_count, self.object_title,
                    ["distance between", "\u201c"] + self.title_fragments + ["\u201d", "and", "\u201c"] +
                    whole.title_fragments + ["\u201d"]).sorted()

    def multipleof(self, whole: View, column: str = AMOUNT) -> View:
        result = self.calculate_columns_for_relations(whole.df, {UNIT: 'times'},
                                                      lambda df: df[column + '_x'] / df[column + '_y'])
        return View(result, self.weight_sum, self.weight_count, self.object_title,
                    ["multiple of", '"'] + self.title_fragments + ['"', "within", '"'] +
                    whole.title_fragments + ['"'])

    # Helper --------------------

    def calculate_columns_for_relations(self, whole_df: pd.DataFrame, column_values: Dict[str, str],
                                        amount_function: Callable[[pd.DataFrame], pd.Series[Any]]) -> pd.DataFrame:
        result = pd.merge(self.df, whole_df, left_index=True, right_index=True, sort=False)
        result[AMOUNT] = amount_function(result)
        resulting_columns = [AMOUNT, UNIT, NAME, SHORT]
        if UNIT in column_values:
            if len(column_values[UNIT]) > 0:
                result[UNIT] = column_values[UNIT]
            else:
                resulting_columns.remove(UNIT)
        else:
            result[UNIT] = result[UNIT + '_x']
        if NAME in column_values:
            if len(column_values[NAME]) > 0:
                result[NAME] = column_values[NAME]
            else:
                resulting_columns.remove(NAME)
        else:
            result[NAME] = result[NAME + '_x']
        if SHORT in column_values:
            if len(column_values[SHORT]) > 0:
                result[SHORT] = column_values[SHORT]
            else:
                resulting_columns.remove(SHORT)
        else:
            result[SHORT] = result[SHORT + '_x']
        return result[resulting_columns]

    def add_standard_columns_for_other_row(self, other_row: pd.Series, other_name: str, position_of_other: int):
        if NAME in self.df:
            other_row[NAME] = other_name
        if SHORT in self.df:
            other_row[SHORT] = other_name
        if UNIT in self.df:
            other_row[UNIT] = self.df[UNIT].max()  # Retrieves the one unit
        if TYPE in self.df:
            other_row[TYPE] = self.df[TYPE].max()  # Retrieves the one unit
        if RANK in self.df:
            other_row[RANK] = position_of_other

    def create_pable_including_other_row(self, other_row: pd.Series, main_pable: View, other_name: str):
        main_df = main_pable.df
        main_df.loc[other_name.lower()] = other_row
        main_df.fillna("", inplace=True)
        return View(main_df, self.weight_sum, self.weight_count, self.object_title, 
                    main_pable.title_fragments + ["(", "aside", other_name, ")"])


# ----------------------------------------------------

def Comparison(views: List[View]) -> View:
    if len(views) < 2:
        raise ValueError("At least two views are needed to compare them.")
    weight_sum = 0.0
    weight_count = 0
    for view in views:
        if len(view.dataframe()) > 1:
            raise ValueError("Only views with one row may be compared.")
        weight_sum += view.weight_sum
        weight_count += view.weight_count
    result = pd.concat([x.df for x in views])
    return View(result, weight_sum, weight_count, "comparison",
                [" and ".join((", ".join([x.object_title for x in views[:-1]]), views[-1].object_title))])
