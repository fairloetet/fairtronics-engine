from typing import List, Dict

class Source:
    desc: str

    def __init__(self, desc: str):
        self.desc = desc
        
    def _m(self, left, right):
        if left == "" or right == "":
            return ""
        else:
            if left == "1":
                if right == "1":
                    return "1"
                else:
                    return right
            else:
                if right == "1":
                    return left
                else:
                    return f"{left} ** {right}"

    def _a(self, left, right):
        if left == "":
            if right == "":
                return ""
            else:
                return right
        else:
            if right == "":
                return left
            else:
                return f"{left} ++ {right}"

    def __add__(self, other):
        return Source(self._a(self.desc, other.desc))
        
    def __mul__(self, other):
        return Source(self._m(self.desc, other.desc))

    def __iadd__(self, other):
        self.desc = self._a(self.desc, other.desc)
        return self
        
    def __imul__(self, other):
        self.desc = self._m(self.desc, other.desc)
        return self
        
    # Don't really need this
    def __radd__(self, other):
        return Source(self._a(str(other), self.desc))
        
    # Don't really need this
    def __rmul__(self, other):
        return Source(self._m(str(other), self.desc))
        
    def __str__(self, other):
        return self.desc
    
    def __repr__(self):
        return "S[" + self.desc + "]"