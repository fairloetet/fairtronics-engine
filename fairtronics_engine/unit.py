# from line_profiler_decorator import profiler as profile
import re
from typing import Dict, List, Tuple, Optional, Set
from fairtronics_engine.analysis import Analysis
from fairtronics_engine.explanation import Explanation
from fairtronics_engine.database import Database, Tier, Slug, CHAIN, PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, \
                                        LOCATION, INDICATOR
from fairtronics_engine.cube_method import ViewMethod


class Unit:

    database: Database
    # TODO Maybe it's better to store the Entities instead of the slugs, since the caller may
    #       already know them and provided its slugs while here we get entities back from the slugs?
    supply_chain: Dict[Tier, Set[Slug]]
    tier: Tier

    # @profile
    def __init__(self, database: Database, object_tier: Tier, object_slugs_specs: str,
                 spec: Optional[str] = None, filter_spec: Optional[Dict[Tier, str]] = None):

        self.database = database
        self.supply_chain = dict()
        self.tier = object_tier

        # Collect root objects
        slugs = self.database.get_entity_slugs(object_tier)
        base_slugs: List[Slug] = []
        for object_slugs_spec in [oss.strip() for oss in object_slugs_specs.split(',')]:  # TODO Remove dated , support
            attr_regex = re.compile(r"^" + object_slugs_spec.replace("(", "\\(").replace(")", "\\)") + "$")
            base_slugs.extend(list(filter(attr_regex.match, slugs)))  # TODO Use list comprehension
        if len(base_slugs) == 0:
            raise ValueError("No matching %s for %s" % (object_tier.singular(), object_slugs_specs))

        # Build the index to calculate the size of the cube upfront
        # First, check if a spec can be converted to filters
        if filter_spec and spec:
            raise ValueError("Don't specifiy a spec string and a non-empty filter, do it for none or either of them.")
        if filter_spec is None and spec is not None:
            _, _, filter_spec = compile_spec_string(spec)
        filters = extract_filtered_slugs(database, filter_spec)
        # No go through
        for this_tier in CHAIN.tiers.values():  # Starting at the upper-most tier
            if this_tier < self.tier:  # Don't start yet
                continue
            elif this_tier == self.tier:  # Special treatment for explicitly given distinct base objects
                self.supply_chain[this_tier] = set(base_slugs)  # Already the objects to analyse
            else:  # Build up this tier by those which come in due to the prevoius tier
                prev_tier = CHAIN.prev_of(this_tier)
                if prev_tier:
                    self.supply_chain[this_tier] = set()
                    for this_slug in self.supply_chain[prev_tier]:
                        for slug in database.get_assignee_slugs(prev_tier, this_slug):
                            if filters is None or this_tier not in filters or slug in filters[this_tier]:
                                self.supply_chain[this_tier].add(slug)

    def analyse(self, spec: Optional[str] = None, selection: Optional[List[Tier]] = None,
                depth: Optional[Tier] = None, filter_spec: Optional[Dict[Tier, str]] = None) -> Analysis:
        if filter_spec is None and depth is None and selection is None:
            if spec is None or len(spec) == 0 or spec == "*":
                spec = "fdpmrci"[self.tier.position():]  # Avoid all tiers above the object_type
            selection, depth, filter_spec = compile_spec_string(spec)  # Has been: , selection_in_uppercase=False)
        elif spec is not None:
            raise ValueError("Don't specifiy a spec string and a non-empty filter, selection and/or depth, "
                             "do it for either of them.")
        if selection is None or len(selection) == 0:
            raise ValueError("Provide at least one selection to inventory.")
        if depth is None:
            depth = max(selection)
        # Has been: elif depth not in selection:
        # Has been:     selection.append(depth)
        filters = extract_filtered_slugs(self.database, filter_spec)
        return Analysis(self.database, self.tier, self.supply_chain, selection, depth, filters)

    def explain(self, spec: Optional[str] = None, path_spec: Optional[Dict[Tier, str]] = None) -> Explanation:
        if path_spec is None:
            if spec is not None:
                _, _, path_spec = compile_spec_string(spec)
            else:
                raise ValueError("Either specifiy a view_spec or provide non-empty filter.")
        elif spec is not None:
            raise ValueError("Either specifiy a view_spec or provide non-empty filter, not both of them.")
        # Convert the str in the path to Slugs, for scrict typing reasons
        raw_path = extract_filtered_slugs(self.database, path_spec)
        for tier in raw_path:
            if len(raw_path[tier]) > 1:
                raise ValueError(f"For tier {tier.name} {len(raw_path[tier])} entities are specified."
                                 "At most one is permitted.")
        # We are only interested in the one slug per tier
        path = {tier: Slug(slugs.pop()) for tier, slugs in raw_path.items()}
        return Explanation(self.database, self.tier, self.supply_chain, path)

    # --------------------------


def compile_spec_string(spec: str, selection_in_uppercase: bool = True) -> Tuple[List[Tier], Tier, Dict[Tier, str]]:

    letter_to_tier = {'F': PORTFOLIO,
                      'D': DEVICE,
                      'P': PART,
                      'M': MATERIAL,
                      'R': RESOURCE,
                      'C': LOCATION,
                      'I': INDICATOR,
                      'f': PORTFOLIO,
                      'd': DEVICE,
                      'p': PART,
                      'm': MATERIAL,
                      'r': RESOURCE,
                      'c': LOCATION,
                      'i': INDICATOR}

    filters_from_spec: Dict[Tier, str] = {}
    selection_from_spec: List[Tier] = []
    depth_from_spec: Tier | None = None
    latest_letter = ""
    first_letter = ""
    in_filter_spec = False
    letter: str
    for i, letter in enumerate(spec):
        if letter in "FDPMRCIfdpmrci_ " and not in_filter_spec:
            if letter == "_" or letter == " ":
                continue
            if (letter.isupper() and selection_in_uppercase) or not selection_in_uppercase:
                selection_from_spec.append(letter_to_tier[letter])
            if not depth_from_spec:
                depth_from_spec = letter_to_tier[letter]
            else:
                depth_from_spec = max(depth_from_spec, letter_to_tier[letter])
            if not first_letter:
                first_letter = letter
            latest_letter = letter
        elif letter == "[" and latest_letter and not in_filter_spec:
            filters_from_spec[letter_to_tier[latest_letter]] = ""
            in_filter_spec = True
        elif letter == "]" and in_filter_spec:
            in_filter_spec = False
        elif in_filter_spec and latest_letter:
            filters_from_spec[letter_to_tier[latest_letter]] += letter
        else:
            raise ValueError("Did not expect letter %s (at position %d) in view spec %s" % (letter, i, spec))
    if not depth_from_spec:
        depth_from_spec = max(max(selection_from_spec), max(filters_from_spec.keys()))
    # if len(selection_from_spec) == 0 and selection_in_uppercase:
    #     raise ValueError("View spec badly defined: No upper case letter given.")
    return selection_from_spec, depth_from_spec, filters_from_spec


def extract_filtered_slugs(database: Database, filter_spec: Optional[Dict[Tier, str]] = None) -> Dict[Tier, Set[Slug]]:
    # Collect all filtered slugs specified
    filters: Dict[Tier, Set[Slug]] = dict()
    if filter_spec:
        for tier in filter_spec:
            filters_equations = filter_spec[tier].split(',')
            for filter_equation in filters_equations:
                filter_attr_value = filter_equation.split('=', 1)
                if len(filter_attr_value) == 1:
                    filter_attr = "slug"
                    filter_value = filter_attr_value[0]
                else:
                    filter_attr = filter_attr_value[0]
                    filter_value = filter_attr_value[1]
                filter_attr = filter_attr.strip()
                filter_value = filter_value.strip()
                value_regex = re.compile(r"^" + filter_value.replace("(", "\\(").replace(")", "\\)") + "$")
                try:
                    machting_slugs = [entity.slug for entity in database.get_entities(tier)
                                      if value_regex.match(str(getattr(entity, filter_attr)))]
                except AttributeError as ae:
                    raise ae
                if len(machting_slugs) == 0:
                    raise ValueError(f"Couldn't find a matching {tier.singular().lower()} where {filter_attr} is {filter_value}.")
                if tier in filters:
                    filters[tier] &= set(machting_slugs)
                else:
                    filters[tier] = set(machting_slugs)
    return filters
