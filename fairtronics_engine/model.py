"""
Contains Pydantic models for elementary building blocks
"""
from typing import Dict, Optional, List, Generic, TypeVar
from pydantic import BaseModel, validator


class Slug(str):
    # def __new__(cls, s: str):
    #    return str.__new__(cls, s)
    pass


class Tier:
    name: str
    name_plural: str
    index: int
    letter: str
    unit: str
    prev: Optional['Tier']
    next: Optional['Tier']

    def __init__(self, index: int, name: str, name_plural: str, letter: str, unit: str):
        self.name = name
        self.name_plural = name_plural
        self.index = index
        self.letter = letter
        self.unit = unit
        self.prev = None
        self.next = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return self.index

    def __eq__(self, other: object):
        if isinstance(other, Tier):
            return self.index == other.index
        return False

    def __ne__(self, other: object):
        if isinstance(other, Tier):
            return self.index != other.index
        return False

    def __gt__(self, other: object):
        if isinstance(other, Tier):
            return self.index > other.index
        return False

    def __ge__(self, other: object):
        if isinstance(other, Tier):
            return self.index >= other.index
        return False

    def __lt__(self, other: object):
        if isinstance(other, Tier):
            return self.index < other.index
        return False

    def __le__(self, other: object):
        if isinstance(other, Tier):
            return self.index <= other.index
        return False

    def singular(self):
        return self.name

    def plural(self):
        return self.name_plural

    def position(self):
        return self.index


class Chain:
    tiers: Dict[int, Tier]

    def __init__(self, *tiers: Tier):
        self.tiers = dict()
        current = None
        for tier in tiers:
            self.tiers[tier.position()] = tier
            if current:
                current.next = tier
                tier.prev = current
            current = tier

    def get_tier(self, index: int) -> Tier:
        return self.tiers[index]
    
    def get_chain_below(self, start_tier: Tier) -> List[Tier]:
        return [tier for index, tier in self.tiers.items() if index >= start_tier.position()]
    
    def next_of(self, tier: Tier) -> Tier | None:
        return tier.next

    def prev_of(self, tier: Tier) -> Tier | None:
        return tier.prev

    def is_next_of(self, tier: Tier, other: Tier) -> bool:
        return other == self.next_of(tier)

    def is_prev_of(self, tier: Tier, other: Tier) -> bool:
        return other == self.prev_of(tier)


class ErrorMessage(BaseModel):
    detail: str


class Category(BaseModel):
    slug: Slug
    name: str
    short_name: str


class Assignment(BaseModel):
    entity_slug: Slug
    assigned_slug: Slug
    amount: float
    source: str


TAssignment = TypeVar("TAssignment", bound=Assignment)


class Entity(BaseModel, Generic[TAssignment]):
    slug: Slug
    name: str
    short_name: str

    def assignments(self) -> Dict[Slug, TAssignment]:
        raise NotImplementedError("This Entity is designed not to have Assignments.")

    def __eq__(self, other: object):
        return self.__hash__() == other.__hash__()

    def __hash__(self) -> int:
        return hash(self.slug)


class NoAssignment(Assignment):
    pass


class Indicator(Entity[NoAssignment]):
    direction: str  # 'ascending': higher value -> higher risk. 'decending': lower value -> higher chance
    probability_based: bool  # True means: can be used with latest Fairtronics engine
    usage: Optional[str]
    data_base: Optional[str] = ""

    @validator('direction')
    def valid_directions(cls, v):
        if v not in ['ascending', 'descending']:
            raise ValueError("Must be one of 'ascending', 'descending'")
        return v

    @validator('usage')
    def valid_usage(cls, v):
        if v not in ['straight', 'inverse', 'unknown']:
            raise ValueError("Must be one of 'straight', 'inverse', 'unknown'")
        return v

    def assignments(self) -> Dict[Slug, Assignment]:
        return {}


class IndicatorAssignment(Assignment):
    pass


class Location(Entity[IndicatorAssignment]):
    indicators: Dict[Slug, IndicatorAssignment]
    code: Optional[str]
    region: Optional[str]
    sub_region: Optional[str]
#    materials: Optional[Dict[str, MaterialAssignment]]
#    resources: Optional[Dict[str, ResourceAssignment]]

    def assignments(self) -> Dict[Slug, IndicatorAssignment]:
        return self.indicators


class LocationAssignment(Assignment):
    pass


class Resource(Entity[LocationAssignment]):
    locations: Dict[Slug, LocationAssignment]
    circularity: str

    @validator('circularity')
    def valid_cicularities(cls, v):
        if v not in ['virgin', 'recycled']:
            raise ValueError("Must be one of 'virgin', 'recycled'")
        return v

    def assignments(self) -> Dict[Slug, LocationAssignment]:
        return self.locations


class ResourceAssignment(Assignment):
    pass


class MaterialCategory(Category):
    parent: str


class Material(Entity[ResourceAssignment]):
    resources: Dict[Slug, ResourceAssignment]
    category: Optional[str]
    cas: Optional[str]
    density_g_cm3: Optional[str]  # float not applicable because empty value is permitted but not identical to 0.0

    def assignments(self) -> Dict[Slug, ResourceAssignment]:
        return self.resources


class MaterialAssignment(Assignment):
    pass


class ComponentCategory(Category):
    parent: str


class PartCategory(Category):
    parent: str


class Manufacturer(Entity[NoAssignment]):
    company_url: Optional[str]


class Part(Entity[MaterialAssignment]):
    manufacturer: str
    weight: float
    weight_unit: str
    category: Optional[str]
    materials: Dict[Slug, MaterialAssignment]
    product_url: Optional[str]

    def assignments(self) -> Dict[Slug, MaterialAssignment]:
        return self.materials


class PartAssignment(Assignment):
    pass


class Device(Entity[PartAssignment]):
    parts: Dict[Slug, PartAssignment]
    manufacturer: str
    dependency: Optional[str]
    description: Optional[str]
    project_url: Optional[str]

    def assignments(self) -> Dict[Slug, PartAssignment]:
        return self.parts


class DeviceAssignment(Assignment):
    pass


class Portfolio(Entity[DeviceAssignment]):
    devices: Dict[Slug, DeviceAssignment]

    def assignments(self) -> Dict[Slug, DeviceAssignment]:
        return self.devices
