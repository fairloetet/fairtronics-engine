import os
import csv
import re
from typing import Collection, List, Dict, Optional, Tuple
from slugify import slugify
import pandas as pd
from fairtronics_engine.model import Device, Assignment, Material, Location, Indicator, Manufacturer, PartCategory, \
    MaterialCategory, MaterialAssignment, LocationAssignment, Part, PartAssignment, Portfolio, DeviceAssignment, \
    Entity, Resource, ResourceAssignment, IndicatorAssignment, Slug, Tier, Chain
from fairtronics_engine.statics import INVERSE, NAME, SHORT, AMOUNT, STRAIGHT, UNKNOWN, VERBOSE
from fairtronics_engine.view import View

# Index of Axes of the cube's dimensions. Note, that the exact numbers need to be retained
# PORTFOLIO of a kind of store tier
PORTFOLIO = Tier(0, "Portfolio", "Portfolios", "f", "items")  # must not be renumbered, axes begin with 0
# DEVICE of a kind of device manufacturer tier
DEVICE = Tier(1, "Device", "Devices", "d", "items")
# PARTS of a kind of part manufacturer tier
PART = Tier(2, "Part", "Parts", "p", "items")
# MATERIALS of a kind of smelter/refiner/commodity/supplier tier
MATERIAL = Tier(3, "Material", "Materials", "m", "g")
# RESOURCES is kind of mine tier
RESOURCE = Tier(4, "Resource", "Resources", "r", "g")
# COUNTRIES actually doesn't really make sense.
LOCATION = Tier(5, "Location", "Locations", "c", "g")
# INDICATORS actually doesn't really make sense.
INDICATOR = Tier(6, "Indicator", "Indicators", "i", "g")

CHAIN = Chain(PORTFOLIO, DEVICE, PART, MATERIAL, RESOURCE, LOCATION, INDICATOR)


class Database:
    name: str
    portfolios: Dict[Slug, Portfolio]
    devices: Dict[Slug, Device]
    parts: Dict[Slug, Part]
    manufacturers: Dict[Slug, Manufacturer]
    materials: Dict[Slug, Material]
    resources: Dict[Slug, Resource]
    locations: Dict[Slug, Location]
    indicators: Dict[Slug, Indicator]
    part_categories: Dict[Slug, PartCategory]
    material_categories: Dict[Slug, MaterialCategory]
    unknown_origin: Location
    secondary_origin: Location
    unknown_material: Material
    unknown_resource: Resource
    unknown_manufacturer: Manufacturer

    def __init__(self, data_folder: Optional[str] = None):
        self.portfolios = dict()
        self.devices = dict()
        self.parts = dict()
        self.manufacturers = dict()
        self.part_categories = dict()
        self.material_categories = dict()
        self.materials = dict()
        self.resources = dict()
        self.locations = dict()
        self.indicators = dict()
        if data_folder:
            self.load(data_folder)
            if VERBOSE:
                print(f"Read database {data_folder} with {len(self.portfolios)} portfolios, {len(self.devices)} "
                      f"devices, {len(self.parts)} parts, {len(self.part_categories)} part categories, "
                      f"{len(self.manufacturers)} manufacturers, {len(self.materials)} materials, "
                      f"{len(self.material_categories)} material categories, {len(self.resources)} resources, "
                      f"{len(self.locations)} locations, {len(self.indicators)} indicators")
            else:
                print("Read database %s" % data_folder)

    def load(self, data_folder: str):

        def data_file(data_name: str):
            file_path = data_folder + os.path.sep + data_name + ".csv"
            if os.path.isfile(file_path):
                return open(file_path, mode="r", encoding="utf-8-sig", newline='')
            raise IOError("File for " + data_name + " not accessible at " + file_path)

        self.name = data_folder
        # First, load the base data entities with empty assignments to each other. Note that the order is important!
        # TODO Could read in only data of the root object and its siblings
        with data_file("locations") as file:
            csv_reader = csv.DictReader(file)
            self.locations = {Slug(row['slug']): Location(slug=Slug(row['slug']), name=row['name'],
                                                          short_name=row['short_name'],
                                                          code=row['code'], region=row['region'],
                                                          sub_region=row['sub_region'],
                                                          indicators=dict())
                              for row in csv_reader}
        self.unknown_origin = Location(slug=Slug('XX'), name='- Unknown Origin -', indicators=dict(),
                                       short_name='Unknown', code='XX', region='XX', sub_region='XX')
        self.locations[self.unknown_origin.slug] = self.unknown_origin
        self.secondary_origin = Location(slug=Slug('YY'), name='- Secondary Origin -', indicators=dict(),
                                         short_name='Secondary origin', code='YY',
                                         region='YY', sub_region='YY')
        self.locations[self.secondary_origin.slug] = self.secondary_origin
        with data_file("resources") as file:
            csv_reader = csv.DictReader(file)
            self.resources = {Slug(row['slug']): Resource(slug=Slug(row['slug']), name=row['name'],
                                                          short_name=row['short_name'], circularity=row['circularity'],
                                                          locations=dict())
                              for row in csv_reader}
            if 'unknown-unknown' in self.resources.keys():
                self.unknown_resource = self.resources[Slug('unknown-unknown')]
            else:
                self.unknown_resource = Resource(slug=Slug('unknown-unknown'), name="- Unknown Resource -",
                                                 short_name='Unknown', locations={}, circularity="virgin")
            self.unknown_resource.locations[self.unknown_origin.slug] = LocationAssignment(
                entity_slug=self.unknown_resource.slug,
                assigned_slug=self.unknown_origin.slug, amount=1.0,
                source="Unknown resource attributed fully to unknown origin")
        with data_file("materials") as file:
            csv_reader = csv.DictReader(file)
            self.materials = {Slug(row['slug']): Material(slug=Slug(row["slug"]), name=row["name"],
                                                          short_name=row['short_name'], resources=dict(),
                                                          cas=row['cas'], category=row['category'],
                                                          density_g_cm3=row['density_g_cm3'])
                              for row in csv_reader}
            if 'unknown-unknown' in self.materials.keys():
                self.unknown_material = self.materials[Slug('unknown-unknown')]
            else:
                self.unknown_material = Material(slug=Slug('unknown-unknwon'), name="- Unknown Material -",
                                                 category="", cas="", density_g_cm3="",
                                                 short_name='Unknown', resources=dict())
            self.unknown_material.resources[self.unknown_resource.slug] = ResourceAssignment(
                entity_slug=self.unknown_material.slug, assigned_slug=self.unknown_resource.slug, amount=1.0,
                source="Unknown material attributed 1:1 to unknown resource")
        with data_file("material_categories") as file:
            csv_reader = csv.DictReader(file)
            self.material_categories = dict((row['slug'], MaterialCategory(slug=Slug(row['slug']), name=row['name'],
                                                                           short_name=row['short_name'],
                                                                           parent=row['parent'])) for row in csv_reader)
        with data_file("part_categories") as file:
            csv_reader = csv.DictReader(file)
            self.part_categories = dict((row['slug'], PartCategory(slug=Slug(row['slug']), name=row['name'],
                                                                   short_name=row['short_name'], parent=row['parent']))
                                        for row in csv_reader)
        with data_file("manufacturers") as file:
            csv_reader = csv.DictReader(file)
            self.manufacturers = {Slug(row['slug']): Manufacturer(slug=Slug(row['slug']), name=row['name'],
                                                                  short_name=row['short_name'],
                                                                  company_url=row['company_url']) for row in csv_reader}
            if 'unknown' in self.manufacturers.keys():
                self.unknown_manufacturer = self.manufacturers[Slug('unknown')]
            else:
                self.unknown_manufacturer = Manufacturer(slug=Slug('unknown'), name="- Unknown Manufacturer -",
                                                         short_name='Unknown', company_url="")

        with data_file("parts") as file:
            csv_reader = csv.DictReader(file)
            self.parts = {Slug(row['slug']): Part(slug=Slug(row["slug"]), name=row["name"],
                                                  short_name=row['short_name'], materials=dict(),
                                                  weight=0.0, weight_unit="g",
                                                  manufacturer=row["Manufacturer.slug"], category=row['category'],
                                                  product_url=row['product_url'])
                          for row in csv_reader}
        with data_file("devices") as file:
            csv_reader = csv.DictReader(file)
            self.devices = {Slug(row['slug']): Device(slug=Slug(row["slug"]), name=row["name"],
                                                      short_name=row['short_name'],
                                                      manufacturer=row["Manufacturer.slug"],
                                                      dependency=row["dependency"], description=row["description"],
                                                      project_url=row['project_url'], parts=dict())
                            for row in csv_reader}
        with data_file("portfolios") as file:
            csv_reader = csv.DictReader(file)
            self.portfolios = {Slug(row['slug']): Portfolio(slug=Slug(row['slug']), name=row['name'],
                                                            short_name=row['short_name'], devices=dict())
                               for row in csv_reader}

        # Second, read indicator data
        with data_file("indicators") as file:
            csv_reader = csv.DictReader(file)
            self.indicators = {Slug(row['slug']):
                               Indicator(slug=Slug(row['slug']), name=row['name'], short_name=row['short_name'],
                                         data_base=row['data_base'], direction=row['direction'],
                                         probability_based=(row['probability_based'] == "True"), usage=STRAIGHT.lower())
                               for row in csv_reader}

        # Third, go for the assignments between the entities
        # TODO Schöneren Python-Code machen! Evtl. if-Operator in Listiterator [] oder next()-Funktion nutzen
        with data_file("resource_shares") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                # TODO Das scheint mir etwas umständlich programmiert. Muss hier wirklich noch ne Schleife rein?
                # TODO Vor allem sollte hier einiges in die Datenkonvertierung, speziell das eval und substitutions!
                for resource in self.resources.values():
                    if row["Resource.slug"] == resource.slug:
                        location = self.locations[Slug(row["Location.slug"])]
                        amount = float(eval(row["amount"]))
                        source = str(row["source"])
                        if location.slug in resource.locations:
                            resource.locations[location.slug].amount += amount
                            resource.locations[location.slug].source += ", and " + source
                        else:
                            ia = LocationAssignment(entity_slug=resource.slug, assigned_slug=location.slug,
                                                    amount=amount, source=source)
                            resource.locations[location.slug] = ia
            # If we don't have location shares, let resource originate from Unknown or Secondary
            for resource in self.resources.values():
                if len(resource.locations) == 0:
                    # Locate secondary resources in special origin
                    if resource.circularity == "recycled":
                        collect_location = self.secondary_origin
                        source = "Recycled resources collected in special 'location'"
                    else:
                        # Mark resources with unknown distribution with an ?
                        # resource.name = "? " + resource.name
                        collect_location = self.unknown_origin
                        source = "Unknown resource origins collected in special 'location'"
                    ia = LocationAssignment(entity_slug=resource.slug, assigned_slug=collect_location.slug,
                                            amount=1.0, source=source)
                    resource.locations[collect_location.slug] = ia
        with data_file("material_resources") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                material_slug = Slug(row["Material.slug"])
                resource_slug = Slug(row["Resource.slug"])
                material = self.materials[material_slug]
                try:
                    resource = self.resources[resource_slug]
                except KeyError:
                    raise KeyError("Resource '" + row["Resource.slug"] +
                                   "' for material '" + row["Material.slug"] + "' unknown")
                amount = float(row["amount"])
                source = str(row["source"])
                if resource.slug in material.resources:
                    material.resources[resource.slug].amount += amount
                    material.resources[resource.slug].source += ", and " + source
                else:
                    ra = ResourceAssignment(entity_slug=material.slug, assigned_slug=resource.slug,
                                            amount=amount, source=source)
                    material.resources[resource.slug] = ra
            # Collect materials with unknown production to unknown resource pool
            for material in self.materials.values():
                if len(material.resources) == 0:
                    # material.name = "? " + material.name
                    material.resources[self.unknown_resource.slug] = ResourceAssignment(
                        entity_slug=material.slug, assigned_slug=self.unknown_resource.slug, amount=1.0,
                        source="Unknown material composition attributed 1:1 to unknown resource")
        with data_file("part_materials") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                part_slug = Slug(row["Part.slug"])
                material_slug = Slug(row["Material.slug"])
                part = self.parts[part_slug]
                material = self.materials[material_slug]
                amount = float(row["amount"])  # This is defined in gramm
                source = str(row["source"])
                part.weight += amount
                if material.slug in part.materials:
                    part.materials[material.slug].amount += amount
                    part.materials[material.slug].source += ", and " + source
                else:
                    ma = MaterialAssignment(entity_slug=part.slug, assigned_slug=material.slug,
                                            amount=amount, source=source)
                    part.materials[material.slug] = ma
        with data_file("device_parts") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                device_slug = Slug(row["Device.slug"])
                part_slug = Slug(row["Part.slug"])
                try:
                    device = self.devices[device_slug]
                except KeyError:
                    raise KeyError("Device '" + row["Device.slug"] + "' unknown within part associations")
                part = self.parts[part_slug]
                amount = float(row["amount"])
                source = str(row["source"])
                if part.slug in device.parts:
                    device.parts[part.slug].amount += amount
                    device.parts[part.slug].source += ", and " + source
                else:
                    pa = PartAssignment(entity_slug=device.slug, assigned_slug=part.slug, amount=amount, source=source)
                    device.parts[part.slug] = pa
        with data_file("portfolio_devices") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                portfolio_slug = Slug(row["Portfolio.slug"])
                device_slug = Slug(row["Device.slug"])
                portfolio = self.portfolios[portfolio_slug]
                device = self.devices[device_slug]
                amount = float(row["amount"])
                source = str(row["source"])
                if device.slug in portfolio.devices:
                    portfolio.devices[device.slug].amount += amount
                    portfolio.devices[device.slug].source += ", and " + source
                else:
                    pa = DeviceAssignment(entity_slug=portfolio.slug, assigned_slug=device.slug,
                                          amount=amount, source=source)
                    portfolio.devices[device.slug] = pa
        with data_file("indicator_data") as file:
            csv_reader = csv.DictReader(file)
            for row in csv_reader:
                location_slug = Slug(row["Location.slug"])
                indicator_slug = Slug(row["Indicator.slug"])
                if row["Location.slug"] not in self.locations:
                    continue
                location = self.locations[location_slug]
                straight_indicator = self.indicators[indicator_slug]
                amount = float(row["amount"])
                source = str(row["source"])
                assert straight_indicator.slug not in location.indicators, \
                    f"There's more than one value for indicator {straight_indicator.name} in location {location.name}."
                location.indicators[straight_indicator.slug] = IndicatorAssignment(
                    entity_slug=location.slug, assigned_slug=straight_indicator.slug, amount=amount, source=source)
            # Create inverse and unknown variant as well
            new_derived_indicators: Dict[Slug, Indicator] = dict()
            for straight_indicator in self.indicators.values():
                inverse_indicator = Indicator(slug=Slug(straight_indicator.slug + "-inverse"), usage=INVERSE.lower(),
                                              name=straight_indicator.name + " (Inverse)",
                                              short_name=straight_indicator.short_name + " (Inverse)",
                                              direction=straight_indicator.direction,
                                              probability_based=straight_indicator.probability_based)
                new_derived_indicators[inverse_indicator.slug] = inverse_indicator
                unknown_indicator = Indicator(slug=Slug(straight_indicator.slug + "-unknown"), usage=UNKNOWN.lower(),
                                              name=straight_indicator.name + " (Unknown)",
                                              short_name=straight_indicator.short_name + " (Unknown)",
                                              direction=straight_indicator.direction,
                                              probability_based=straight_indicator.probability_based)
                new_derived_indicators[unknown_indicator.slug] = unknown_indicator
                for location_slug in self.locations:
                    location = self.locations[location_slug]
                    if straight_indicator.slug in location.indicators:
                        ia = location.indicators[straight_indicator.slug]
                        inverse_amount = 1.0 - ia.amount
                        inverse_source = "Inverse value from " + ia.source
                        self.locations[location.slug].indicators[inverse_indicator.slug] = IndicatorAssignment(
                            entity_slug=location.slug, assigned_slug=inverse_indicator.slug, amount=inverse_amount,
                            source=inverse_source)
                    else:
                        unknown_amount = 1.0
                        unknown_source = "Actually a value is unknown"
                        self.locations[location.slug].indicators[unknown_indicator.slug] = IndicatorAssignment(
                            entity_slug=location.slug, assigned_slug=unknown_indicator.slug, amount=unknown_amount,
                            source=unknown_source)
            self.indicators |= new_derived_indicators

    # ---------------------

    def entity_dict(self, tier: Tier) -> Dict[Slug, Entity[Assignment]]:
        return {PORTFOLIO: self.portfolios,
                DEVICE: self.devices,
                PART: self.parts,
                MATERIAL: self.materials,
                RESOURCE: self.resources,
                LOCATION: self.locations,
                INDICATOR: self.indicators}[tier]

    def get_entities(self, tier: Tier, slugs: Collection[Slug] = []) -> Collection[Entity[Assignment]]:
        if slugs:
            return [self.entity_dict(tier)[slug] for slug in slugs]
        else:
            return self.entity_dict(tier).values()

    def get_entity(self, tier: Tier, slug: Slug) -> Entity[Assignment]:
        return self.entity_dict(tier)[slug]

    def get_entity_slugs(self, tier: Tier) -> Collection[Slug]:
        return self.entity_dict(tier).keys()

    def get_assigments(self, tier: Tier, slug: Slug) -> Dict[Slug, Assignment]:
        return self.get_entity(tier, slug).assignments()

    def get_assignee_slugs(self, tier: Tier, slug: Slug) -> Collection[Slug]:
        return self.get_entity(tier, slug).assignments().keys()

    def get_assignment(self, tier: Tier, slug: Slug, assigned_slug: Slug) -> Assignment:
        return self.get_entity(tier, slug).assignments()[assigned_slug]

    def has_assignment(self, tier: Tier, slug: Slug, assigned_slug: Slug) -> bool:
        return assigned_slug in self.get_entity(tier, slug).assignments()

    # TODO Change second parameter to plural or spend extra method
    def get_names_of_tiers(self, tiers: List[Tier], singular: bool = False) -> List[str]:
        return [self.get_name_of_tier(tier, singular) for tier in tiers]

    def get_name_of_tier(self, tier: Tier, singular: bool = False) -> str:
        return tier.singular() if singular else tier.plural()

    # ---------------------

    def list(self, entity_tier: Tier | int, entity_slug: Optional[str] = None, assoc_tier: Optional[Tier | int] = None) -> View:
        if entity_slug:
            entity_slug = Slug(entity_slug)
        if isinstance(entity_tier, int):
            entity_tier = CHAIN.get_tier(entity_tier)
        if assoc_tier and isinstance(assoc_tier, int):
            assoc_tier = CHAIN.get_tier(assoc_tier)
        if entity_tier == PORTFOLIO:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.portfolios[key].name, self.portfolios[key].short_name)
                         for key in self.portfolios],
                        index=self.portfolios.keys(), columns=[NAME, SHORT])
            else:
                entity = self.portfolios[entity_slug]
                if not assoc_tier or assoc_tier == DEVICE:
                    df = pd.DataFrame.from_records(
                            [(self.devices[key].name, self.devices[key].short_name, entity.devices[key].amount)
                             for key in entity.devices],
                            index=entity.devices.keys(), columns=[NAME, SHORT, AMOUNT])
                else:
                    raise KeyError("No association for portfolios other than devices listable.")
        elif entity_tier == DEVICE:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.devices[key].name, self.devices[key].short_name, self.devices[key].manufacturer,
                          self.devices[key].dependency, self.devices[key].description)
                         for key in self.devices],
                        index=self.devices.keys(), columns=[NAME, SHORT, 'manufacturer', 'dependency', 'description'])
                df['manufacturer'] = df['manufacturer'].apply(lambda man: (self.manufacturers[man].name
                                                                           if man in self.manufacturers else ""))
                df['dependency'] = df['dependency'].apply(lambda dev: (self.devices[dev].short_name
                                                                       if dev in self.devices else ""))
            else:
                entity = self.devices[entity_slug]
                if not assoc_tier or assoc_tier == PART:
                    df = pd.DataFrame.from_records(
                            [(self.parts[key].name, self.parts[key].short_name, entity.parts[key].amount)
                             for key in entity.parts],
                            index=entity.parts.keys(), columns=[NAME, SHORT, AMOUNT])
                else:
                    raise KeyError("No association for devices other than parts listable.")
        elif entity_tier == PART:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.parts[key].name, self.parts[key].short_name, self.parts[key].manufacturer, 
                          self.parts[key].category)
                         for key in self.parts],
                        index=self.parts.keys(), columns=[NAME, SHORT, 'manufacturer', 'category'])
                df['manufacturer'] = df['manufacturer'].apply(lambda man: (self.manufacturers[man].name
                                                                           if man in self.manufacturers else ""))
                df['category'] = df['category'].apply(lambda cat: (self.part_categories[cat].name
                                                                   if cat in self.part_categories else ""))
            else:
                entity = self.parts[entity_slug]
                if not assoc_tier or assoc_tier == MATERIAL:
                    df = pd.DataFrame.from_records(
                            [(self.materials[key].name, self.materials[key].short_name, entity.materials[key].amount)
                             for key in entity.materials],
                            index=entity.materials.keys(), columns=[NAME, SHORT, AMOUNT])
                else:
                    raise KeyError("No association for parts other than materials listable.")
        elif entity_tier == MATERIAL:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.materials[key].name, self.materials[key].short_name, self.materials[key].category)
                         for key in self.materials],
                        index=self.materials.keys(), columns=[NAME, SHORT, 'category'])
                df['category'] = df['category'].apply(lambda cat: (self.material_categories[cat].name
                                                                   if cat in self.material_categories else ""))
            else:
                entity = self.materials[entity_slug]
                if not assoc_tier or assoc_tier == RESOURCE:
                    df = pd.DataFrame.from_records(
                            [(self.resources[key].name, self.resources[key].short_name, entity.resources[key].amount)
                             for key in entity.resources],
                            index=entity.resources.keys(), columns=[NAME, SHORT, AMOUNT])
                else:
                    raise KeyError("No association for materials other than resources listable.")
        elif entity_tier == RESOURCE:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.resources[key].name, self.resources[key].short_name, self.resources[key].circularity)
                         for key in self.resources],
                        index=self.resources.keys(), columns=[NAME, SHORT, 'circularity'])
            else:
                entity = self.resources[entity_slug]
                if not assoc_tier or assoc_tier == LOCATION:
                    df = pd.DataFrame.from_records(
                            [(self.locations[key].name, self.locations[key].short_name, entity.locations[key].amount)
                             for key in entity.locations],
                            index=entity.locations.keys(), columns=[NAME, SHORT, AMOUNT])
                else:
                    raise KeyError("No association for resources other than locations listable.")
        elif entity_tier == LOCATION:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.locations[key].name, self.locations[key].short_name)
                         for key in self.locations],
                        index=self.locations.keys(), columns=[NAME, SHORT])
            else:
                entity = self.locations[entity_slug]
                if not assoc_tier or assoc_tier == INDICATOR:
                    df = pd.DataFrame.from_records(
                            [(self.indicators[key].name, self.indicators[key].short_name, entity.indicators[key].amount)
                             for key in entity.indicators],
                            index=entity.indicators.keys(), columns=[NAME, SHORT, AMOUNT])
                else:
                    raise KeyError("No association for locations other than indicators listable.")
        elif entity_tier == INDICATOR:
            if not entity_slug:
                df = pd.DataFrame.from_records(
                        [(self.indicators[key].name, self.indicators[key].short_name)
                         for key in self.indicators],
                        index=self.indicators.keys(), columns=[NAME, SHORT])
            else:
                raise KeyError("No association for indicators listable.")
        else:
            assert False, "Didn't expect an entity type of id " + str(entity_tier)
        if 'amount' in df:
            rank = df['amount'].rank(method='max', ascending=False).astype(int)
            df.insert(0, "rank", rank)
            df.sort_values(by=['amount'], ascending=False, inplace=True)
        if assoc_tier and entity_slug:
            entity_name = self.get_name_of_tier(entity_tier, True)
            assocs_name = self.get_name_of_tier(assoc_tier, False)
            title_type = assocs_name
            title_fragments = ["Amounts of", entity_name, "'" + entity_slug + "'"]
        else:
            entities_name = self.get_name_of_tier(entity_tier, False)
            title_type = "database " + self.name
            title_fragments = ["all", entities_name]
        return View(df, 0.0, len(df.index), title_type, title_fragments)

    def info(self, entity_type: Tier, entity_slug: str) -> View:
        entity_slug = Slug(entity_slug)
        if entity_type == PORTFOLIO:
            df = pd.DataFrame({"name": self.portfolios[entity_slug].name,
                               "devices": ', '.join([self.devices[x].name
                                                     for x in self.portfolios[entity_slug].devices.keys()])},
                              index=[entity_slug])
        elif entity_type == DEVICE:
            df = pd.DataFrame({"name": self.devices[entity_slug].name,
                               "manufacturer": self.devices[entity_slug].manufacturer,
                               "description": self.devices[entity_slug].description},
                              index=[entity_slug])
        elif entity_type == PART:
            df = pd.DataFrame({"name": self.parts[entity_slug].name,
                               "weight": self.parts[entity_slug].weight,
                               "unit": self.parts[entity_slug].weight_unit,
                               "manufacturer": self.parts[entity_slug].manufacturer,
                               "category": self.parts[entity_slug].category},
                              index=[entity_slug])
        elif entity_type == MATERIAL:
            df = pd.DataFrame({"name": self.materials[entity_slug].name,
                               "cas":  self.materials[entity_slug].cas,
                               "density (g/cm3)":  self.materials[entity_slug].density_g_cm3,
                               "category": self.materials[entity_slug].category,
                               "resources": ', '.join([self.resources[x].name
                                                       for x in self.materials[entity_slug].resources.keys()])},
                              index=[entity_slug])
        elif entity_type == RESOURCE:
            df = pd.DataFrame({"name": self.resources[entity_slug].name,
                               "circularity": self.resources[entity_slug].circularity},
                              index=[entity_slug])
        elif entity_type == LOCATION:
            df = pd.DataFrame({"name": self.locations[entity_slug].name,
                               "code": self.locations[entity_slug].code},
                              index=[entity_slug])
        elif entity_type == INDICATOR:
            df = pd.DataFrame({"name": self.locations[entity_slug].name},
                              index=[entity_slug])
        else:
            assert False, "Didn't expect an entity type of id " + str(entity_type)
        entity_name = self.get_name_of_tier(entity_type, False)
        return View(df, 0.0, len(df.index), entity_name, ["'" + entity_slug + "'"])

    def search(self, entity_tier: Tier, attribute: str, value_spec: str) -> View:
        value_pattern = re.compile(r"^" + value_spec + "$")
        if entity_tier == PORTFOLIO:
            slug_list = {
                "name": [key for key, value in self.portfolios.items() if re.match(value_pattern, value.name)]
            }[attribute]
        elif entity_tier == DEVICE:
            slug_list = {
                "name": [key for key, value in self.devices.items() if re.match(value_pattern, value.name)],
                "short_name": [key for key, value in self.devices.items() if re.match(value_pattern, value.short_name)],
                "manufacturer": [key for key, value in self.devices.items()
                                 if re.match(value_pattern, value.manufacturer)],
                "dependency": [key for key, value in self.devices.items() 
                               if not value.dependency or re.match(value_pattern, value.dependency)],
                "description": [key for key, value in self.devices.items()
                                if not value.description or re.match(value_pattern, value.description)]
            }[attribute]
        elif entity_tier == PART:
            slug_list = {
                "name": [key for key, value in self.parts.items() if re.match(value_pattern, value.name)],
                "short_name": [key for key, value in self.parts.items() if re.match(value_pattern, value.short_name)],
                "manufacturer": [key for key, value in self.parts.items()
                                 if re.match(value_pattern, value.manufacturer)],
                "weight": [key for key, value in self.parts.items() if re.match(value_pattern, str(value.weight))],
                "weight_unit": [key for key, value in self.parts.items() if re.match(value_pattern, value.weight_unit)],
                "category": [key for key, value in self.parts.items() if not value.category or re.match(value_pattern, value.category)],
                "materials": [key for key, value in self.parts.items()
                              if re.match(value_pattern, str(value.materials.keys()))],
            }[attribute]
        elif entity_tier == MATERIAL:
            slug_list = {
                "name": [key for key, value in self.materials.items() if re.match(value_pattern, value.name)],
                "short_name": [key for key, value in self.materials.items()
                               if re.match(value_pattern, value.short_name)],
                "cas": [key for key, value in self.materials.items() if not value.cas or re.match(value_pattern, value.cas)],
                "category": [key for key, value in self.materials.items()
                             if not value.category or re.match(value_pattern, value.category)],
                "density_g_cm3": [key for key, value in self.materials.items()
                                  if not value.density_g_cm3 or re.match(value_pattern, value.density_g_cm3)],
                "resources": [key for key, value in self.materials.items()
                              if re.match(value_pattern, str(value.resources.keys()))],
            }[attribute]
        elif entity_tier == RESOURCE:
            slug_list = {
                "name": [key for key, value in self.resources.items() if re.match(value_pattern, value.name)],
                "short_name": [key for key, value in self.resources.items()
                               if re.match(value_pattern, value.short_name)],
                "circularity": [key for key, value in self.resources.items()
                                if re.match(value_pattern, str(value.circularity))],
                "locations": [key for key, value in self.resources.items()
                              if re.match(value_pattern, str(value.locations.keys()))],
            }[attribute]
        elif entity_tier == LOCATION:
            slug_list = {
                "name": [key for key, value in self.locations.items() if re.match(value_pattern, value.name)],
                "short_name": [key for key, value in self.locations.items()
                               if re.match(value_pattern, value.short_name)],
                "code": [key for key, value in self.locations.items() 
                         if not value.code or re.match(value_pattern, value.code)],
                "region": [key for key, value in self.locations.items() 
                           if not value.region or re.match(value_pattern, value.region)],
                "sub_region": [key for key, value in self.locations.items()
                               if not value.sub_region or re.match(value_pattern, value.sub_region)],
                "indicators": [key for key, value in self.locations.items()
                               if re.match(value_pattern, str(value.indicators.keys()))],
            }[attribute]
        elif entity_tier == INDICATOR:
            slug_list = {
                "name": [key for key, value in self.indicators.items() if re.match(value_pattern, value.name)],
                "short_name": [key for key, value in self.indicators.items()
                               if re.match(value_pattern, value.short_name)],
                "direction": [key for key, value in self.indicators.items()
                              if re.match(value_pattern, str(value.direction))],
                "usage": [key for key, value in self.indicators.items()
                          if re.match(value_pattern, str(value.usage))],
            }[attribute]
        else:
            assert False, "Didn't expect an entity type of id " + str(entity_tier)
        entity_name = self.get_name_of_tier(entity_tier, False)
        if len(slug_list) == 0:
            df = pd.DataFrame()
        else:
            df = pd.concat([self.info(entity_tier, slug).df for slug in slug_list])
        return View(df, 0.0, len(df.index), entity_name, ["Entities with", attribute, "matching", value_spec])

    def model(self, entity_tier: Tier) -> View:
        if entity_tier == PORTFOLIO:
            description = "A number of devices of various kind"
            attributes = {
                "name": "Name of the portfolio"
            }
        elif entity_tier == DEVICE:
            description = "Consisting of many parts"
            attributes = {
                "name": "Name of the device",
                "manufacturer": "Manufacturer of the device (as key/slug)",
                "dependency": "Other device this device is part of",
                "description": "Description of device"
            }
        elif entity_tier == PART:
            description = "A number of devices of various kind"
            attributes = {
                "name": "Name of the part",
                "manufacturer": "Manufacturer of the part (as key/slug)",
                "weight": "Weight of the part",
                "weight_unit": "Unit of the weight (mg, g, kg)",
                "category": "Category of the part (as key/slug)",
                "materials": "List of materials in the part"
            }
        elif entity_tier == MATERIAL:
            description = "A material in a part, composed of resources"
            attributes = {
                "name": "Name of the material",
                "cas": "CAS number of the material, in known, maybe more than one",
                "category": "Category of the material (as key/slug)",
                "density_g_cm3": "Density of the material in g/cm3, maybe empty = unknown",
                "resources": "List of resources / raw material this material is made of"
            }
        elif entity_tier == RESOURCE:
            description = "A resource (or raw material) taken from earth, sea, or air"
            attributes = {
                "name": "Name of the resource",
                "circularity": "'virgin' if primary resource, 'recycled' is secondary",
                "locations": "List of locations this resource where is extracted"
            }
        elif entity_tier == LOCATION:
            description = "A location where resources are extracted by work"
            attributes = {
                "name": "Name of location",
                "code": "Two letter code of the location",
                "region": "Region in world this location is part of",
                "sub_region": "Local area in world this location is part of"
            }
        elif entity_tier == INDICATOR:
            description = "An indication for social judgement of a location"
            attributes = {
                "name": "name of indicator",
                "direction": "Description of indicator",
                "usage": "Whether it's a direct (or straight), the inverse of a direct or an indicator"
                         "collecting unknown values",
            }
        else:
            assert False, "Didn't expect an entity type of id " + str(entity_tier)
        df = pd.DataFrame({"description": attributes.values()}, index=list(attributes.keys()))
        entity_name = self.get_name_of_tier(entity_tier, True)
        return View(df, 0.0, len(df.index), "", [entity_name, "is", description])

    # --------------------------------

    def add_device(self, name: str, manufacturer: Optional[str] = None, parts: Optional[Dict[str, float]] = None,
                   short_name: Optional[str] = None, slug: Optional[str] = None,
                   dependency: Optional[str] = None, description: Optional[str] = None,
                   project_url: Optional[str] = None) -> Device:
        if manufacturer:
            if manufacturer not in self.manufacturers:
                raise KeyError(f"{manufacturer} isn't a slug of any known manufacturer.")
        else:
            manufacturer = self.unknown_manufacturer.slug
        if not parts:
            parts = {}
        if dependency and dependency not in self.devices:
            raise KeyError(f"{dependency} isn't a slug of any known device.")
        short, device_slug = _get_strings_clear(name, short_name, slug, manufacturer)
        if device_slug in self.devices:
            _, device_slug = _get_strings_clear(name, None, slug, manufacturer)
        if device_slug in self.devices:
            raise KeyError(f"A device with slug {slug} already exists.")
        new_part_assignments = {}
        for part in parts:
            if part not in self.parts:
                raise KeyError(f"{part} isn't a slug for any known part.")
            amount = parts[part]
            new_part_assignments[part] = PartAssignment(entity_slug=device_slug, assigned_slug=part,
                                                        amount=amount, source="")
        new_device = Device(slug=device_slug, name=name, short_name=short, parts=new_part_assignments,
                            manufacturer=manufacturer, dependency=dependency,
                            description=description, project_url=project_url)
        self.devices[device_slug] = new_device
        return new_device

    def add_part_to_device(self, device_slug: str, part_slug: str, amount: float) -> Part:
        if device_slug not in self.devices:
            raise KeyError(f"{device_slug} isn't a slug of any known device.")
        device = self.devices[device_slug]
        if part_slug not in self.parts:
            raise KeyError(f"{part_slug} isn't a slug of any known part.")
        part = self.parts[part_slug]
        if part.slug in device.parts:
            device.parts[part.slug].amount += amount
        else:
            device.parts[part.slug] = PartAssignment(entity_slug=device_slug, assigned_slug=part_slug,
                                                     amount=amount, source="")
        return part

    def delete_device(self, slug: str):
        try:
            self.devices.pop(Slug(slug))
            for portfolio in self.portfolios.values():
                for device_slug in list(portfolio.devices):
                    if device_slug == slug:
                        del portfolio.devices[device_slug]
        except KeyError as ex:
            raise KeyError(f"A device with slug {slug} doesn't exist.") from ex

    def add_manufacturer(self, name: str, short_name: Optional[str] = None, slug: Optional[str] = None,
                         company_url: Optional[str] = "") -> Manufacturer:
        short_name, manu_slug = _get_strings_clear(name, short_name, slug)
        if manu_slug in self.manufacturers:
            raise KeyError(f"A manufacturer with slug {slug} already exists.")
        new_manu = Manufacturer(slug=manu_slug, name=name, short_name=short_name, company_url=company_url)
        self.manufacturers[manu_slug] = new_manu
        return new_manu


def _get_strings_clear(name: str, short_name: Optional[str] = None, given_slug: Optional[str] = None,
                       slug_prefix: Optional[str] = None) -> Tuple[str, Slug]:
    if not name:
        raise ValueError("Name may not be empty.")
    if not short_name:
        short_name = name.translate(str.maketrans(dict.fromkeys('aeiou'))) if len(name) > 25 else name
    if not given_slug:
        slug = slugify(f"{slug_prefix}-{short_name}") if slug_prefix else slugify(short_name)
    elif given_slug != slugify(given_slug):
        raise ValueError(f"Given slug {given_slug} doesn't seem to be slug-y. Use {slugify(given_slug)} instead.")
    else:
        slug = given_slug
    return short_name, Slug(slug)
