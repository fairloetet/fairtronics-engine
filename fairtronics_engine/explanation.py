from string import Template
from decimal import Decimal
from typing import List, Dict, Set
from fairtronics_engine.database import DEVICE, Database, Tier, Slug, AMOUNT
from fairtronics_engine.analysis import Analysis
from fairtronics_engine.statics import UNIT

EXPLAIN_METHOD_STAGED = "staged"
EXPLAIN_METHOD_ORDERED = "ordered"


explanation_templates_ordered: Dict[str, Template] = {
    "": Template("$object_shortname only (TODO)"),
    "p": Template("$part_amount {$part_shortname}(s) in $object_shortname (TODO)"),
    "pm": Template("(TODO)"),
    "pmr": Template("(TODO)"),
    "pmrc": Template("(TODO)"),
    "pmrci": Template("(TODO)"),
    "m": Template("(TODO)"),
    "mr": Template("(TODO)"),
    "mrc": Template("Aus $location_shortname kommen $resource_amount $resource_unit $resource_shortname, "
                    "welches sich vor allem aus dem Anteil von $material_shortname ($material_amount$material_unit), "
                    "dem für's $material_shortname notwendigen $resource_shortname "
                    "($material_resource_amount$material_resource_unit pro $material_unit $material_shortname) "
                    "und dem Weltmarktanteil $location_shortname an $resource_shortname ($resource_location_share% "
                    "[$resource_location_source]) ergibt."),
    "mrci": Template("(TODO)"),
    "r": Template("(TODO)"),
    "rc": Template("Aus $location_shortname kommen $location_amount$location_unit $resource_shortname, "
                   "weil $object_shortname $resource_amount$resource_unit $resource_shortname enthält und "
                   "${location_shortname}s Weltmarktanteil an $resource_shortname $resource_location_share% "
                   "[$resource_location_source] ist."),
    "rci": Template("(TODO)"),
    "c": Template("(TODO)"),
    "ci": Template("(TODO)"),
    "i": Template("(TODO)"),
    "pr": Template("(TODO)"),
    "prc": Template("$part_shortname entsteht aus u.a. $resource_amount$resource_unit $resource_shortname, "
                    "wovon $location_amount$location_unit aus $location_shortname stammen aufgrund des "
                    "$resource_shortname-Weltmarktanteils von $resource_location_share% [$resource_location_source]"),
    "prci": Template("(TODO)"),
    "pc": Template("(TODO)"),
    "pci": Template("Von der $indicator_shortname in $location_shortname stecken $location_amount$location_unit "
                    "im $part_shortname, weil in $location_shortname ein Risiko von $location_indicator_share% "
                    "$indicator_shortname [$location_indicator_source] herrscht."),
    "pi": Template("(TODO)"),
    "pmc": Template("(TODO)"),
    "pmci": Template("(TODO)"),
    "mc": Template("(TODO)"),
    "mci": Template("(TODO)"),
    "pmri": Template("(TODO)"),
    "mri": Template("(TODO)"),
    "mi": Template("(TODO)"),
    "pmi": Template("(TODO)"),
    "pri": Template("(TODO)"),
    "ri": Template("(TODO)")
}

explanation_templates_segmented: Dict[str, Template] = {
    "dp": Template("$object_shortname has $part_amount $part_shortname."),
    "dm": Template("$object_shortname contains $material_amount$material_unit $material_shortname."),
    "dr": Template("$object_shortname contains $resource_amount$resource_unit $resource_shortname."),
    "dc": Template("$location_amount$location_unit of the raw materials in $object_shortname come from "
                   "$location_shortname."),
    "di": Template("$indicator_amount$indicator_unit are in risk of $indicator_shortname within $object_shortname."),
    "pm": Template("$part_shortname contains $material_amount$material_unit $material_shortname."),
    "pr": Template("$part_shortname is produced from $resource_amount$resource_unit $resource_shortname, "
                   "amoung others."),
    "pc": Template("$location_amount$location_unit of the raw materials, $part_shortname is made of, come from "
                   "$location_shortname."),
    "pi": Template("$indicator_amount$indicator_unit of the raw materials of $part_shortname have a risk of "
                   "$indicator_shortname."),
    "mp": Template("$part_shortname contains $part_amount$material_unit (i.e. $part_material_share%) "
                   "$material_shortname."),
    "mr": Template("$material_shortname is produced (possibly amoung others) from $resource_amount$resource_unit $resource_shortname."),
    "mc": Template("$location_amount$location_unit of the raw materials, $material_shortname is made of, come from "
                   "$location_shortname."),
    "mi": Template("$indicator_amount$indicator_unit of the raw materials of $material_shortname have a risk of "
                   "$indicator_shortname."),
    "rp": Template("$part_shortname is produced from $part_amount$resource_unit $resource_shortname, amoung others."),
    "rm": Template("$material_shortname is produced (possibly amoung others) from $material_amount$material_unit $resource_shortname."),
    "rc": Template("$location_amount$location_unit $resource_shortname come from $location_shortname, "
                   "because the world market share of $resource_shortname for location_shortname is "
                   "$resource_location_share%."),
    "ri": Template("$indicator_amount$indicator_unit of $resource_shortname are extracted in risk of "
                   "$indicator_shortname."),
    "cp": Template("$part_amount$location_unit of the raw materials, $part_shortname is made of, come from "
                   "$location_shortname."),
    "cm": Template("$material_amount$material_unit of the raw materials, $material_shortname is made of, "
                   "come from $location_shortname."),
    "cr": Template("$resource_amount$resource_unit of $location_shortname's raw materials is $resource_shortname."),
    "ci": Template("$indicator_amount$indicator_unit of the raw materials extracted in $location_shortname "
                   "are in risk of $indicator_shortname."),
    "ip": Template("Of this $indicator_shortname risk, $part_amount$indicator_unit accounts to the "
                   "raw materials in $part_shortname."),
    "im": Template("Of this $indicator_shortname risk, $material_amount$indicator_unit accounts to the "
                   "raw materials in $material_shortname."),
    "ir": Template("Of this $indicator_shortname risk, $resource_amount$indicator_unit accounts to the "
                   "extraction of $resource_shortname."),
    "ic": Template("Of this $indicator_shortname risk, $location_amount$indicator_unit occurs in $location_shortname."),
}


class Explanation():

    sentences: List[str]

    # TODO Support spec string as a substitute for the path as well, i.e. _, _, path = compile_view_spec(spec).
    #   Currenty hidden in Analysis.
    def __init__(self, database: Database, object_tier: Tier, supply_chain: Dict[Tier, Set[Slug]],
                 path: Dict[Tier, Slug], method: str = EXPLAIN_METHOD_STAGED):

        def smart_round(num: float) -> float:
            # TODO Doesn't work well in all cases
            if num >= 10 or (num >= 1 and num // 10 == 0):
                result = round(num)
            elif num > 1:
                result = round(num, 1)
            else:
                dec = Decimal(float(num))
                adj = abs(dec.adjusted()) + 1
                result = round(num, adj)
            return result

        assert object_tier == DEVICE, "Currently only explanations for device analyses are supported."
        assert len(supply_chain[object_tier]) == 1, \
               "For explanation, currently only functional units with a single object are supported."

        # Each selection needs a filter of exactly one object for this explanation to work
        explanation_available = all([',' not in path[tier] for tier in path])
        if explanation_available:
            # For ordered method, sort axes, because explanation is done in order of the tiers
            if method == EXPLAIN_METHOD_ORDERED:
                path = dict(sorted(path.items()))
            # Optional: Add one tier in front in case there is some distance to the object_type, e.g. DEVICE
            # if filters[0] - 1 < self.object_type:
            #     filters[filters[0]-1] = "die_obersten_x_elemente_die_genügend"
        else:
            return "Explanation in only available for exactly one filtered object in each tier."

        # Find all placeholder values
        # TODO This assumes that the functional unit consists of only one object
        object_slug = next(iter(supply_chain[object_tier]))  # With sets it's a bit strange to fetch the one and only element
        mapping: Dict[str, str] = dict()
        view_selection: List[Tier] = list()
        view_filters: Dict[Tier, Set[Slug]] = dict()
        view_slugs: Dict[Tier, Slug] = dict()
        view_slug: str = ""
        # Add base values
        object = database.get_entity(object_tier, object_slug)  # TODO Hier wird nur das erste genommen
        mapping["object_amount"] = "1"
        mapping["object_unit"] = object_tier.unit
        mapping["object_name"] = object.name
        mapping["object_shortname"] = object.short_name
        # Add thread of values
        for tier in path:
            slug = path[tier]
            view_selection += [tier]
            view_filters[tier] = {slug}
            view_slugs[tier] = slug
            # An Analysis always sort the selection and therefore the slugs in multi-dimensional selections.
            #   In case of non-sorted path, we need to recreate the order. (In case of sorted path,
            #   doing it as well doesn't take much time.)
            view_slug = "|".join(view_slugs[tier] for tier in sorted(view_slugs.keys()))
            analysis = Analysis(database, object_tier, supply_chain, selection=view_selection,
                                depth=tier, filters=view_filters)
            view = analysis.view()
            row = view.df.loc[view_slug]
            tier_name = database.get_name_of_tier(tier, True).lower()
            entity = database.get_entity(tier, slug)
            mapping[tier_name + "_amount"] = str(smart_round(float(row[AMOUNT])))
            mapping[tier_name + "_unit"] = str(row[UNIT])
            mapping[tier_name + "_name"] = entity.name
            mapping[tier_name + "_shortname"] = entity.short_name
        # Add association values
        for this_axis in path.keys():
            next_axis = this_axis.next
            if next_axis in path.keys():
                this_slug = Slug(path[this_axis])
                next_slug = path[next_axis]
                assignment = database.get_assignment(this_axis, this_slug, next_slug)
                this_entity_name = database.get_name_of_tier(this_axis, True).lower()
                next_entity_name = database.get_name_of_tier(next_axis, True).lower()
                # mapping[this_entity_name + "_" + next_entity_name + "_amount"] = smart_round(association.amount)
                mapping[this_entity_name + "_" + next_entity_name + "_share"] = \
                    str(smart_round(assignment.amount * 100))
                mapping[this_entity_name + "_" + next_entity_name + "_source"] = \
                    assignment.source if assignment.source else ""

        self.sentences = []
        if method == EXPLAIN_METHOD_ORDERED:
            # Fetch correct templates, identified by sequence of filters
            template_key = ''.join([tier.letter for tier in list(path)])
            if template_key in explanation_templates_ordered:
                template = explanation_templates_ordered[template_key]
            else:
                return "No explanation template defined for view of kind '" + template_key + "'"
            # Replace placeholders
            self.sentences.append(template.safe_substitute(**mapping).capitalize())
        elif method == EXPLAIN_METHOD_STAGED:
            path_keys = [object_tier]+list(path.keys())
            for tier_left, tier_right in zip(path_keys, path_keys[1:]):
                template_key = tier_left.letter + tier_right.letter
                if template_key in explanation_templates_segmented:
                    template = explanation_templates_segmented[template_key]
                    self.sentences.append(template.safe_substitute(**mapping))
                else:
                    self.sentences.append(f"[ {template_key.upper()} ]")
        else:
            raise ValueError("Either specify 'ordered' or 'segmented' as method for explaination.")

    def write(self) -> str:
        return " ".join(self.sentences)
