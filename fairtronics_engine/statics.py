# Uses of assessments
# STRAIGHT: Use probability of having indication. All locations with unknown probability get 0%, i.e.
# best performance. As a consequence, only look at the hotspots, not at the "good" performing locations.
STRAIGHT = "straight"
# INVERS: Probability of not having indication, e.g. 3% for a location gets converted to 97%. All locations
# with unknown probability have probability 0% (i.e. worst performance).
INVERSE = "inverse"
# UNKNOWN: Amount of unknown indication: All locations for which we have indicator data get 0%, all other
# (i.e. with unknown indication) get 100% probability. Use this to get an idea of how much data is missing.
UNKNOWN = "unknown"

# View column names
SLUG = 'slug'
NAME = 'name'
SHORT = 'short name'
UNIT = 'unit'
AMOUNT = 'amount'
SHARE = 'share %'
TYPE = 'type'
RANK = 'rank'
CONTRIB = 'contribution %'
USAGE = 'usage'

# View special rows
OTHER_NAME = "Other"
OTHER_SHORT = "Other"
OTHER_SLUG = "_other_"
OTHER_TOP_NAME = "Other (top)"
OTHER_TOP_SHORT = "Other (top)"
OTHER_TOP_SLUG = "_other-top_"
OTHER_BOTTOM_NAME = "Other (bottom)"
OTHER_BOTTOM_SHORT = "Other (bottom)"
OTHER_BOTTOM_SLUG = "_other-bottom_"

WITH_OTHER_NONE = "none"
WITH_OTHER_SINGLE = "single"
WITH_OTHER_TOP_BOTTOM = "top-bottom"
WITH_OTHER_TOP_BOTTOM_SINGLE = WITH_OTHER_TOP_BOTTOM + "-" + WITH_OTHER_SINGLE

VIEW_DEVICE_NUMBER_OF_PARTS = "Dp"
VIEW_DEVICE_PARTS = "dP"

SHOW_DEVICE_BOM = [AMOUNT, 'part manufacturer', NAME]
SHOW_STANDARD = [RANK, AMOUNT, UNIT, SHARE, SHORT]
SHOW_STANDARD_ALL = [RANK, AMOUNT, UNIT, SHARE, NAME, SHORT, TYPE]
SHOW_STANDARD_ASSESS = [RANK, AMOUNT, UNIT, SHORT]
SHOW_STANDARD_GROUPED = [NAME, AMOUNT, UNIT, SHARE]
SHOW_STANDARD_RELATION = [AMOUNT, UNIT, NAME]

VERBOSE = False
