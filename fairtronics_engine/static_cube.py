# from line_profiler_decorator import profiler as profile
from fairtronics_engine.cube_method import CubeMethod
from fairtronics_engine.cube import Cube
from typing import List, Dict, Tuple, Any, Iterable, Optional, TypeVar, Sequence, Set
import numpy as np
from fairtronics_engine.database import Database, Tier, Slug, CHAIN

T = TypeVar('T')


class StaticCube(Cube[T]):

    ndary: np.ndarray
    start_tier: Tier
    final_tier: Tier
    slug_map: Dict[Tier, List[Slug]]  # A mapping of slugs to cube positions along the tiers, therefore a list

    # @profile
    def __init__(self, database: Database, method: CubeMethod[T], supply_chain: Dict[Tier, Set[Slug]],
                 selection: List[Tier] = [], filters: Dict[Tier, Set[Slug]] = {}, depth: Optional[Tier] = None):

        super().__init__(database, method, supply_chain, selection, filters, depth)
        self.start_tier = min(supply_chain.keys())
        self.final_tier = self.start_tier  # Just as a starting point

        # Restrict unit to what we actually want to analyse
        # TODO Maybe we can store the assignments here which we need later, not just the slugs
        self.selection = sorted(selection)
        self.slug_map = dict()
        if self.selection:
            self.final_tier = max(self.selection)
        if filters:
            self.final_tier = max(self.final_tier, max(filters.keys()))
        if depth:
            self.final_tier = max(self.final_tier, depth)
        for tier in supply_chain:
            if tier > self.final_tier:
                break
            if tier in filters:
                self.slug_map[tier] = list(supply_chain[tier] & filters[tier])  # Only interested in filtered entities
            else:
                self.slug_map[tier] = list(supply_chain[tier])

        # Initialize the cube. We only need those tiers which are in the selection
        cube_shape = tuple([len(self.slug_map[tier]) for tier in self.selection])
        # TODO Hier evtl. die DTYPEs durchiterieren, wenn der Cube zu groß wird
        self.ndary = np.ndarray(cube_shape, dtype=method.dtype)
        # The cube starts with 1.0 because we will multiply the actual weights on it
        self.ndary.fill(self.method.one)

    def shape(self) -> Tuple[int]:
        return np.shape(self.ndary)

    def calculate(self) -> None:
        # recursive variant of skip_tiers
        def skip_tiers_recursive(tier: Tier, factors: Dict[Slug, T], stop_tiers: List[Tier]) -> \
                Tuple[Tier, Dict[Slug, T]]:
            next_tier = CHAIN.next_of(tier)
            new_factors: Dict[Slug, T] = {}
            for slug, factor in factors.items():
                # A "if factor == 0.0: continue" doesn't pay of
                assignments = self.database.get_assigments(tier, slug)
                # Collect all slug of the skipped tier if it is in the tiers's filter set
                # Important to multiply it with probably previous recursions
                for assigned_slug in self.slug_map[next_tier]:
                    if assigned_slug in assignments:
                        new_factor = self.method.func(assignments[assigned_slug]) * factor
                        if assigned_slug in new_factors:
                            new_factors[assigned_slug] += new_factor
                        else:
                            new_factors[assigned_slug] = new_factor
            if next_tier in stop_tiers:
                return next_tier, new_factors
            # Ok, the next tier still isn't the tier to stop, so skip another tier
            # TODO Diese Endrekursion lässt sich sicher gut in eine while-Schleife umwandeln!
            return skip_tiers_recursive(next_tier, new_factors, stop_tiers)

        # In case of tiers not in selection, we need to skip these, i.e. starting with
        #   a tier and a single slug, collect all its assigned slugs in the tier following,
        #   until the next tier in selection has been reached.
        # TODO Would be better to give the stop_tier explicitly. This would eliminate the second return
        #   parameter and the check "in stop_filters"
        # TODO Just as we have a skip_tiers subfunction, we should have a account_tiers function for tier
        #   series in selection
        # @profile
        def skip_tiers(tier: Tier, factors: Dict[Slug, T], stop_tiers: List[Tier]) -> Tuple[Tier, Dict[Slug, T]]:
            next_tier = tier
            while next_tier not in stop_tiers:
                # Ok, the next tier still isn't the tier to stop, so skip another tier
                next_tier = CHAIN.next_of(tier)
                new_factors: Dict[Slug, T] = {}
                for slug, factor in factors.items():
                    # A "if factor == self.method.zero: continue" doesn't pay of
                    assignments = self.database.get_assigments(tier, slug)
                    # Collect all slug of the skipped tier if it is in the tiers's filter set
                    # Important to multiply it with probably previous recursions
                    for assigned_slug in self.slug_map[next_tier]:
                        if assigned_slug in assignments:
                            new_factor = self.method.func(assignments[assigned_slug]) * factor
                            if assigned_slug in new_factors:
                                new_factors[assigned_slug] += new_factor
                            else:
                                new_factors[assigned_slug] = new_factor
                tier, factors = next_tier, new_factors
            return next_tier, new_factors

        # The algorithm proceeds along the supply chain's tiers one after the other.
        # Since we are only interested in a few of these tiers (the selection = slug_map) and we only need to
        # store those in the cube, the chain changes between tiers in the selection and tiers not in selection,
        # e.g. dpMrC starts with two tiers not in selection, one tier in selection, one tier not in selection
        # and finally a tier again in selection.
        # The cube only holds data for the selected tier. For the tiers not in selection, the algorithm needs
        # to skip them by collecting all the factors to connect all entities (represented by its slugs in slug_map)
        # on the proceeding tier in selection to the next tier in selection, i.e. for dpMrC the materials and
        # the locations need to be connected by collecting the resource factors between them. (This is performed
        # by the sub-method skip_tiers().)
        # For tiers in selection, the factors are broadcasted through the cube to the following tiers as well
        # TODO Could it be an alternative to run through the Assignments, not the Entities in the Tiers?

        # Start at the beginning and check for tiers to be skipped right away, i.e. just like dp in in dpMrC
        # TODO Probably this may be integrated in the following main loop
        # Include each base object once = 1.0
        start_factors = {slug: self.method.one for slug in self.slug_map[self.start_tier]}
        if self.start_tier not in self.selection:
            tier, start_factors = skip_tiers(self.start_tier, start_factors, self.selection)
        else:
            tier = self.start_tier
        for index, slug in enumerate(self.slug_map[tier]):
            # 0.0 for deleting the initial 1.0
            self.ndary[(index, Ellipsis)] *= (start_factors[slug] if slug in start_factors else self.method.zero)
        left_index = tuple()  # Start list comprehension tuple from the left of the cube
        last_selected_tier = max(self.selection)

        # Now for all tiers in between, check for tiers in selection and broadcast its factors or
        #   skip tiers not in selection while retaining their factors
        # TODO No need to multiply ones on ones, so the base tier can simply be skipped in the loop?
        for this_tier in self.selection:
            # Peek what's next
            next_tier = CHAIN.next_of(this_tier)
            if not next_tier or next_tier > last_selected_tier:
                break  # Ah, there's no next anymore we are interested to see
            need_to_skip_tiers = next_tier not in self.selection  # Ah, the next we are not interested in (if true)
            # For all entities/slugs in this_tier we need to get the factors of all its assigned entities
            #   and multiply them to the cube
            for this_index, this_slug in enumerate(self.slug_map[this_tier]):
                assignments = self.database.get_assigments(this_tier, this_slug)
                factors = {assigned_slug: self.method.func(assignments[assigned_slug])
                           for assigned_slug in self.slug_map[next_tier] if assigned_slug in assignments}
                if need_to_skip_tiers:
                    # Collect all data until we are interested in again and put them in the cube
                    tier, factors = skip_tiers(next_tier, factors, self.selection)
                else:
                    tier = next_tier
                # Else, we already have the right factors and next_tier we are interested in
                # We need to iterate through all slags of the next tier to get the index right and
                #   to eliminate the initial ones (1.0) in case we have no factor
                for index, slug in enumerate(self.slug_map[tier]):
                    # Here's the trick: Broadcast new factors across all relevant cells down the chain
                    # https://numpy.org/doc/stable/user/basics.indexing.html#dealing-with-variable-numbers-of-indices-within-programs
                    # https://numpy.org/doc/stable/user/basics.broadcasting.html
                    self.ndary[left_index + (this_index, index, Ellipsis,)] *= \
                        (factors[slug] if slug in factors else self.method.zero)  # zero for deleting the initial ones
            left_index += (slice(None),)  # PRoceed to the next tier in the list comprehension tuple

        # Finally, at the end sum up all factors of tiers not in selection up to the suply chain's depth and
        #   apply them to the cube
        # TODO Probably this may be integrated in the preceeding main loop
        if this_tier < self.final_tier:
            for this_index, this_slug in enumerate(self.slug_map[this_tier]):
                _, final_factors = skip_tiers(this_tier, {this_slug: self.method.one}, [self.final_tier])
                final_factor = sum(final_factors.values(), self.method.zero)  # TODO np.sum()?
                self.ndary[left_index + (this_index,)] *= final_factor

    # TODO What type to declare? Any doesn't seem to be specific
    # @profile
    def values(self, cutoff: Optional[float] = 0.0, start: Optional[int] = 0, end: Optional[int] = None,
               ascending: bool = False) -> Tuple[Iterable[Tuple[Sequence[Slug], Any]], Any, Any, Any, int]:

        if self.ndary.size == 0:
            return (()), 0.0, 0.0, 0.0, 0

        if cutoff is not None:
            cutoff_indices = np.nonzero(self.ndary > cutoff)
            # In the end, we only need this amount of (sorted) values to cutoff correctly
            size = len(cutoff_indices[0])  # Gets the number of remaining indices according to the first dimension
        else:
            size = self.ndary.size
        if size == 0.0:
            return (()), 0.0, 0.0, 0.0, 0

        if end is None:
            end = size
        elif end < 0:
            end = size + end
        elif end > size:
            end = size
        if start is None:
            start = 0
        elif start < 0:
            # TODO Maybe this isn't as expected when having cutoff and start/end being both negative
            start = end + start

        if end - start > size:
            end = start + size

        if end - start <= 0:
            return (()), 0.0, 0.0, 0.0, 0

        if ascending:
            start += (self.ndary.size - size)
            end += (self.ndary.size - size)

        # We need to perform all operations in the index world of the cube to later fetch
        #   the corresponding slugs by index
        # And, for the same reason, although self.cube[cutoff_indices] are the only values we are interested in,
        #   we need to keep with whole self.cube to allow for clean unraveling after flattening
        flat_cube = self.ndary.flatten()

        # The following computes the n largest indices from a numpy array in tuples of arrays. Each position in the
        # tuple represents a dimension with the indices in the list being the index in this dimension.
        # It's based on https://stackoverflow.com/a/38884051 enhanced with skipping the m first largest indices,
        #   with m < n
        # and adding variant on smalles indices, i.e. ascending = True
        # See also note on N-dimensional arrays in https://numpy.org/doc/stable/reference/generated/numpy.argsort.html
        # Alternative for only one dimension: np.flip(np.argsort(array))[:n]
        if ascending:
            partitioned_indices_flat = np.argpartition(flat_cube, [start, end-1])
            # so, end-1 is part of the indices while end isn't
            indices_middle_flat = partitioned_indices_flat[slice(start, end)]
            indices_bottom_flat = partitioned_indices_flat[end:]
            indices_top_flat = partitioned_indices_flat[:start]
            sorted_indices_cube_flat = np.argsort(flat_cube[indices_middle_flat])
        else:
            partitioned_indices_flat = np.argpartition(flat_cube, [-start, -end])
            # else case to avoid [end:-0] if start is 0
            indices_middle_flat = partitioned_indices_flat[slice(-end, -start if start > 0 else None)]
            indices_bottom_flat = partitioned_indices_flat[:-end]
            indices_top_flat = partitioned_indices_flat[-start:] if start > 0 else np.indices([])
            sorted_indices_cube_flat = np.argsort(-flat_cube[indices_middle_flat])
        sorted_indices_flat = indices_middle_flat[sorted_indices_cube_flat]
        middle_indices = np.unravel_index(sorted_indices_flat, self.ndary.shape) if self.ndary.ndim > 0 else ([0],)

        # Now, back from index world to cube world with its values and the slugs beside
        # TODO take_along_axis? See https://numpy.org/doc/stable/reference/generated/numpy.argpartition.html
        # TODO Is the unraveling really needed? No way to fetch the values and slugs applying the flat indices
        #   to the flat cube?
        requested_values = self.ndary[middle_indices]
        requested_slugs = [[self.slug_map[self.selection[tier_index]][slug_index]
                            for tier_index, slug_index in enumerate(cube_index)]
                           for cube_index in np.transpose(middle_indices)]

        # Data to view remaining (i.e. even cutoff) data not in the values:
        top_values_sum = np.sum(flat_cube[indices_top_flat])
        middle_values_sum = np.sum(flat_cube[indices_middle_flat])
        bottom_values_sum = np.sum(flat_cube[indices_bottom_flat])

        return zip(requested_slugs, requested_values), top_values_sum, middle_values_sum, bottom_values_sum, size
