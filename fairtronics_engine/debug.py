class Debug:
    desc: str

    def __init__(self, desc: str):
        self.desc = desc

    def _m(self, left: str, right: str) -> str:
        if left == "" or right == "":
            return ""
        else:
            if left == "1":
                if right == "1":
                    return "1"
                else:
                    return right
            else:
                if right == "1":
                    return left
                else:
                    return f"{left} ** {right}"

    def _a(self, left: str, right: str) -> str:
        if left == "":
            if right == "":
                return ""
            else:
                return right
        else:
            if right == "":
                return left
            else:
                return f"{left} ++ {right}"

    def __add__(self, other: object):
        if isinstance(other, Debug):
            return Debug(self._a(self.desc, other.desc))
        raise TypeError()

    def __mul__(self, other: object):
        if isinstance(other, Debug):
            return Debug(self._m(self.desc, other.desc))
        raise TypeError()

    def __iadd__(self, other: object):
        if isinstance(other, Debug):
            self.desc = self._a(self.desc, other.desc)
            return self
        raise TypeError()

    def __imul__(self, other: object):
        if isinstance(other, Debug):
            self.desc = self._m(self.desc, other.desc)
            return self
        raise TypeError()

    # Don't really need this
    def __radd__(self, other: object):
        return Debug(self._a(str(other), self.desc))

    # Don't really need this
    def __rmul__(self, other: object):
        return Debug(self._m(str(other), self.desc))

    def __str__(self):
        return self.desc

    def __repr__(self):
        return "S[" + self.desc + "]"
