from typing import Tuple, Any, Iterable, Optional, Generic, TypeVar, Sequence, Dict, Collection, List, Set
from fairtronics_engine.model import Slug, Tier
from fairtronics_engine.database import Database
from fairtronics_engine.cube_method import CubeMethod

T = TypeVar('T')


class Cube(Generic[T]):

    database: Database
    method: CubeMethod[T]
    supply_chain: Dict[Tier, Set[Slug]]
    selection: List[Tier]
    filters: Dict[Tier, Set[Slug]]
    depth: Optional[Tier]

    def __init__(self, database: Database, method: CubeMethod[T], supply_chain: Dict[Tier, Set[Slug]],
                 selection: List[Tier] = [], filters: Dict[Tier, Set[Slug]] = {}, depth: Optional[Tier] = None):
        """
        Superclass for all data processing objects used by Analysis

        The initalization just stores the parameters for reuse in self

        :param database: the database of all the entities
        :param method: the CubeMethod with basic operators and constants to do the calculation with
        :param supply_chain: the Unit's supply chain with only the database entities relevant for the Unit
        :param selection: the list of tiers representing the dimensions of the cube,
                          i.e. what the Analysis is interested in. Expected to have the tiers in order
        :param filters: the entities (represented by their slugs) per tier the calculation
                        should be restricted to
        :param depth: the depth of supply chain which should be considered.
                      Defaults to the deepest tier in the selection
        """
        self.database = database
        self.method = method
        self.supply_chain = supply_chain
        self.selection = selection
        self.filters = filters
        self.depth = depth if depth else selection[0] if selection else None

    def shape(self) -> Tuple[int]:
        """
        Returns dimensions of cube

        :return: length of each axis, corresponding to the Cube's selection
        :raises NotImplementedError: if not implemented by subclass
        """

        raise NotImplementedError("Use a subclass of Cube.")

    def calculate(self) -> None:
        """
        Perfrom Cube-internal calculation

        Called after __init__()-ialation and before fetching values() to calculate the values, in this
        case to build up the cube with all the factors of the supply_chain

        :raises NotImplementedError: if not implemented by subclass
        """

        raise NotImplementedError("Use a subclass of Cube.")

    def values(self, cutoff: Optional[float] = 0.0, start: Optional[int] = 0, end: Optional[int] = None,
               ascending: bool = False) -> Tuple[Iterable[Tuple[Sequence[Slug], Any]], Any, Any, Any, int]:
        """
        Retrieves the values, possibly restricted, within the Cube in sorted order

        :param cutoff: If not None, ignore all values equal or below this value. Typically only the non-zero values
            should be taken, because a value of 0 normally indicates that there has never been a connection
            between the dimension's entities and therefore should not be taken into account.
            Default is 0.0, i.e. all zero values are ignored
        :param start: Given all values in sorted order, only return values including and below the start-th one.
            start could be negative to indicate a position above the bottom of the value list.
            If start is 0, all non-cutoff values are returned until the end is reached.
            Default is 0, i.e. all is returned
        :param end: Given all values in sorted order, only return values including and above the end-th one.
            end could be negative to indicate a position above the bottom of the value list.
            If end is None, all non-cutoff values are returned beginning from start,
            i.e. end is set to the bottom-most position.
            end is also set to the bottom-most position, if it happends to be out of range.
            If end is smaller than start, an empty list is returned.
            Default is None, i.e. all is returned
        :param ascending: If True, the values are sorted with the lowest one at the top.
            If False, the biggest one is at the top.
            Default is False

        :return: Multiple values:
            * tuples_of_values_and_slugs - Iterable on ([slugs], value) tupels in the requested order and number.
              The slugs are the keys of the database's entities in the cube's dimensions, i.e. tiers in the selection
            * top_values_sum - The sum of all values above (ascending or descending) the requested values,
              possibly including the values out of the cutoff
            * middle_values_sum - The sum of all requested values in the tuples_of_values_and_slugs (see above)
            * bottom_values_sum - The sum of all values below (ascending or descending) the requested values,
              The sum of all values below (ascending or descending) the requested values,
              possibly including the values out of the cutoff
            * size - The number of all values without the cutoff values. The count of all values in the cube
              regardless of the requested restrictions on cutoff or start/end is
              top_values_sum + middle_values_sum + bottom_values_sum
        :raises NotImplementedError: if not implemented by subclass
        """

        raise NotImplementedError("Use a subclass of Cube.")
