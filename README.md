# Fairtronics Analysis Engine

The computational core of the Fairtronics project.

## Setup

Make sure the following prerequisites are met:

- Python 3.9 or higher
- [poetry](https://python-poetry.org/docs/#installation)

Install dependencies:

```bash
$ poetry install
```

## Command Line Usage

Enter the virtual environment:

```bash
$ poetry shell
```

Now you can run the command line interface using the `ftanalyze` command:

```bash
$ ftanalyze
```

## Development

Set up the virtual environment as defined above. Configure your IDE to use it.
You can find out where the virtualenv is stored using this command:

```bash
$ poetry env info -p
```

### Make a release

Once you’ve finished implementing a feature, you can make a release by assigning
it a version number.

First, make sure that all relevant code is committed and merged into the
**main** branch.

You can find out the current version number by looking at `pyproject.toml` or
using the release script without an argument:

```bash
$ ./release.sh
Current version: fairtronics-engine 0.1.7-dev
```

Code on the main branch always has a version number ending in `-dev` to keep
packages from being confused with a release.

Now determine what the new version number should be, considering the rules of
[semantic versioning](https://semver.org/):

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
> - MAJOR version when you make incompatible API changes,
> - MINOR version when you add functionality in a backwards compatible manner, and
> - PATCH version when you make backwards compatible bug fixes.

This means that from version 1.0.1, you could advance to

- 1.0.2 if you made a bug fix
- 1.1.0 if you added functionality but kept backwards compatibility
- 2.0.0 if you broke backwards compatibility

As long as we have the MAJOR version 0, we will treat version numbers in a more
relaxed way, as breaking changes are expected to happen regularly. So we will
increase MINOR for breaking changes and PATCH for non-breaking changes,
including extensions.

Finally, make a release stating your new version number (without a -dev suffix!).

```bash
$ ./release.sh 1.0.2
```

By doing this, a separate branch is created for this release, which triggers a
pipeline in Gitlab that builds the package for the release.