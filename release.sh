#!/usr/bin/env bash
# Usage: release.sh <version>

set -euo pipefail

if [ "$#" -ne 1 ]; then
    echo "Current version: $(poetry version)"
    exit 1
fi

version="$1"
branch=$(git branch --show-current)
git checkout -b "release-$version"
poetry version "$version"
git add pyproject.toml
git commit -m "Version $version"
git tag -a -m "Version $version" "$version"
git push origin "$version"
git push --tags
git checkout "$branch"
poetry version "${version}-dev"
git add pyproject.toml
git commit -m "Bump version to ${version}-dev"
